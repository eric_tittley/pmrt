#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "PMRT.h"

#include "RT.h"
#include "RT_Data.h"
#include "Logic.h"

#ifdef TIME_CODE_SEGMENTS
#include "Support.h"
#endif

#ifndef CONSTANT_DENSITY
# include "mpipm.h"
#endif

/* For MASTER */
#include "RT_constants_configuration.h"

int PMRT_BackupState( const int NumPMProcs,
                      const int NumParticles,
                      const int NumPMFiles,
                      const int FirstRTPassFlag,
                      const int Nlos,
                      const char *Dir,
                      const char *PMBackupFile,
                      const float *r,
                      const float *v,
                      RT_Data ** const Data,
                      const double aa,
                      const double dloga) {
 int me;
 int ierr;
 char filename[FILENAME_MAX];

 /* Set a MPI process number. MASTER if not MPI */
#ifndef NO_MPI
 (void)MPI_Comm_rank(MPI_COMM_WORLD, &me);
#else
 me=MASTER;
#endif

#if VERBOSE >= 1
 if(me==MASTER) {
  printf(" Backing up...\n"); ierr=fflush(stdout);
 }
#endif

#ifndef CONSTANT_DENSITY
 if(me < NumPMProcs ){
  /* Backup the PM data */
  ierr=outputfile(me, NumPMProcs, NumParticles, r, v, aa, PMBackupFile,
                  NumPMFiles);
  PMRT_ErrorCheck_File(ierr,"Unable to write dump file",PM_BAK,me,__LINE__);
 }
#endif

 /* Backup the RT data */
 if( (me==MASTER) && (FirstRTPassFlag!=TRUE) ) {
  ierr=snprintf(filename,FILENAME_MAX-1,"%s/RT.bak",Dir);
  if(ierr>(int)FILENAME_MAX) {
   ierr=EXIT_FAILURE;
  } else {
   ierr=EXIT_SUCCESS;
  }
  PMRT_ErrorCheck_File(ierr,"Full path too long",filename,me,__LINE__);
  ierr=RT_SaveData(filename, Data, Nlos);
  PMRT_ErrorCheck_File(ierr,"Unable to write dump file",filename,me,__LINE__);
 } /* if me==MASTER */

 /* Backup the timestep */
 if(me==MASTER) {
  ierr=snprintf(filename,FILENAME_MAX-1,"%s/Status.bak",Dir);
  if(ierr>(int)FILENAME_MAX) {
   ierr=EXIT_FAILURE;
  } else {
   ierr=EXIT_SUCCESS;
  }
  PMRT_ErrorCheck_File(ierr,"Full path too long",filename,me,__LINE__);
  ierr=PMRT_WriteStatus(filename,dloga);
  PMRT_ErrorCheck_File(ierr,"Unable to write dump file",filename,me,__LINE__);
 }

 return EXIT_SUCCESS; 
}
