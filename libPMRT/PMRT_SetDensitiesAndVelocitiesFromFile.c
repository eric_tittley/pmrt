#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#ifndef NO_MPI
# include <mpi.h>
#endif

#include "PMRT.h"

#include "RT.h"
#include "RT_Data.h"
#include "Logic.h"
#include "Support.h"

/* For MASTER */
#include "RT_constants_configuration.h"

PMRT_SetDensitiesAndVelocitiesFromFile(DensityParameters *Params,
				       const int Ncells,
				       const double a_f,
			   	       const int Nlos,
			   	       const int Offset,
			   	       const int NperDim,
			   	       const double r0,
			   	       const double length,
				       RT_Data *Data
				      ) {

 int me;
 int ierr;
 double *density;
 double *velocity_z;
 double *veloctiy_x;

 /* Return if this is not the first pass */
 if( Params->FirstPass == FALSE ) {
  return;
 } else {
  Params->FirstPass == FALSE;
 }

 /* Set a MPI process number. MASTER if not MPI */
#ifndef NO_MPI
 (void)MPI_Comm_rank(MPI_COMM_WORLD, &me);
#else
 me=MASTER;
#endif
 
 /* Everything to do is done by the master */
 if (me == MASTER) {

  /* Allocate memory */
  ierr = fields_allocate_memory(me, Ncells, &density, &velocity_z, &velocity_x);

  /* Read the file */
  ierr = read_density_file(me,Params->DensityFile,a_f,Ncells,density,
                           velocity_z,velocity_x);
  PMRT_ErrorCheck_File(ierr,"read_density_file failed",
                       Params->DensityFile,me,__LINE__);

  /* Grid the data onto the RTData structure */
  PMRT_GriddedDataToRTData(density,
                           velocity_x,
			   velocity_z,
			   Nlos;
			   Offset;
			   NperDim;
			   r0;
			   length;
			   Data);

  /* Free allocated memory */
  free(density);
  free(velocity_x);
  free(velocity_z);
 
 } /* End if me == MASTER */
 
 return EXIT_SUCCESS;
}
