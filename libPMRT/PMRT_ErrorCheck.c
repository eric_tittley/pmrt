#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#ifndef NO_MPI
# include <mpi.h>
#endif

#include "PMRT.h"

void PMRT_ErrorCheck( const int ierr,
                      const char *Msg,
		      const int me,
		      const int LINE )
{
 int ierr_dum;
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) ERROR: PMRT: %i: %s\n",me,LINE,Msg);
  ierr_dum=fflush(stdout);
 #ifndef NO_MPI
  ierr_dum=MPI_Abort(MPI_COMM_WORLD, EXIT_FAILURE);
  if(ierr_dum != MPI_SUCCESS) {
   printf("(%2i) ERROR: PMRT: %i: MPI_Abort failed\n",me,LINE);
   exit(EXIT_FAILURE);
  }
 #else
  exit(EXIT_FAILURE);
 #endif
 }
}

