#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>

#ifndef NO_MPI
# include <mpi.h>
#endif

#include "PMRT.h"

#ifndef NO_MPI
void PMRT_ErrorCheck_MPI( const int ierr,
                          const char *Msg,
		          const int me,
		          const int LINE )
{
 int ierr_dum;
 if(ierr != MPI_SUCCESS) {
  PMRT_ErrorCheck( EXIT_FAILURE, Msg, me, LINE );
 }
}
#endif

