#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>

#include "PMRT.h"

#include "RT.h"
#include "RT_Data.h"
#include "Logic.h"

PMRT_GriddedDataToRTData(const double *density,
                         const double *velocity_x,
			 const double *velocity_z,
			 const int Nlos;
			 const int Offset;
			 const int NperDim;
			 const double r0;
			 const double length;
			 RT_Data *Data) {

 int i,j;

 /* Density */
 for (j = 0; j < Nlos; j++) {
  /* Copy what was new to old */
  memcpy(Data[j]->Dold, Data[j]->D, sizeof(double)*Data[j]->NumCells);

  /* Copy new to D */
  for (i = 0; i < Data[j]->NumCells; i++) {
   Data[j]->D[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
   	                                  density+Offset+NperDim*j,
					  NperDim, r0, length );
  #ifdef DEBUG
   if(Data[j]->D[i] <= 0) {
    printf("ERROR: %s: %i: RT_InterpolateFromGrid returned Data[%i].D[%i]=%5.3e\n",
           __FILE__,__LINE__,j,i,Data[j]->D[i]);
    printf("	   Data.R+dR/2=%5.3e; NperDim=%i; r0=%5.3e; length=%5.3e\n",
           Data[j]->R[i]+dR/2.0, NperDim, r0, length);
    PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
   }
  #endif
  }
 }

 if(Params.CalcVelocities==TRUE) {
  /* velocity_* hold the velocity grids.
   * Copy the LOS velocities to the Data structure.
   * Not needed for the code to run, but useful for post-processing.
   * velocity_* are [NG x NG x NFLUX].  We only want velocity_*[0,*,*] 
   * or velocity_*[0,0,*] */
  for (j = 0; j < Nlos; j++) {
   for (i = 0; i < Data[j]->NumCells; i++) {
    Data[j]->v_x[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
 				             velocity_x+NperDim*j,
					     NperDim,
					     r0,
					     length );
   }
  }
  for (j = 0; j < Nlos; j++) {
   for (i = 0; i < Data[j]->NumCells; i++) {
    Data[j]->v_z[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
 				             velocity_z+NperDim*j,
					     NperDim,
					     r0,
					     length );
   }
  }
 }

}
