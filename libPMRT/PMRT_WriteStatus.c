/* Save the timestep and the iteration step.
 *
 * Arguments
 *  Input, not modified
 *   filename	Name of the file to which to write the data.
 *   dloga	Timestep.
 *
 * Returns
 *  EXIT_SUCCESS	Success
 *  EXIT_FAILURE	Failure
 *
 * Author: Eric Tittley
 *
 * History
 *  04 08 04 First version
 *  04 09 03 Elliminated iter (iteration number)
 *  11 05 30 Moved to PMRT library
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "mpipm.h"

int PMRT_WriteStatus(const char *filename, const double dloga)
{
 FILE *fid;
 int ierr;
 size_t Nwritten;
 
 /* Open the file */
 fid = fopen(filename, "w");
 if (fid == NULL) {
  perror(filename);
  printf("ERROR: %s: %i: Unable to open file %s\n",
         __FILE__, __LINE__, filename);
  return EXIT_FAILURE;
 }

 /* Write the timestep */
 Nwritten = fwrite(&dloga, sizeof(double), 1, fid);
 if( Nwritten != 1 ) {
  printf("ERROR: %s: %i: Wrote %d of 1 elements to %s\n",
         __FILE__, __LINE__, (int)Nwritten, filename);
  return EXIT_FAILURE;
 }
 
 /* Close the file */
 ierr = fclose(fid);
 if (ierr != 0) {
  perror(filename);
  printf("ERROR: %s: %i: Unable to close file %s\n",
         __FILE__, __LINE__, filename);
  return EXIT_FAILURE;
 }

 return EXIT_SUCCESS;
}
