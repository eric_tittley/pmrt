#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifndef CONSTANT_DENSITY
# include <mpi.h>
#endif

#include "PMRT.h"

/* For MASTER */
#include "RT_constants_configuration.h"

double PMRT_GetNewTimeStep(
                          #ifndef CONSTANT_DENSITY
                           const MPI_Comm PM_Comm,
                           const int      Ng,
                                 double   MyFmax,
                           const double   feps,
			   const double   stepmax,
                          #endif
                           const double   aa,
                           const double   DTol,
                           const double   af,
                           const int      iout,
                           const double  *aout) {

 /* The timestep to return */
 double dloga;
 
 int me=0;
#if !(defined CONSTANT_DENSITY) || ( VERBOSE >= 1 )
 int ierr;
#endif

#ifndef CONSTANT_DENSITY
 int NumAllProcs,NumPMProcs;
 double Fmax;
#endif
   
#ifndef CONSTANT_DENSITY
 /* The number of all MPI processes */
 ierr=MPI_Comm_size(MPI_COMM_WORLD, &NumAllProcs);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Comm_size failed",me,__LINE__);

 /* The number of MPI processes performing PM */
 ierr=MPI_Comm_size(PM_Comm, &NumPMProcs);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Comm_rank failed",me,__LINE__);

 /* Which MPI process I am */
 ierr=MPI_Comm_rank(MPI_COMM_WORLD, &me);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Comm_rank failed",me,__LINE__);
#endif

 /* Get the maximum force amongst all the nodes
  * The only options here are if the data is using CONSTANT DENSITY
  * otherwise the densities are coming from the PM code, which necessarily
  * requires MPI.
  * But we can still have CONSTANT DENSITY and MPI, if RT is distributing via
  * MPI.
  */
#ifdef CONSTANT_DENSITY
 dloga = log(DTol * aout[iout] / aa);
#else

 /* Density is coming from the PM code */
 /* Find the maximum Fmax (maximum force) among all the PM processes */
 if(me < NumPMProcs) {
  ierr=MPI_Allreduce(&MyFmax, &Fmax, 1, MPI_DOUBLE, MPI_MAX, PM_Comm);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Allreduce failed",me,__LINE__);
 }
 /* And broadcast to everyone if the PM Communicator is not the WORLD */
 if(NumAllProcs != NumPMProcs) {
  ierr=MPI_Bcast(&Fmax, 1, MPI_FLOAT, MASTER, MPI_COMM_WORLD);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Bcast failed",me,__LINE__);
 }
#if VERBOSE >= 1
 if( me == MASTER ) {
  printf("PMRT: Maximum PM force @ z = %f; Fmax = %f\n", 1.0 / aa - 1.0,Fmax);
 }
#endif
 /* The suggested next time (expansion) step */
 dloga = sqrt(2.0 * feps / Fmax / (double)Ng);
#ifdef DEBUG
 if(isnan(dloga)!=0) {
  printf("ERROR: PMRT: dloga=NAN: feps=%e; AFmax = %e; Ng=%i;\n", feps,Fmax,Ng);
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
 }
#endif
 /* Limit the time step to a maximum step size */
 if (dloga > stepmax)
  dloga = stepmax;

#endif /* ifdef CONSTANT_DENSITY ... else ... endif */

 /* Truncate the time step so t does not not surpass (significantly) the next
  * output time. This ensures outputs occur when they should, not at the
  * ealiest convenience. */
 if (aa * exp(dloga) > aout[iout])
  dloga = log(DTol * aout[iout] / aa);
 /* Same as the above, but for the final time step */
 if (aa * exp(dloga) > af)
  dloga = log(DTol * af / aa);
#if VERBOSE >= 1
 if( dloga < 0.001 ) {
  printf("(%2i) PMRT: WARNING: dloga < 0.001  dloga = %f\n",me,dloga);
 #ifndef CONSTANT_DENSITY
  printf("     sqrt(2*feps/Fmax/Ng) = %f\n",sqrt(2 * feps / Fmax / (double)Ng));
 #endif
  printf("     log(DTol*aout[iout]/aa) = %f\n",
         log(DTol*aout[iout]/aa));
  printf("     log(DTol*af/aa) = %f\n",log(DTol*af/aa) );
  ierr=fflush(stdout);
 }
#endif
 if( (dloga <= 0.0) && (aa<af) ) {
  printf("(%2i) PMRT: ERROR: %i: dloga <= 0  dloga = %e\n",me,__LINE__,dloga);
 #ifndef CONSTANT_DENSITY
  printf("     sqrt(2*feps/Fmax/Ng) = %f\n",sqrt(2 * feps / Fmax / (double)Ng));
 #endif
  printf("     log(DTol*aout[iout]/aa) = %e\n",
         log(DTol*aout[iout]/aa));
  printf("     log(DTol*af/aa) = %e\n",log(DTol*af/aa) );
  printf("     aout[iout]=%e; aa=%e; af=%e\n",aout[iout],aa,af);
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
 }

 return dloga;
}
