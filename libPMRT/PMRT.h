#ifndef _PMRT_H_
#define _PMRT_H_

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#ifndef CONSTANT_DENSITY
# include "mpi.h"
#endif

#include "RT_Data.h"

void PMRT_ErrorCheck( const int ierr,
                      const char *Msg,
		      const int me,
		      const int LINE );

void PMRT_ErrorCheck_File( const int ierr,
                           const char *Msg,
			   const char *File,
		           const int me,
		           const int LINE );

void PMRT_ErrorCheck_MPI( const int ierr,
                          const char *Msg,
		          const int me,
		          const int LINE );

int PMRT_ReadStatus(const char *filename, double *dloga);

int PMRT_WriteStatus(const char *filename, const double dloga);

int PMRT_BackupState( const int NumPMProcs,
                      const int NumParticles,
                      const int NumPMFiles,
                      const int FirstRTPassFlag,
                      const int Nlos,
                      const char *Dir,
                      const char *PMBackupFile,
                      const float *r,
                      const float *v,
                      RT_Data ** const Data,
                      const double aa,
                      const double dloga);

double PMRT_GetNewTimeStep(
                          #ifndef CONSTANT_DENSITY
                           const MPI_Comm PM_Comm,
                           const int      Ng,
                                 double   MyFmax,
                           const double   feps,
			   const double   stepmax,
                          #endif
                           const double   aa,
                           const double   DTol,
                           const double   af,
                           const int      iout,
                           const double  *aout);


#endif
