int PMRT_SetDensitiesAndVelocities(RT_Data *Data, DensityParameters *Params ) {

 /* Cases:
  *  Read the density from a file
  *  Set a constant density
  *   Not comoving
  *   Comoving
  *  Get the density from a particle distribution
  *   Gridded
  *   Polar
  */
  
 switch (Params.Method) {
  case DM_FILE:
   ierr = PMRT_SetDensitiesAndVelocitiesFromFile(Params,
				                 Ncells,
				                 a_f,
			   	                 Nlos,
			   	                 Offset,
			   	                 NperDim,
			   	                 r0,
			   	                 length,
						 Data);
   break;
  case DM_CONST:
   break;
  case DM_CONST_COMOVING:
   break;
  case DM_FROM_PARTICLES_GRIDDED:
   break;
  case DM_FROM_PARTICLES_POLAR:
   break;
  default:
   printf("Unknown method for calculating the density");
 } /* End switch(Params.Method) */
 PMRT_ErrorCheck(ierr,"density set failed",me,__LINE__);





  #ifdef READ_DENSITY_FILE
   if (me == MASTER) {
    ierr = read_density_file(me,READ_DENSITY_FILE,a_f,Ncells,density,
                             velocity_z,velocity_x);
    PMRT_ErrorCheck_File(ierr,"read_density_file failed",
                         READ_DENSITY_FILE,me,__LINE__);
   }
  #endif
   if(me < num_PM_procs) {
   #if (defined CALC_DENSITY) || (defined CALC_DENSITY_POLAR)
    /* Calculate the velocity unit */
    vunit=Vunit(aa,Length,omega_m,omega_v,0.0);
    if(me == MASTER ) {
     printf("vunit=%8.6e;\n",vunit);
    }
   #endif
   #ifdef CALC_DENSITY
   #if VERBOSE >= 1
    printf("(%2i) Calling calc_density.\n",me); ierr=fflush(stdout);
   #endif
    ierr = calc_density(PM_Comm, np, NG, a_f, vunit, r, v, kernel,
                        density, velocity_x, velocity_z);
    PMRT_ErrorCheck(ierr,"calc_density failed",me,__LINE__);
   #if VERBOSE >= 1
    printf("(%2i) Returned from calc_density.\n",me);
    ierr=fflush(stdout);
   #endif
   #endif /* CALC_DENSITY */
   #ifdef CALC_DENSITY_POLAR
    /* Note: velocity_z is actually velocity_y */
    ierr = calc_polardensity(me, r, v, np, nR, nTheta, xoffset, yoffset,
    		             points, particleRadius, R_RT, slab_thickness,
			     a_f, vunit, &seed, density, velocity_x, velocity_z);
    PMRT_ErrorCheck(ierr,"calc_polardensity failed",me,__LINE__);
   #endif /* CALC_DENSITY_POLAR */
   } /* end if me < num_PM_procs */

   if( (me==MASTER) ) {
   #ifdef CONSTANT_DENSITY
    for (j = 0; j < Nlos; j++) {
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->D[i]    = CONSTANT_DENSITY;
      Data[j]->Dold[i] = CONSTANT_DENSITY;
     #ifdef CONSTANT_DENSITY_COMOVING
      Data[j]->D[i]    *= pow(1+z_f,3);
      Data[j]->Dold[i] *= pow(1+z  ,3);
     #endif
     }
    }
   #else /* Not constant density */
    /* Set the densities at the start and end of the iteration. */
    /* First, calculate the offset to get us to the slice we want */
   #if ( LOS_SLICE != 0 ) && !(defined CALC_DENSITY_POLAR)
    Offset = LOS_SLICE*NGy;
   #else
    Offset = 0;
   #endif
    for (j = 0; j < Nlos; j++) {
     memcpy(Data[j]->Dold, Data[j]->D, sizeof(double)*Data[j]->NumCells);
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->D[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
                       density+Offset+NGx*j, NGx, r0, length );
     #ifdef ENFORCE_MINIMUM_DENSITY
      /*Enforce a minimum density */
      if (Data[j]->D[i] <= 0.) {
       Data[j]->D[i] = ENFORCE_MINIMUM_DENSITY;
      }
     #endif
     #ifdef DEBUG
      if(Data[j]->D[i] <= 0) {
       printf("ERROR: PMRT: %i: RT_InterpolateFromGrid returned Data[%i].D[%i]=%5.3e\n",
              __LINE__,j,i,Data[j]->D[i]);
       printf("       Data.R+dR/2=%5.3e; NGx=%i; r0=%5.3e; length=%5.3e\n",
              Data[j]->R[i]+dR/2.0, NGx, r0, length);
       PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
      }
     #endif
     }
    }
   #endif /* If CONSTANT_DENSITY then ... else ... endif */
   }

   /* On the first pass, set Data.D to density at the *start*, and the
    * velocities to 0. */
   if( (FirstRTPassFlag==TRUE) && (me==MASTER) ) {
    FirstRTPassFlag=FALSE;
    for (j = 0; j < Nlos; j++) {
     memcpy(Data[j]->Dold, Data[j]->D, sizeof(double)*Data[j]->NumCells);
     memset(Data[j]->v_z,           0, sizeof(double)*Data[j]->NumCells);
     memset(Data[j]->v_x,           0, sizeof(double)*Data[j]->NumCells);
    } 
   }


} /* End of PMRT_SetDensitiesAndVelocities */
