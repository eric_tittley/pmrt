/* Read the timestep and the iteration step.
 *
 * Arguments
 *  Input, not modified
 *   filename	Name of the file to which to Read the data.
 *  Initialised
 *   dloga	Timestep.
 *
 * Returns
 *  EXIT_SUCCESS	Success
 *  EXIT_FAILURE	Failure
 *
 * Author: Eric Tittley
 *
 * History
 *  04 08 04 First version
 *  04 09 03 Elliminated iter (the iteration number)
 *  11 05 30 Moved to PMRT library
 */

#include <stdlib.h>
#include <stdio.h>
#include <errno.h>

#include "mpipm.h"

int PMRT_ReadStatus(const char *filename, double *dloga)
{
 FILE *fid;
 int ierr;
 size_t Nread;
 
 /* Open the file */
 fid = fopen(filename, "r");
 if (fid == NULL) {
  perror(filename);
  printf("ERROR: %s: %i: Unable to open file %s\n",
         __FILE__, __LINE__, filename);
  return EXIT_FAILURE;
 }

 /* Read the timestep */
 Nread = fread(dloga, sizeof(double), 1, fid);
 if( Nread != 1 ) {
  printf("ERROR: %s: %i: Read %d of 1 elements from %s\n",
         __FILE__, __LINE__, (int)Nread, filename);
  return EXIT_FAILURE;
 }
 
 /* Close the file */
 ierr = fclose(fid);
 if (ierr != 0) {
  perror(filename);
  printf("ERROR: %s: %i: Unable to close file %s\n",
         __FILE__, __LINE__, filename);
  return EXIT_FAILURE;
 }

 return EXIT_SUCCESS;
}
