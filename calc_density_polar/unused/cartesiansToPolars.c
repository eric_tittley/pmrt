#include <math.h>

#include "calc_polardensity.h"

/*Used in differential map - not needed for actual calculation of density field*/
vec cartesiansToPolars(vec in, vec offset)
{
 float x = in.e1 - offset.e1;   /*allows shift of origin to be at center of particles positions, which range from 0 to 1 */
 float y = in.e2 - offset.e2;
 /*float z = in.e3 - offset.e3; */
 float r = sqrt(x * x + y * y);
 float theta;

 y *= -1.0;
 if (x >= 0 && y >= 0)
  theta = 1.5 * M_PI + atan(x / y);
 else if (x >= 0 && y < 0)
  theta = -atan(y / x);
 else if (x < 0 && y < 0)
  theta = 0.5 * M_PI + atan(x / y);
 else if (x < 0 && y >= 0)
  theta = 1.0 * M_PI - atan(y / x);
 /* if (x>=0 && y>=0) theta = atan(x / y);
  * else if (x>=0 && y<0) theta = 0.5 * M_PI - atan(y / x);
  * else if (x <0 && y<0) theta = M_PI + atan(x / y);
  * else if (x<0 && y>=0) theta = 1.5 * M_PI - atan(y / x);  */
 vec out = { r, theta, 0.0f };
 return out;
}
