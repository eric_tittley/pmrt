#include "config.h"

#include <stdio.h>

#include "calc_polardensity.h"

/*Used for testing only*/
void printlist(LINK head)
{
 if (head == NULL)
  printf(" NULL\n");
 else {
  printf(" (%i, %i, %li) -->", head->x, head->y, head->weight);
  printlist(head->next);
 }
}
