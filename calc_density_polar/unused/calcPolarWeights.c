#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "calc_polardensity.h"

/*possibly split this into separate file - do not want to calculate this too often - ideally only once per grid size*/
/*This method should output to a file so that it can be used multiple times - checks could be improved but works*/
int calcPolarWeights(int nr, int ntheta, int nx, int ny, float xoffset,
                     float yoffset, long seed, long points)
{
 int i, j, x, y;
 float randR, randT, dr, dtheta, weight;
 dr = (M_SQRT1_2 / nr);
 dtheta = (2 * M_PI / ntheta);
 polarCellWeights polargrid[nr][ntheta];
 long cartesianCount[5 * nx][5 * ny];
 float polarcheck[nr];
 long p, q, qlimit, pointsInIntersection;
 LINK current, previous;
 FILE *ofp;
 char outFileName[] = "weightdata_rxxxx_txxxx_xxxxx_yxxxx";
 qlimit = 100;
 printf
  ("Entering calcPolarWeights: No of points %ld * %ld. X offset: %f Y offset %f\n",
   qlimit, points, xoffset, yoffset);
 fflush(stdout);
 for (i = 0; i < nr; i++) {
  polarcheck[nr] = 0;
  for (j = 0; j < ntheta; j++) {        /*Set up polargrid */
   polargrid[i][j].r = i;
   polargrid[i][j].theta = j;
   polargrid[i][j].numbercells = 0;
   polargrid[i][j].numberpoints = 0;
   polargrid[i][j].firstCell = NULL;
  }
 }
 for (i = 0; i < 5 * nx; i++) {
  for (j = 0; j < 5 * ny; j++) {
   cartesianCount[i][j] = 0;
  }
 }

 for (q = 0; q < qlimit; q++) {
  for (p = 0; p < points; p++) {
   randR = M_SQRT1_2 * sqrt(ran1(&seed));
   randT = 2 * M_PI * ran1(&seed);
   i = (int) (randR / dr);
   j = (int) (randT / dtheta);
   polargrid[i][j].numberpoints++;
   polarcheck[i] += 1.0f;
   x = (int) ((float) xoffset + ((float) (nx) * randR * cos(randT)));
   y = (int) ((float) yoffset + ((float) (ny) * randR * sin(randT)));
   cartesianCount[x + (2 * nx)][y + (2 * ny)] += 1;
   if (polargrid[i][j].firstCell != NULL) {
    current = polargrid[i][j].firstCell;
    previous = polargrid[i][j].firstCell;
    int flag = 0;
    for (; (current != NULL) && (flag != 1); current = current->next) {
     previous = current;
     if (current->x == x && current->y == y) {
      current->weight += 1;
      flag = 1;
     }
    }
    if (flag == 0) {
     previous->next = malloc(sizeof(cellList));
     previous->next->x = x;
     previous->next->y = y;
     previous->next->weight = 1;
     previous->next->next = NULL;
     polargrid[i][j].numbercells++;
    }
   } else {
    polargrid[i][j].firstCell = malloc(sizeof(cellList));
    polargrid[i][j].firstCell->x = x;
    polargrid[i][j].firstCell->y = y;
    polargrid[i][j].firstCell->weight = 1;
    polargrid[i][j].firstCell->next = NULL;
    polargrid[i][j].numbercells = 1;
   }
  }
  /*printf("\nq = %d\n", q); */
 }


 /*
  * for (i = 0; i<nr; i++){
  * printf("i = %d total = %f\n", i, polarcheck[i]/(M_PI*(i+1)*(i+1)-M_PI*i*i));
  * } */

 /*Check weights add up properly */
 /*for (i = 0; i<nr; i++) for (j = 0; j<ntheta; j++) {
  * float expectedtotal = (i+1)*10;
  * float actualsum = 0;
  * LINK current = polargrid[i][j].firstCell;
  * for (; (current != NULL); current = current -> next){
  * actualsum += current -> weight;
  * }
  * printf("Cell (%d, %d) %f\n", i, j, expectedtotal/actualsum);
  * } */

 /*structure of output file header contains nr, ntheta, nx, ny */
 /*Then contains for each r, theta noCells, followed by corresponding weights (as normalised floats) */

 sprintf(outFileName, "weightdata_r%d_t%d_x%d_y%d.wgt", nr, ntheta, nx, ny);
 ofp = fopen(outFileName, "w");
 fwrite(&nr, 4, 1, ofp);
 fwrite(&ntheta, 4, 1, ofp);
 fwrite(&nx, 4, 1, ofp);
 fwrite(&ny, 4, 1, ofp);
 /*float cartesianCellPoints = dx*dy*points*10/(0.5f*M_PI); */
 for (i = 0; i < nr; i++)
  for (j = 0; j < ntheta; j++) {
   fwrite(&polargrid[i][j].numbercells, 4, 1, ofp);
   current = polargrid[i][j].firstCell;
   for (; (current != NULL); current = current->next) {
    x = current->x;
    y = current->y;
    pointsInIntersection = (current->weight);
    weight = (float) (pointsInIntersection)
     / (cartesianCount[x + (2 * nx)][y + (2 * ny)]);
    fwrite(&x, 4, 1, ofp);
    fwrite(&y, 4, 1, ofp);
    fwrite(&weight, 4, 1, ofp);
   }
  }
 fclose(ofp);

 /*
  * LINK nextLink;
  * for (i = 0; i<nr; i++) {
  * for (j = 0; j<ntheta; j++) {
  * LINK current = polargrid[i][j].firstCell;
  * do {
  * nextLink = current->next;
  * free(current);
  * current=nextLink;
  * } while(current != NULL);
  * }
  * } */

 printf("\ncalc_polarweights successful - exiting...\n");
 return EXIT_SUCCESS;
}
