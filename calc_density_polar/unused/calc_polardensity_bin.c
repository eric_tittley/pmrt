/* HISTORY
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 */

#include "config.h"

#include <stdio.h>
#include <math.h>

#include "calc_polardensity.h"
#include "mpipm.h"
#include "RT_constants.h"
#include "calc_density.h"

int calc_polardensity_bin(const int me, float *r, float *polar_density,
                          int nr, int ntheta, long npart, float xoffset,
                          float yoffset, const float aa)
{
 float area, scale, rho_c, NpPolar, fraction;
 int a, b, i, j, jPlus, jMinus, npart2d;
 float dtheta, dr, drA, dthetaA, drB, dthetaB, dthetaC, drC, dthetaD, drD;
 float wA, wB, wC, wD, norm;
 float rpart, thetapart, rgrid, thetagrid, rc, thetac;
 int quadrant, sgnr, sgntheta;
#if VERBOSE >= 1
 int ierr;
 char filename[1024];
#endif

 vec cartesian, polar, originOffset;
 printf("Entering calc_polardensity_bin...\n");
 dtheta = 2 * M_PI / ntheta;
 dr = M_SQRT1_2 / nr;
 vec quadrantOffsets[] =
  { (vec){ 0.0, 0.0, 0.0}, (vec){ 1.0, 1.0, 0.0}, (vec){ 1.0, 0.0, 0.0},
    (vec){ 1.0,-1.0, 0.0}, (vec){-1.0, 1.0, 0.0}, (vec){-1.0, 0.0, 0.0},
    (vec){-1.0,-1.0, 0.0}, (vec){ 0.0, 1.0, 0.0}, (vec){ 0.0,-1.0, 0.0} };
 originOffset = (vec){xoffset, yoffset, 0.0};
 npart2d = 1 + (int) pow(Np, 2.0 / 3.0);
 for (a = 0; a < 3 * npart; a += 3) {
  cartesian = (vec){r[a], r[a + 1], r[a + 2]};
  if (cartesian.e3 < (1.0 / 64.0)) {
   for (b = 0; b < 9; b++) {
    polar = cartesiansToPolars(cartesian,
                               addVectors(originOffset, quadrantOffsets[b]) );
    if (polar.e1 < M_SQRT1_2) {
     i = (int) (polar.e1 * nr / M_SQRT1_2);
     j = (int) (polar.e2 * ntheta * 0.5) / M_PI;
     rpart = polar.e1;
     thetapart = polar.e2;
     rc = dr * (i + 0.5);
     thetac = dtheta * (j + 0.5);
     if (rc > rpart) {
      if (thetac > thetapart) {
       rgrid = dr * i;
       thetagrid = dtheta * (j);
       sgnr = 1;
       sgntheta = 1;
       quadrant = 1;
      } else {
       rgrid = dr * (i);
       thetagrid = dtheta * (j + 1);
       sgnr = 1;
       sgntheta = -1;
       quadrant = 3;
      }
     } else {
      if (thetac > thetapart) {
       rgrid = dr * (i + 1);
       thetagrid = dtheta * (j);
       sgnr = -1;
       sgntheta = 1;
       quadrant = 2;
      } else {
       rgrid = dr * (i + 1);
       thetagrid = dtheta * (j + 1);
       sgnr = -1;
       sgntheta = -1;
       quadrant = 4;
      }
     }
     dthetaA = 0.5 * dtheta + sgntheta * (thetapart - thetagrid);
     dthetaB = dthetaA;
     dthetaC = dtheta - dthetaA;
     dthetaD = dthetaC;
     drA = 0.5 * dr + sgnr * (rpart - rgrid);
     drB = dr - drA;
     drC = drA;
     drD = drB;
     wA = sqrt(rpart * rpart * dthetaA * dthetaA + drA * drA);
     wB = sqrt(rpart * rpart * dthetaB * dthetaB + drB * drB);
     wC = sqrt(rpart * rpart * dthetaC * dthetaC + drC * drC);
     wD = sqrt(rpart * rpart * dthetaD * dthetaD + drD * drD);
     norm = wA + wB + wC + wD;
     if (j == 0) {
      jPlus = j + 1;
      jMinus = ntheta - 1;
     } else if (j == (ntheta - 1) ) {
      jPlus = 0;
      jMinus = j - 1;
     } else {
      jPlus = j + 1;
      jMinus = j - 1;
     }

     if (quadrant == 1) {
      if (i != 0) {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i - 1 + nr * j]      += wB / norm;
       polar_density[i + nr * jMinus]     += wC / norm;
       polar_density[i - 1 + nr * jMinus] += wD / norm;
      } else {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + nr * jMinus]     += wC / norm;
      }
     } else if (quadrant == 2) {
      if (i != nr - 1) {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + 1 + nr * j]      += wB / norm;
       polar_density[i + nr * jMinus]     += wC / norm;
       polar_density[i + 1 + nr * jMinus] += wD / norm;
      } else {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + nr * jMinus]     += wC / norm;
      }
     } else if (quadrant == 3) {
      if (i != 0) {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i - 1 + nr * j]      += wB / norm;
       polar_density[i + nr * jPlus]      += wC / norm;
       polar_density[i - 1 + nr * jPlus]  += wD / norm;
      } else {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + nr * jPlus]      += wC / norm;
      }
     } else {
      if (i != nr - 1) {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + 1 + nr * j]      += wB / norm;
       polar_density[i + nr * jPlus]      += wC / norm;
       polar_density[i + 1 + nr * jPlus]  += wD / norm;
      } else {
       polar_density[i + nr * j]          += wA / norm;
       polar_density[i + nr * jPlus]      += wC / norm;
      }
     }
    }
   }
  }
 }
 reduce(&PM_Comm, nr*ntheta, polar_density);


 /* ********** SCALE THE DENSITY FROM NUMBER PER UNIT CELL AREA TO kg/m^3 ********** */

 if (me == MASTER) {
#if VERBOSE >= 1
  scale = 0;
  for (i = 0; i < nr * ntheta; i++) {
   scale += polar_density[i];
  }
  printf("calc_density: Np should equal sum(density)\n");
  printf("              Np=%i; sum(density)=%e;\n", Np, scale);
  fflush(stdout);
#endif
  /* Scale the density to the gas density */
  rho_c = 1.879e-26 * h * h * pow(aa, -3);      /* critical density kg/m^3 */

  /* NpPolar is required as the no particles is > Np due to the circular area
   * which is Pi/2 bigger than the square area.*/

  NpPolar = 0.0f;
  for (i = 0; i < nr * ntheta; i++) {
   NpPolar += polar_density[i];
  }

  printf("NpPolar: %f. 0.5*PI*Np: %f\n", NpPolar,
         0.5f * M_PI * ((float) npart2d));

  /*
   * V_box = pow(Width*Mpc,3.0);
   * V_cell= V_box/NCELLS;
   * scale = ( rho_c * omega_b * V_box ) / ( Np * V_cell );
   * * Following is  the same as the previous line
   * * (gas density for overdensity of 1) / ( average # of particles per cell )
   */
  /* kg/m^3 / particle */
  scale = (rho_c * omega_b) * ((float) nr * (float) ntheta / NpPolar);

  for (i = 0; i < nr; i++) {
   /* polar grid, so cells have different areas */
   area = (float) (2 * (float) i + 1) / (float) nr;
   fraction = scale / area;
   /* printf("Fraction: %e\n", fraction); */
   for (j = 0; j < ntheta; j++) {
    polar_density[i + nr * j] *= fraction;
   }
  }

#ifdef ENFORCE_MINIMUM_DENSITY
  /*Enforce a minimum density */
  for (i = 0; i < nr; i++) {
   for (j = 0; j < ntheta; j++) {
    if (polar_density[i + nr * j] <= 0.) {
     polar_density[i + nr * j] = 1e-35;
    }
   }
  }
#endif

#if VERBOSE >= 1
  printf("calc_density: rho_c=%e; omega_b=%e; scale=%e;\n", rho_c, omega_b,
         scale);
  printf("calc_density: h=%e; aa=%e;\n", h, aa);
  fflush(stdout);
#endif

#if VERBOSE >= 1
  /* Dump the density grid to file */
  strncpy(filename, "density.dat", 1023);
  ierr = dumpdensity(filename, polar_density, (size_t) NCELLS);
#ifdef DEBUG
  if (ierr != 0) {
   printf("ERROR: calc_density: Failed to dump density to file: %s\n",
          filename);
   return EXIT_FAILURE;
  }
#endif
#endif
 }
 /* end if me==MASTER */
 printf("calc_polardensity_bin	successful - Exiting.\n");
 return EXIT_SUCCESS;
}
