#include "calc_polardensity.h"

vec addVectors(vec a, vec b)
{
 vec c = { a.e1 + b.e1, a.e2 + b.e2, a.e3 + b.e3 };
 return c;
}
