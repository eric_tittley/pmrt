/* Take a cartesian grid of densities and weight file and computes a 2d polar
 * density grid. The cartesian grid is output from calc_density, NOT from the
 * N-body data (though code does exist which does this).
 *
 * ARGUMENTS
 *  Input, not modified
 *   nr		Number of cells radially
 *   ntheta	Number of cells angularly
 *   nx   	Number of cartesian cells in x dimension of density field
 *   ny		Number of cartesian cells in y dimension of density field
 *   density 	The cartesian density field, computed from calc_dens	
 *  Initialised
 *   polar_density   The new density field in circular polars
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if:	Unable to read input weights files
 *
 * TODO
 *  Rewrite calc_polardensity to generate output to memory
 *  Add command line prompt for weight file (not ideal, maybe specify
 *  elsewhere - argument of mpirun? Not sure how to do this)
 *  Split random no, weight method + differential map into own files? Do not
 *  want to run each time.
 *  May be better to make weight method entirely seperate program
 *
 * AUTHOR: Calum Brown
 *	Modifications by Eric Tittley
 *
 * HISTORY 
 *  06 07 07 Rewrote calc_polardensity to be more compatable to rest of PMRT
 *	code - unfinished
 *  06 10 31 
 *           
 */

#include "config.h"

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

#include "mpipm.h"
#include "RT_constants.h"

#include "calc_polardensity.h"

/*calc_polardensity - creates a density field in circular polars using a weight file and a cartesian density file.
  Cartesian density passed as pointer, weight file specified from command line. Output is pointer to new polar field. */

int calc_polardensity(int nr, int ntheta, int nx, int ny, float *density,
                      float *polar_density)
{
 printf("Entering calc_polardensity\n");
 /*Search for relevant weight file - if not found prompt */
 FILE *wfp;
 int i, j, overlappingCells, cell, x, y, count;
 int nxIn=nx, nyIn=ny, nrIn=nr, nthetaIn=ntheta;
 float weight;
 float areaPolarCell;
 float areaCartesianCell = (1.0f / (nx * ny));
 char weightFileName[] = "weightdata_rXXXX_tXXXX_xXXXX_yXXXX.wgt";
 sprintf(weightFileName, "weightdata_r%d_t%d_x%d_y%d.wgt", nr, ntheta, nx, ny);
 wfp = fopen(weightFileName, "r");
 if (!wfp) {
  printf("Unable to find weight data. Exiting\n");
  return EXIT_FAILURE;
 } else
  printf("Weight data found.\n");

 /*This needs to be rewritten to use pointers and 1d arrays */
 /* float densPolar[nr][ntheta]; *//*REWRITE THIS LINE */
 memset(polar_density, 0, nr * ntheta * sizeof(float));
 /* for (i = 0; i<nr; i++) for (j = 0; j<ntheta; j++) densPolar[i][j] = 0; *//*REWRITE THIS LINE */

 /*For each polar cell, read in its entry in the weight file and then read each entry in turn.
  * Then add (weight*dens[x][y]) to that cell. */
 fread(&nrIn, 4, 1, wfp);
 fread(&nthetaIn, 4, 1, wfp);
 fread(&nxIn, 4, 1, wfp);
 fread(&nyIn, 4, 1, wfp);
 if (nx != nxIn || ny != nyIn || nr != nrIn || ntheta != nthetaIn) {
  printf("Weight data file does not match specified grid parameters.\n");
  return EXIT_FAILURE;
 }
 overlappingCells = 0;
 for (i = 0; i < nr; i++) {
  for (j = 0; j < ntheta; j++) {
   areaPolarCell =
    ((M_PI * (i + 1) * (i + 1)) - (M_PI * i * i)) / (2 * ntheta * nr * nr);
   count = fread(&overlappingCells, 4, 1, wfp);
   if (count == 0) {
    printf
     ("ERROR: calc_polardensity: %i: Unable to read weights for i=%i, j=%i\n",
      __LINE__ - 3, i, j);
    return EXIT_FAILURE;
   }
   for (cell = 0; cell < overlappingCells; cell++) {
    fread(&x, 4, 1, wfp);
    fread(&y, 4, 1, wfp);
    fread(&weight, 4, 1, wfp);
    if (x >= nx)
     x -= nx;
    else if (x < 0)
     x += nx;
    if (y >= ny)
     y -= ny;
    else if (y < 0)
     y += ny;
    if (x >= nx)
     x -= nx;
    else if (x < 0)
     x += nx;
    if (y >= ny)
     y -= ny;
    else if (y < 0)
     y += ny;
    if (x >= nx)
     x -= nx;
    else if (x < 0)
     x += nx;
    if (y >= ny)
     y -= ny;
    else if (y < 0)
     y += ny;
    polar_density[i + nr * j] += weight * density[x + (nx * y)];


    if (polar_density[i + nr * j] <= 0) {
     printf("ERROR: Density <= 0. Polar Density: %5.3e r: %i theta: %i Array position: %d Weight: %f x: %d y: %d Cartesian Density: %f\n",
       polar_density[i + nr * j], i, j, i + nr * j, weight, x, y,
       density[x + nx * y]);
#ifdef ENFORCE_MINIMUM_DENSITY
     polar_density[i + nr * j] = ENFORCE_MINIMUM_DENSITY;
#endif
    }
   }
   polar_density[i + nr * j] *= (areaCartesianCell / areaPolarCell);
  }
 }

 printf("Success - Exiting calc_polardensity\n\n");
 return EXIT_SUCCESS;
}
