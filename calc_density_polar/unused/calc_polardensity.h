#ifndef _CALC_POLARDENSITY_H
#define _CALC_POLARDENSITY_H

typedef struct vec {            /*simple vector structure */
 float e1, e2, e3;
} vec;

typedef struct cellList {       /*Linked list object. Each structure one entry in list, storing cartesian x and y and value. */
 int x, y;
 long weight;
 float normWeight;              /*Weight is the number of points that have fallen in this cell for the parent polar cell */
 struct cellList *next;
} cellList;

typedef cellList *LINK;         /*Link to an entry in the list - pointer */

typedef struct polarCellWeights {       /*Polar grid Cell, with link to start of relevant list */
 int r, theta, numbercells;
 long numberpoints;
 LINK firstCell;
} polarCellWeights;

int calc_polardensity(int nr, int ntheta, int nx, int ny, float *density,
                      float *polar_density);

int calc_polardensity_mcbin(const int me, float * const r, const long npart,
                            const int nr, const int ntheta,
			    const float xoffset, const float yoffset, 
                            const int points, const float particleRadius,
                            const float aa, long * const seed,
			    float * const polar_density);

int calc_polardensity_bin(const int me, float *r, float *polar_density,
                          int nr, int ntheta, long npart, float xoffset,
                          float yoffset, const float aa);

int calcPolarWeights(int nr, int ntheta, int nx, int ny, float xoffset,
                     float yoffset, long seed, long points);

float ran1(long *idum);

vec cartesiansToPolars(vec in, vec offset);
vec addVectors(vec a, vec b);
void printlist(LINK head);

#endif
