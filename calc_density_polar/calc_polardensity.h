#ifndef _CALC_POLARDENSITY_H
#define _CALC_POLARDENSITY_H

#ifdef HAVE_CONFIG
# include "config.h"
#endif

#include "Logic.h"

typedef struct vec {            /*simple vector structure */
 double e1, e2, e3;
} vec;

int calc_polardensity(const int me, float *const r,
                      float *const v, 
                      const long npart, const int nr, const int ntheta,
		      const double xoffset, const double yoffset, 
                      const int points, const double particleRadius,
                      const double R_RT, const double slab_thickness,
                      const double aa, const double vunit,
		      long *const seed,
		      float *const polar_density,
		      float *const velocity_x,
		      float *const velocity_y);

double ran1(long *idum);

/*
* #ifndef S_SPLINT_S
* typedef unsigned char bool;
* #endif
*/

#endif
