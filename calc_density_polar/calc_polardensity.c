/*
 * ARGUMENTS
 *  Input, not modified
 *   me		My node.
 *   r		Positions of the particles (simulation units Lside).
 *   v		Velocities of the particles (simulation units Lside)
 *   nr		Number of radial grid points in density grid.
 *   ntheta	Number of azimuthal grid points in density grid.
 *   npart	Number of particles in this node.
 *   xoffset,yoffset	The x-y position on which to centre the polar grid.
 *			[0 to 1]
 *   points	Number of cloud particles to generate around each particle.
 *   particleRadius	The radius of the cloud around each particle in which
 *			to place the cloud particles.
 *   R_RT	The radius over which RT will be performed. [PM units]
 *   slab_thickness	The thickness of the slab. [PM units]
 *   aa		The current expansion factor.
 *   vunit	Constant to convert simulation units into km/s
 *  Input and modified.
 *   seed	A random seed for the cloud
 *  Initialised (Memory must have been allocated)
 *   polar_density	Grid of density (kg/m^3) [nr x ntheta]
 *   velocity_x    Grid of x velocities (km/s, if vunit is correct)
 *   velocity_y    Grid of y velocities (km/s, if vunit is correct)
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE If:
 *   Scale is NAN.
 *   Unable to dump the density grid to a file (VERBOSE only).
 *   Assignment to polar_density[] is out of bounds.
 *
 * NOTES
 *  Only the MASTER node returns with useful values in polar_density,
 *  velocity_x, and velocity_y.
 *
 * MACROS USED
 *  VERBOSE	Defaults to 0
 *  DEBUG	Defaults to undefined
 *  ENFORCE_MINIMUM_DENSITY	Defaults to undefined (same as 0.)
 *  CALC_VELOCITIES	Defaults to undefined
 *
 *  h		The Hubble factor, Ho = h 100 km/s/Mpc.
 *  omega_b	The critical density fraction for baryonic material.
 *
 * AUTHOR: Calum Brown
 *
 * MAINTAINED BY: Eric Tittley
 *
 * HISTORY
 *  06 09 01 Mature version
 *  07 02 05 Added check of bounds on polar_density[] assignments.
 *	Added floor()'s to evaluations of i & j.
 *	Introduced 'cell'.
 *	Modified calculation of theta to avoid 2pi (0 instead) and
 *	avoid divide-by-zero.
 *	Changed all floats to double since comparing with the M_* macros
 *	from math.h which are all double.
 *  07 04 16 Renamed calc_polardensity (was calc_polardensity)
 *	Added the calculation of velocity_x and velocity_y
 *  07 04 25 Finally got scale and rho_c correct.
 *	Don't loop over offsets if R_RT < 0.5
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 *  08 09 08 Set theta to 0 to avoid compiler warnings when DEBUG is not set.
 *	Addedkeyword.
 *  09 05 05 The area of a circle is NOT 2 pi R^2!  Calc_polardensity was
 *	producing densities half what they should be. Fixed "scale".
 * TODO
 */

#include "config.h"

#ifndef VERBOSE
 #define VERBOSE 0
#endif

#include <stdio.h>
#include <math.h>
#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
# define M_PI_2		1.57079632679489661923	/* pi/2 */
# define M_1_PI		0.31830988618379067154	/* 1/pi */
#endif

#include "calc_polardensity.h"
#include "mpipm.h"
#include "calc_density.h"
#include "RT_constants_configuration.h"
#include "RT_constants_cosmology.h"
#include "Logic.h"

int calc_polardensity(const int me, float *const r,
                      float *const v,
                      const long npart, const int nr, const int ntheta,
		      const double xoffset, const double yoffset, 
                      const int points, const double particleRadius,
                      const double R_RT, const double slab_thickness,
                      const double aa, const double vunit,
		      long  *const seed,
		      float *const polar_density,
		      float *const velocity_x,
		      float *const velocity_y)
{
 const int Ncells = nr*ntheta;
 double fraction, randR, randT, x, y, rsquared, scale, rho_c;
 double theta = 0;
 double particleMass = 1. / ((double) points);
 int A, B, C, i, j, cell, n_search_quads;
 vec cartesian, originOffset;
 /*this array should generalise to 3d easily - extend to all 27 sectors */
 vec quadrantOffsets[] = {
  (vec){ 0.0, 0.0, 0.0}, (vec){ 1.0, 1.0, 0.0}, (vec){ 1.0, 0.0, 0.0},
  (vec){ 1.0,-1.0, 0.0}, (vec){-1.0, 1.0, 0.0}, (vec){-1.0, 0.0, 0.0},
  (vec){-1.0,-1.0, 0.0}, (vec){ 0.0, 1.0, 0.0}, (vec){ 0.0,-1.0, 0.0}
 };
#ifdef CALC_VELOCITIES
 float weight;
#endif
#if VERBOSE > 0
 int ierr;
 char filename[1024];
#endif

 /* R^2, R_RT is the radius over which to perform RT. */
 const double R2 = R_RT*R_RT;

#if VERBOSE > 0
 printf("(%2i) npart %li Np %i\n", me, npart, Np);
 printf("(%2i) Entering calc_polardensity. points=%i; npart=%li; radius=%f\n",
   me, points, npart, particleRadius);
 (void)fflush(stdout);
#endif

 /* Set arrays to zero */
 memset(polar_density, 0, Ncells * sizeof(float));
#ifdef CALC_VELOCITIES
 memset(velocity_x, 0, Ncells * sizeof(float));
 memset(velocity_y, 0, Ncells * sizeof(float));
#endif

 /* originOffset = (vec){xoffset, yoffset, 0.0}; */
 originOffset.e1 = xoffset;
 originOffset.e2 = yoffset;
 if(R_RT > 0.5 ) {
  n_search_quads = 9;
 } else {
  n_search_quads = 1;
 }
 for (A = 0; A < (int)(3 * npart); A += 3) { /* Loop through all particles */
  /* If in the slab with r_z < slab_thickness */
  if( r[A + 2] < (float)slab_thickness ) {
   for (B = 0; B < n_search_quads; B++) { /* Loop through all neighbour quadrants */
    /* cartesian is the position of the particle wrt the origin */
    /*
    cartesian = (vec){
     (double)r[A]     - originOffset.e1 - quadrantOffsets[B].e1,
     (double)r[A + 1] - originOffset.e2 - quadrantOffsets[B].e2,
     (double)r[A + 2]};
    */
    cartesian.e1 = (double)r[A]     - originOffset.e1 - quadrantOffsets[B].e1;
    cartesian.e2 = (double)r[A + 1] - originOffset.e2 - quadrantOffsets[B].e2;
    cartesian.e3 = (double)r[A + 2];
    if ((cartesian.e1 * cartesian.e1 + cartesian.e2 * cartesian.e2) < R2) {
     /* Generate points points in a cloud of radius particleRadius */
     for (C = 0; C < points; C++) {
      randR = particleRadius * ran1(seed);
      randT = 2. * M_PI * ran1(seed);
      x = (randR * cos(randT)) - cartesian.e1;
      y = (randR * sin(randT)) - cartesian.e2;
      rsquared = (x * x + y * y);
      /* if the cloud particle is within r^2 < R^2, find that azimuth. */
      if (rsquared < R2) {
       x *= -1.0;
      #ifdef DEBUG
       theta=-999.;
      #endif
       if( (x >= 0.) && (y > 0.) ) {
        theta = 1.5 * M_PI + atan(x / y);
       } else if( (x >= 0.) && (y <= 0.) ) {
        if(x>1e-10) { /* For small x, atan(x)=pi/2 - x */
         theta = -atan(y / x);
	} else {
	 theta = M_PI_2;
	}
       } else if( (x < 0.) && (y < 0.) ) {
        theta = 0.5 * M_PI + atan(x / y);
       } else if( (x < 0.) && (y >= 0.) ) {
        theta = 1.0 * M_PI - atan(y / x);
       }
      #ifdef DEBUG
       if(theta < -998.) {
        printf("ERROR: calc_polardensity: theta not set\n");
	printf(" x=%6.3f; y=%6.3f\n",x,y);
	(void)fflush(stdout);
	return EXIT_FAILURE;
       }
      #endif
       /* The index along the ray.
        * i = r/r_max nr; r_max = R_RT */
       i = (int) floor( sqrt(rsquared)/R_RT * ((double) nr) );
       /* The azimuthal index
        * j = theta/theta_max ntheta; theta_max = 2 pi */
       j = (int) floor( theta * 0.5 * M_1_PI * ((double) ntheta) );
       cell = i + nr * j;
      #ifdef DEBUG
       if( (i<0) || (i>=nr) ) {
        printf("(%2i) ERROR: calc_polardensity: %i: i out of bounds. i=%i; nr=%i\n",
	       me, __LINE__, i, nr);
	printf(" sqrt(2.*rsquared)=%f\n",sqrt(2.*rsquared));
	(void)fflush(stdout);
	return EXIT_FAILURE;
       }
       if( (j<0) || (j>=ntheta) ) {
        printf("(%2i) ERROR: calc_polardensity: %i: j out of bounds. j=%i; ntheta=%i\n",
	       me, __LINE__, j, ntheta);
	printf(" theta * 0.5 * M_1_PI =%f\n",theta * 0.5 * M_1_PI);
	printf(" x=%f; y=%f; theta=%f\n",x,y,theta);
	(void)fflush(stdout);
	return EXIT_FAILURE;
       }
       if( (cell<0) || (cell>=Ncells) ) {
        printf("(%2i) ERROR: calc_polardensity: %i: cell out of bounds. cell=%i\n",
	       me, __LINE__, cell);
	printf("      i=%i; nr=%i; j=%i; ntheta=%i\n",i,nr,j,ntheta);
	(void)fflush(stdout);
	return EXIT_FAILURE;
       }
      #endif
       polar_density[cell] += (float)particleMass;
      #ifdef CALC_VELOCITIES
       velocity_x[cell] += v[A    ];
       velocity_y[cell] += v[A + 1];
      #endif
      }
     }
    }
   }
  }
 }

 /* Collect the data from all the Slaves */
 (void)reduce(&PM_Comm, Ncells, polar_density);
#ifdef CALC_VELOCITIES
 (void)reduce(&PM_Comm, Ncells, velocity_x);
 (void)reduce(&PM_Comm, Ncells, velocity_y);
#endif

#ifdef CALC_VELOCITIES
 if (me == MASTER) {
  /* Convert the sum of velocities into a mean velocity, and convert to km/s */
  for(i=0;i<Ncells;i++) {
   weight = (float)vunit / ( (float)points * polar_density[i] );
   velocity_x[i] *= weight;
   velocity_y[i] *= weight;
  }
 }
#endif

 /* ******** SCALE THE DENSITY FROM NUMBER PER UNIT CELL AREA TO kg/m^3 ****** */

 if (me == MASTER) {
  /* Scale the density to the gas density */
  rho_c = 1.879e-26 * h * h;      /* kg/m^3 */

/*
 * MassPerParticle = TotalMassInVolume / NumberParticlesInVolume
 * MassPerCell = NumberParticlesInCell * MassPerParticle
 * DensityPerCell = MassPerCell / VolumeOfCell
 * TotalMassInVolume = (Length*Mpc/h)^3 * rho_c * omega_b
 * NumberParticlesInVolume = Np
 * MassPerParticle   =  (Length*Mpc/h)^3 * rho_c * omega_b / Np
 * NumberOfParticlesInCell = polar_density
 * MassPerCell = polar_density[i] * (Length*Mpc/h)^3 * rho_c * omega_b / Np
 * VolumeOfCell(Radius[i]) = slab_thickness*Length*Mpc*aa/h
 *                          *2*pi*((Radius[i]+dR)^2-(Radius[i])^2) / ntheta
 * Radius[i] = (i/nr)*R_RT*Length*Mpc*aa/h
 * dR = R_RT*Length*Mpc*aa/h/nr
 * Radius[i]+dR = (i+1)/nr * R_RT*Length*Mpc*aa/h
 * (Radius[i]+dR)^2-(Radius[i])^2 = ((i+1)^2-i^2)*(R_RT*Length*Mpc*aa/h/nr)^2
 * VolumeOfCell(Radius[i]) = slab_thickness*pi*((i+1)^2-i^2)*R_RT^2
 *                          *(Length*Mpc*aa/h)^3/nr^2 / ntheta
 * DensityPerCell =  polar_density[cell] * (Length*Mpc/h)^3 * rho_c * omega_b / Np
 *                / ( slab_thickness*pi*((i+1)^2-i^2)*R_RT^2*
 *                   (Length*Mpc*aa/h)^3/nr^2 / ntheta )
 *                =  polar_density[cell] * rho_c * omega_b / Np * ntheta
 *                 / ( slab_thickness*pi*((i+1)^2-i^2)*R_RT^2*aa^3/nr^2 )
 * scale = rho_c * omega_b * ntheta * nr^2 / ( Np *slab_thickness*pi*R_RT^2*aa^3 )
 * DensityPerCell[cell] = polar_density[cell] * scale / ((i+1)^2-i^2)
 */

  scale =   rho_c * omega_b * ntheta * nr*nr 
          / ( Np * slab_thickness * M_PI * R_RT*R_RT * aa*aa*aa );
 #ifdef DEBUG
  if(!(bool)isfinite(scale)) {
   printf("(%2i) ERROR: calc_polardensity: %i: scale=%e\n",me,__LINE__-3,
          scale);
   printf("	 rhoc_c=%5.3e; omega_b=%6.4f; nr=%i; ntheta=%i; Np=%i; slab_thickness=%5.3e; R_RT=%5.3f; aa=%5.3e\n",
          rho_c, omega_b, nr, ntheta, Np, slab_thickness, R_RT, aa);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 #endif


  for (i = 0; i < nr; i++) {
   /* polar grid, so cells have different areas */
   /* (i+1)^2 - i^2 = i^2 +2i +1 - i^2 = 2i + 1 */
   fraction = scale / (2.*(double)i + 1.);
   for (j = 0; j < ntheta; j++) {
    polar_density[i + nr * j] *= (float)fraction;
   }
  }

 #ifdef ENFORCE_MINIMUM_DENSITY
  /*Enforce a minimum density */
  for (i = 0; i < Ncells; i++) {
   if (polar_density[i] <= ENFORCE_MINIMUM_DENSITY ) {
    polar_density[i] = ENFORCE_MINIMUM_DENSITY;
   }
  }
 #endif

 #if VERBOSE >= 1
  printf("calc_polardensity: rho_c=%e; omega_b=%e; scale=%e;\n",
         rho_c, omega_b, scale);
  printf("calc_polardensity: h=%e; aa=%e;\n", h, aa);
  (void)fflush(stdout);
 #endif

 #if VERBOSE >= 1
  /* Dump the density grid to file */
  strncpy(filename, "density.dat", 1023);
  ierr = dumpdensity(filename, polar_density, (size_t) Ncells);
 #ifdef DEBUG
  if (ierr != 0) {
   printf("ERROR: calc_polardensity: Failed to dump density to file: %s\n",
          filename);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 #endif
 #endif

 } /* end if me==MASTER */
#if VERBOSE >= 1
 printf("(%2i) calc_polardensity successful - Exiting.\n",me);
 (void)fflush(stdout);
#endif
 return EXIT_SUCCESS;
}
