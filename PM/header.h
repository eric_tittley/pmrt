#ifndef _PM_HEADER_
#define _PM_HEADER_

typedef struct {
 int npart;		       /* Total number of particles. */
 int nsph;		       /* Number of gas particles.   */
 int nstar;		       /* Number of star particles.  */
 float aa;		       /* Scale factor. */
 float softlen; 	       /* Gravitational softening    */
} FileHeader;

#endif
