/*
 * HISTORY
 *  03 12 16 Eric Tittley
 *	Constants removed from mpipm.h since globals should only be
 *	instantiated once.
 *  04 03 25 omega_m and omega_v are set in RT.h
 *  06 04 13 Added PM_Comm.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 */

#include "mpipm.h"
#include "RT_constants_cosmology.h" /* for omega_m and omega_v */

/* Some "global" cosmological parameters. */
double omega_k;
double lside[Ndim];	/* Length of each side in units of ? */
int Np;			/* Number of particles in all nodes */
int Ng;			/* Number of grid cells per side for the PM code */
int MaxPart;		/* Maximum number of particles in a slab. 2*Np/nproc */

/* The MPI Communicator for the PM component. */
MPI_Comm PM_Comm;
