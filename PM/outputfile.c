/* Output the particle positions to file
 *
 * ARGUMENTS
 *  Input, not modified
 *   me		MPI node number
 *   nproc	Number of processors
 *   np		Number of particles
 *   r		Particle positions
 *   v		Particle velocities
 *   aa		Expansion factor
 *   fname	Base file name
 *   files	Number of files to which to write the output.
 *		This needs to be a factor of the number of processors.
 *		Each file will have data from nproc/files processors.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE
 *
 * FILE FORMAT (binary)
 *  eflag	(int)
 *  hsize	(int)
 *  header (structure FileHeader):
 *   npart	(int)	Number of particles
 *   nsph	(int)	Number of SPH particles (always 0 for now)
 *   nstar	(int)	Number of Star particles (always 0 for now)
 *   aa		(float) Scale factor
 *   softlen	(float) Gravitational softening length
 *  r		(floats) Ndim*npart particle positions
 *  v		(floats) Ndim*npart particle velocities
 *
 * AUTHOR: Martin White
 *
 * HISTORY (Eric Tittley)
 *  03 10 20 Removed unused variable nn.
 *  03 12 16 Explicit pointer type conversion on return from mymalloc.
 *  04 03 04 Don't append .bin to file names.
 *  04 05 24 Return EXIT_FAILURE, not 1 on error.
 *	Return an int.
 *	Explicitly include system header files.
 *  04 07 21 Added extra comments.
 *	Don't exit(), return instead.
 *  04 08 11 Removed definition of FileHeader to header.h.
 *  04 09 10 All arguments are constant except files.  np is constant in
 *	in principle, but MPI_Allgather messes that up.
 *  05 04 20 Made the output serial, since there may be issues with NFS and
 *	nfs locking.
 *	Reordered some of the code, while tracking down the problem.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 */

#ifdef HAVE_CONFIG
#include "config.h"
#else
#define VERBOSE 0
#endif

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <mpi.h>

#include "mpipm.h"
#include "header.h"

int outputfile(const int me, const int nproc, int np, const float *r,
               const float *v, const double aa, const char *fname, int files)
{
 FileHeader header;
 char namebuf[BUFSIZ];
 int first, myfile;
 /* int procsperfile; */
 int npfile=0, nskip1=0, nskip2=0;
 int i, nwrote, eflag, hsize;
 int *np_list, *myfile_list;
 FILE *fp;

 /* For serializing the output */
 MPI_Status status;
 int dummy=1;
 const int TAG = 10;
 const int MASTER = 0;

 if (me == MASTER && nproc < files) {
  printf("Number of processors must be larger or equal to number");
  printf("of output files. Only %d files will be used.\n", nproc);
  files = nproc;
 }

 MPI_Barrier(PM_Comm);

 if (files > 1) {
  myfile = (int) floor( (float) files * ( (float)me / (float)nproc ) );
  /* The name of the file I write to. */
  sprintf(namebuf, "%s.%02d", fname, myfile);
 } else {
  myfile = 0;
  sprintf(namebuf, "%s", fname);
 }

#if VERBOSE >= 1
 printf("(%i) outputfile: Writing to file %i\n",me,myfile); fflush(stdout);
#endif

 np_list = (int *) mymalloc(nproc * sizeof(int));
 myfile_list = (int *) mymalloc(nproc * sizeof(int));
 
 /* Array np_list contains a list of the number of particles each process
  * represents. */
 MPI_Allgather(&np, 1, MPI_INT, np_list, 1, MPI_INT, PM_Comm);

 /* myfile_list is the list of files to which each process is writing.
  * If myfile_list[i] = j, then process i is writing to file j.
  * Clearly j <= i. */
 MPI_Allgather(&myfile, 1, MPI_INT, myfile_list, 1, MPI_INT, PM_Comm);

 /* Find the first processor that is sharing my file. It needs to open the file
  * and write the header.  The variable 'first' stores the result. */
 for (first = 0; first < nproc; first++) {
  if (myfile == myfile_list[first]) {
   break;
  }
 }

 /* Find the number of particles in all processes writing to my file: npfile
  * Find the number that will be written before me in the file:       nskip1
  * Find the number written after me in the file:                     nskip2 */
 for (i = 0; i < nproc; i++) {
  if (myfile == myfile_list[i]) {
   npfile += np_list[i];
   if (i < me)
    nskip1 += np_list[i];
   if (i > me)
    nskip2 += np_list[i];
  }
 }
#if VERBOSE >= 1
 printf("(%i) npfile=%i; nskip1=%i; nskip2=%i;\n",me,npfile,nskip1,nskip2);
 fflush(stdout);
#endif

 /* Only one processor at a time, to prevent NFS problems */
 if (me == first) { /* We need to open file and write the header */
  /* Open File */
  if ((fp = fopen(namebuf, "w")) == NULL) {
   printf("Error opening %s for writing.\n", namebuf);
   fflush(stdout);
   perror("fopen");
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Compile and write header */
  header.npart = npfile;
  header.nsph = 0;
  header.nstar = 0;
  header.aa = (float)aa;
  header.softlen = (float)1.5 / (float)Ng;
  eflag = 1;
  hsize = sizeof(FileHeader);
  fwrite(&eflag, 1, sizeof(int), fp);
  fwrite(&hsize, 1, sizeof(int), fp);
  fwrite(&header, 1, hsize, fp);
  /* Write coordinates. */
  nwrote = fwrite(r, sizeof(float), Ndim * np, fp);
  if (nwrote != Ndim * np) {
   printf("Error writing r to file %s\n", fname);
   printf("Tried to write %d, actually got %d\n", Ndim * np, nwrote);
   fflush(stdout);
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Skip over range in r that will be written by other processes */
  fseek(fp, sizeof(float) * Ndim * nskip2, SEEK_CUR);
  /* write velocities */
  nwrote = fwrite(v, sizeof(float), Ndim * np, fp);
  if (nwrote != Ndim * np) {
   printf("Error writing v to file %s\n", fname);
   printf("Tried to write %d, actually got %d\n", Ndim * np, nwrote);
   fflush(stdout);
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Make room for rest of data */
  fseek(fp, sizeof(float) * Ndim * nskip2, SEEK_CUR);
  /* Close file */
  fclose(fp);
  /* Let the other processes write their share one at a time */
  for(i=me+1;i<nproc;i++) { /* Loop over remaining processes */
   if (myfile_list[i] == myfile) { /* If process shares my file */
    /* Tell the process it is its turn */
#if VERBOSE >= 1
    printf("(%i) Telling %i to write\n",me,i); fflush(stdout);
#endif
    MPI_Ssend(&dummy,1,MPI_INT, i, TAG, PM_Comm);
    /* Block until received */
    MPI_Recv(&dummy,1,MPI_INT, i, TAG, PM_Comm, &status);
    /* The other process is done writing */
   }
  } /* end loop over remaining processes */
 } else { /* I don't go first.  Wait until I'm told I can go. */
  /* Block until received */
  MPI_Recv(&dummy,1,MPI_INT, first, TAG, PM_Comm, &status);
#if VERBOSE >= 1
  printf("(%i), %i has told me it is my turn\n",me,first); fflush(stdout);
#endif
  if ((fp = fopen(namebuf, "r+")) == NULL) {
   printf("Error opening %s for appending.\n", namebuf);
   fflush(stdout);
   perror("fopen");
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Skip over the header */
  fseek(fp, 2 * sizeof(int) + sizeof(FileHeader), SEEK_CUR);
  /* Skip what has already been written */
  fseek(fp, sizeof(float) * Ndim * nskip1, SEEK_CUR);
  /* Write coordinates. */
  nwrote = fwrite(r, sizeof(float), Ndim * np, fp);
  if (nwrote != Ndim * np) {
   printf("Error writing r to file %s\n", fname);
   printf("Tried to write %d, actually got %d\n", Ndim * np, nwrote);
   fflush(stdout);
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Skip over range in r that has yet to be written */
  fseek(fp, sizeof(float) * Ndim * nskip2, SEEK_CUR);
  /* Skip range in v that has been written */
  fseek(fp, sizeof(float) * Ndim * nskip1, SEEK_CUR);
  /* write velocities */
  nwrote = fwrite(v, sizeof(float), Ndim * np, fp);
  if (nwrote != Ndim * np) {
   printf("Error writing v to file %s\n", fname);
   printf("Tried to write %d, actually got %d\n", Ndim * np, nwrote);
   fflush(stdout);
   free(myfile_list);
   free(np_list);
   return EXIT_FAILURE;
  }
  /* Skip to end of file */
  fseek(fp, sizeof(float) * Ndim * nskip2, SEEK_CUR);
  /* Close file */
  fclose(fp);
  /* Confirm I've written my share */
#if VERBOSE >= 1
  printf("(%i), I'm telling %i I'm done writing.\n",me,first);
  fflush(stdout);
#endif
  MPI_Ssend(&dummy,1,MPI_INT, first, TAG, PM_Comm);
 }
 
 free(myfile_list);
 free(np_list);

 return EXIT_SUCCESS;
}
