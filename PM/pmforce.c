/*
 * HISTORY (Eric Tittley)
 *  03 12 16 Explicit type conversion
 *  04 01 12 Reordered assignement onto density grid to optimize location
 *	in memory.
 *  04 03 25 omega_m and omega_v are set in constants.c
 *  05 05 06 Report amount of memory being allocated.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 */

#include <math.h>

#include "mpipm.h"
#include "RT_constants_configuration.h" /* for MASTER */
#include "RT_constants_cosmology.h" /* for omega_m and omega_v */

void pmforce(float r[], double aa, float force[], int me, int nproc, int np,
             rfftwnd_mpi_plan fftfplan, rfftwnd_mpi_plan fftiplan)
/* Calculate the force given the density field using the PM method.  */
{
 fftw_real *ddat=NULL, *Fdat=NULL, *work=NULL;
 fftw_complex *dgrid, *Fgrid;
 int xlim[2], ylim[2], fftsize;
 int leftpart, rightpart;
 float om, k2, ki;
 float dx, dy, dz, fj, scale;
 double ng, mdens, totm;
 int Ng2, nn, ix, iy, iz, ii[3], ip, idim;
 static int FirstCall=1;
 MPI_Status status;

#if VERBOSE > 1
 printf("(%2i) pmforce: Got to.\n",me);
 fflush(stdout);
#endif

 ng = 0.9999999 * Ng;
 Ng2 = Ng + 2;

 /* Calculate Omega_M at aa. */
 om = (float)(omega_m / (omega_m + omega_v * aa * aa * aa + omega_k * aa));
 totm = Np;
 mdens = totm / pow((double) Ng, 3.0) / Ls;

 /* Create the arrays and their complex counterparts.  Leave some room for
  * shadow grid cells. */
 rfftwnd_mpi_local_sizes(fftfplan, &xlim[1], &xlim[0], &ylim[1], &ylim[0],
                         &fftsize);
 fftsize += Ng * Ng2;
 ddat = (fftw_real *) mymalloc(fftsize * sizeof(fftw_real));
 Fdat = (fftw_real *) mymalloc(fftsize * sizeof(fftw_real));
#ifdef USEWORK
 work = (fftw_real *) mymalloc(fftsize * sizeof(fftw_real));
#endif

 if ( (me == MASTER) && (FirstCall==1) ) {
  FirstCall=0;
#ifdef USEWORK
  printf("pmforce: Each processor allocated %7.2f M for FFT.\n",
         (double)(3 * fftsize * sizeof(fftw_real)) / pow(2.,20));
#else
  printf("pmforce: Each processor allocated %7.2f M for FFT.\n",
         (double)(2 * fftsize * sizeof(fftw_real)) / pow(2.,20));
#endif
  fflush(stdout);
 }

 dgrid = (fftw_complex *) & ddat[0];
 Fgrid = (fftw_complex *) & Fdat[0];

#if VERBOSE > 1
 printf("(%2i) pmforce: Filling the density grid.\n",me);
 fflush(stdout);
#endif
 /* Fill the density grid. */
 for (ix = 0; ix < fftsize; ix++)
  ddat[ix] = Fdat[ix] = 0;
 for (nn = 0; nn < np; nn++) {
  ix = (int) floor(ng * r[Ndim * nn + 0]);
  if (ix >= xlim[0] && ix < xlim[0] + xlim[1]) {
   ix -= xlim[0];
   ii[0] = (ix + 1);
   dx = (float)ng * r[Ndim * nn + 0] - (float)(ix + xlim[0]);
   iy = (int) floor(ng * r[Ndim * nn + 1]);
   ii[1] = (iy + 1) % Ng;
   dy = (float)ng * r[Ndim * nn + 1] - (float)iy;
   iz = (int) floor(ng * r[Ndim * nn + 2]);
   ii[2] = (iz + 1) % Ng;
   dz = (float)ng * r[Ndim * nn + 2] - (float)iz;
   ddat[(Ng * Ng2 * ix + Ng2 * iy + iz)] +=
    (1.0 - dx) * (1.0 - dy) * (1.0 - dz);
   ddat[(Ng * Ng2 * ix + Ng2 * iy + ii[2])] += (1.0 - dx) * (1.0 - dy) * dz;
   ddat[(Ng * Ng2 * ix + Ng2 * ii[1] + iz)] += (1.0 - dx) * dy * (1.0 - dz);
   ddat[(Ng * Ng2 * ix + Ng2 * ii[1] + ii[2])] += (1.0 - dx) * dy * dz;
   ddat[(Ng * Ng2 * ii[0] + Ng2 * iy + iz)] += dx * (1.0 - dy) * (1.0 - dz);
   ddat[(Ng * Ng2 * ii[0] + Ng2 * iy + ii[2])] += dx * (1.0 - dy) * dz;
   ddat[(Ng * Ng2 * ii[0] + Ng2 * ii[1] + iz)] += dx * dy * (1.0 - dz);
   ddat[(Ng * Ng2 * ii[0] + Ng2 * ii[1] + ii[2])] += dx * dy * dz;
  }

  else {
   printf("ERROR: pmforce: %d has particle %d, ix=%d, not assigned to grid.\n", me, nn,
          ix);
   fflush(stdout);
   MPI_Abort(PM_Comm, 1234);
  }
 }
#if VERBOSE > 1
 printf("(%2i) pmforce: Sharing the shadow regions.\n",me);
 fflush(stdout);
#endif

 /* Now share the shadow regions among the processes.  Copy them into the
  * buffer regions then add them to the zeroth row. */
 rightpart = (me + 1) % nproc;
 leftpart = (me - 1 + nproc) % nproc;
 if (me & 1) {
  MPI_Ssend(&ddat[Ng * Ng2 * xlim[1]], Ng * Ng2 * sizeof(fftw_real), MPI_BYTE,
            rightpart, me, PM_Comm);
  MPI_Recv(Fdat, Ng * Ng2 * sizeof(fftw_real), MPI_BYTE, leftpart, leftpart,
           PM_Comm, &status);
 }

 else {
  MPI_Recv(Fdat, Ng * Ng2 * sizeof(fftw_real), MPI_BYTE, leftpart, leftpart,
           PM_Comm, &status);
  MPI_Ssend(&ddat[Ng * Ng2 * xlim[1]], Ng * Ng2 * sizeof(fftw_real), MPI_BYTE,
            rightpart, me, PM_Comm);
 }
 for (iy = 0; iy < Ng; iy++)
  for (iz = 0; iz < Ng; iz++)
   ddat[(Ng * Ng2 * 0 + Ng2 * iy + iz)] += Fdat[Ng2 * iy + iz];

#if VERBOSE > 1
 printf("(%2i) pmforce: Performing the FFT.\n",me);
 fflush(stdout);
#endif
 /* And do the FFT. */
 rfftwnd_mpi(fftfplan, 1, ddat, work, FFTW_TRANSPOSED_ORDER);
 scale = (float)(1.0 / (double)Ls / pow((double) Ng, 3.0) / mdens);

#if VERBOSE > 1
 printf("(%2i) pmforce: Calculating force components.\n",me);
 fflush(stdout);
#endif
 /* Calculate each component of the force. */
 for (idim = 0; idim < Ndim; idim++) {
 
  for (iy = ylim[0]; iy < ylim[0] + ylim[1]; iy++) {
   for (ix = 0; ix < Ls * Ng; ix++) {
    for (iz = 0; iz < Ng / 2 + 1; iz++) {
     if (ix > Ls * Ng / 2)
      ii[0] = ix - Ls * Ng;

     else
      ii[0] = ix;
     if (iy > Ng / 2)
      ii[1] = iy - Ng;

     else
      ii[1] = iy;
     if (iz > Ng / 2)
      ii[2] = iz - Ng;

     else
      ii[2] = iz;
     ki = (float)((double)ii[idim] / lside[idim]);       /* In units of 2Pi */
     k2 = (float)(  (double)ii[0] / lside[0] * (double)ii[0] / lside[0]
                  + (double)ii[1] / lside[1] * (double)ii[1] / lside[1]
                  + (double)ii[2] / lside[2] * (double)ii[2] / lside[2] );
     k2 *= (float)(2.0 * M_PI);            /* Also with a 2Pi removed */
     if (k2 > 0.0) {
      ip = Ls * Ng * (Ng / 2 + 1) * (iy - ylim[0]) + (Ng / 2 + 1) * ix + iz;
      Fgrid[ip].re =  ki / k2 * 1.5 * om * dgrid[ip].im * scale * (-1);
      Fgrid[ip].im = -ki / k2 * 1.5 * om * dgrid[ip].re * scale * (-1);
     }
    }
   }
  }

  if (ylim[0] == 0) Fgrid[0].re = Fgrid[0].im = 0.;
  rfftwnd_mpi(fftiplan, 1, Fdat, work, FFTW_TRANSPOSED_ORDER);

  /* Now share the zero regions among the processes.  Our first column
   * goes to the partner's shadow region, wrapping at Nproc. */
  MPI_Sendrecv(&Fdat[0], Ng * Ng2 * sizeof(fftw_real), MPI_BYTE, leftpart, me,
               &Fdat[Ng * Ng2 * xlim[1]], Ng * Ng2 * sizeof(fftw_real),
               MPI_BYTE, rightpart, rightpart, PM_Comm, &status);

  /* Pull the force back off the grid and onto the particles. */
  for (nn = 0; nn < np; nn++) {
   ix = (int) floor(ng * r[Ndim * nn + 0]);
   if (ix >= xlim[0] && ix < xlim[0] + xlim[1]) {
    ix -= xlim[0];
    ii[0] = (ix + 1);
    dx = (float)ng * r[Ndim * nn + 0] - (float)(ix + xlim[0]);
    iy = (int) floor(ng * r[Ndim * nn + 1]);
    ii[1] = (iy + 1) % Ng;
    dy = (float)ng * r[Ndim * nn + 1] - (float)iy;
    iz = (int) floor(ng * r[Ndim * nn + 2]);
    ii[2] = (iz + 1) % Ng;
    dz = (float)ng * r[Ndim * nn + 2] - (float)iz;
    fj  = (float)Fdat[(Ng * Ng2 * ix + Ng2 * iy + iz)] * ((float)1. - dx) * ((float)1. - dy) * ((float)1. - dz);
    fj += (float)Fdat[(Ng * Ng2 * ii[0] + Ng2 * iy + iz)] * dx * ((float)1. - dy) * ((float)1. - dz);
    fj += (float)Fdat[(Ng * Ng2 * ix + Ng2 * ii[1] + iz)] * ((float)1. - dx) * dy * ((float)1. - dz);
    fj += (float)Fdat[(Ng * Ng2 * ix + Ng2 * iy + ii[2])] * ((float)1. - dx) * ((float)1. - dy) * dz;
    fj += (float)Fdat[(Ng * Ng2 * ii[0] + Ng2 * ii[1] + iz)] * dx * dy * ((float)1. - dz);
    fj += (float)Fdat[(Ng * Ng2 * ii[0] + Ng2 * iy + ii[2])] * dx * ((float)1. - dy) * dz;
    fj += (float)Fdat[(Ng * Ng2 * ix + Ng2 * ii[1] + ii[2])] * ((float)1. - dx) * dy * dz;
    fj += (float)Fdat[(Ng * Ng2 * ii[0] + Ng2 * ii[1] + ii[2])] * dx * dy * dz;
    force[Ndim * nn + idim] = fj;
   } else {
    printf("ERROR: pmforce: %d has particle, ix=%d, not assigned to grid.\n", me, ix);
    fflush(stdout);
    MPI_Abort(PM_Comm, 1234);
   }
  }

 } /* for idim = 0,Ndim-1 */
#if VERBOSE > 1
 printf("(%2i) pmforce: Freeing memory.\n",me);
 fflush(stdout);
#endif
 free(work);
 free(Fdat);
 free(ddat);
#if VERBOSE > 1
 printf("(%2i) pmforce: Done.\n",me);
 fflush(stdout);
#endif
}
