#ifndef _MPIPM_H_
#define _MPIPM_H_

#include	<stdlib.h>
#include	<stdio.h>
#include	<string.h>
#include	<math.h>
#include	<mpi.h>

#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

/* Define some constants. */

#define		Ls		1       /* Should be 1 */
#define		Ndim		3
#define		MaxTime		3.1536e7  /* Time in seconds. */
/* #define		Nout		32 */ /* Don't need this anymore */

/* Used in inputfile */
#define MAXBUF	65536

/* Some "global" cosmological parameters. */
extern double omega_k;
extern double lside[Ndim];
extern int Np, Ng, MaxPart;

/* The MPI Communicator for the PM component. */
extern MPI_Comm PM_Comm;

/* ----------------- Application functions. ------------------------- */
/*@null@*//*@out@*/ void *mymalloc(size_t size);

/* ----------------- Utility functions. ------------------------- */
void slabdecompose(int nproc, int me, int *npart, int rnge[], float *r,
                   float *v);

/* ----------------------- I/O functions. ------------------------- */
int countdata(const int me, const char fbase[], const int Nfile);
int inputfile(const int me, const int nproc, int *npart, double *aa,
              /*@null@*/ float *r, /*@null@*/ float *v,
	      const char *froot, const int files);
int outputfile(const int me, const int nproc, int np, const float *r,
               const float *v, const double aa, const char *fname, int files);
int WriteStatus(const char *filename, const double *dloga);
int ReadStatus(const char *filename, double *dloga);

/* ----------------- PM functions. ------------------------- */
void pmforce(float r[], double aa, float force[], int me, int nproc, int np,
             rfftwnd_mpi_plan fftfplan, rfftwnd_mpi_plan fftiplan);

float periodic(float x, double L);

#endif /* ifndef _MPIPM_H_ */
