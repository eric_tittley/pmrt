/*
 * HISTORY (Eric Tittley)
 *  03 12 16 Explicit type conversions.
 *	Explicit pointer type conversions on return from mymalloc.
 *	Test me==MASTER instead of me==0.
 *  04 01 08 Comment statements
 *  04 01 09 Little changes trying to solve a problem that was not a problem.
 *	Mostly VERBOSE lines.
 *  04 03 04 Don't append .bin to file names.
 *  04 07 22 Return EXIT_SUCCESS/FAILURE instead of exit()'ing.
 *	Explicit includes.
 *  04 08 11 Added more informative error messages.
 *	Don't abort MPI here.  Let the calling routine do so.
 *  04 09 10 Declare constant arguments constant.
 *  05 04 20 Re-ordered a bit to clarify what happens when me == MASTER.
 *  05 05 03 Already had the header declared in header.h.  Include it and don't
 *	redefine FileHeader.
 *  05 08 25 Cosmetic changes.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 *  06 05 11 Cast sizeof() to (int) when in printf statement.
 */

#include <stdlib.h>
#include <stdio.h>

#include <mpi.h>

#include "config.h"
#include "mpipm.h"
#include "header.h"

/* Or include RT.h */
#define MASTER 0

int inputfile(const int me, const int nproc, int *npart, double *aa, float *r,
              float *v, const char *froot, const int files)
/* Reads the TreePM file format. */
{
 FileHeader header;
 char fname[BUFSIZ];            /* BUFSIZ = 8k as defined in stdio.h */
 int i, n, ngot, ifile;
 int *Nh;
 int np, slab, eflag, hsize;
 int BufPos;
 float pos[Ndim], vel[Ndim], *rh, *vh;
 FILE *fpr, *fpv;
 MPI_Status status;

#if VERBOSE >= 1
 printf("(%i) inputfile: Begin...\n", me); fflush(stdout);
#endif

 np = Np = 0;
 if (me == MASTER) {
  rh = (float *) mymalloc(nproc * MAXBUF * Ndim * sizeof(float));
  vh = (float *) mymalloc(nproc * MAXBUF * Ndim * sizeof(float));
  Nh = (int *) mymalloc(nproc * sizeof(int));
  for (i = 0; i < nproc; i++)
   Nh[i] = 0;
  for (ifile = 0; ifile < files; ifile++) {
   if (files > 1) {
    sprintf(fname, "%s.%02d", froot, ifile);
   } else {
    sprintf(fname, "%s", froot);
   }
   printf("Reading '%s' ...\n", fname);
   fflush(stdout);
   if ((fpr = fopen(fname, "r")) == NULL) {
    perror("fopen");
    printf("ERROR: inputfile: Can't open file `%s'\n", fname);
    return EXIT_FAILURE;
   }
  #if VERBOSE >= 1
   printf("reading eflag...\n");
   fflush(stdout);
  #endif
   ngot = fread(&eflag, sizeof(int), 1, fpr);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: inputfile: Can't read eflag from %s\n", fname);
    return EXIT_FAILURE;
   }
   if (eflag != 1) {
    printf("ERROR: inputfile: Endian flag (%i) is not 1.\n",eflag);
    return EXIT_FAILURE;
   }
  #if VERBOSE >= 1
   printf("reading hsize...\n");
   fflush(stdout);
  #endif
   ngot = fread(&hsize, sizeof(int), 1, fpr);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: inputfile: Can't read hsize from %s\n", fname);
    return EXIT_FAILURE;
   }
   if (hsize != sizeof(FileHeader)) {
    printf("ERROR: inputfile: Header size is stored as %d, expecting %d\n",
           hsize,(int)sizeof(FileHeader));
    return EXIT_FAILURE;
   }

   /* Read and unpack header. */
  #if VERBOSE >= 1
   printf("reading header...\n");
   fflush(stdout);
  #endif
   ngot = fread(&header, sizeof(FileHeader), 1, fpr);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: inputfile: Can't read header from %s\n", fname);
    return EXIT_FAILURE;
   }
   *aa = header.aa;
   if ((fpv = fopen(fname, "r")) == NULL) {
    perror("fopen");
    printf("ERROR: inputfile: Can't open file `%s'\n", fname);
    return EXIT_FAILURE;
   }
   fseek(fpv,
         2 * sizeof(int) + sizeof(FileHeader) +
         header.npart * Ndim * sizeof(float), SEEK_CUR);
  #if VERBOSE >= 1
   printf("reading pos & vel\n");
   fflush(stdout);
   printf("(%i) header.npart=%i\n", me, header.npart);
   fflush(stdout);
  #endif
   /* Read each particle in the file, one at a time.
    * For each particle, determine into which slab it goes, where there is one
    * slab for each processor.
    * rh and vh are buffers into which to store the positions.
    * Each is arranged like 
    * [s1p1, s1p2, ..., s1pMAXBUF, s2p1, s2p2, ... s2pMAXBUF, ..., snprocpMAXBUF]
    * where smpn => slab m, particle n.
    * (of course, 
    * Nh[i] stores the number of particles found so far in slab i.
    * BufPos, then, is the position in the buffer to store the particle.
    * If the slab is the MASTER's, then store directly in r & v.
    * Otherwise, add to the buffer at the position given by BufPos.
    * If Nh[i]==MAXBUF, then send the buffer to the appropriate node and reset
    * Nh[i] to 0.
    */
   for (n = 0; n < header.npart; n++) {
   #if VERBOSE >= 3
    printf("(%i) %i %i", me, header.npart, n);
    fflush(stdout);
   #endif
    /* Read in this particle's info */
    fread(pos, sizeof(float), Ndim, fpr);
    fread(vel, sizeof(float), Ndim, fpv);
    /* The slab in which this particle belongs. (Assume Ls=1) */
    slab = (int) floor((double) ((float) nproc * periodic(pos[0], 1.0)));
   #if VERBOSE >= 3
    printf(" slab=%i\n", slab);
    fflush(stdout);
   #endif
    /* If slab == 0, then don't bother storing in the buffer, just add directly
     * to r and v */
    if (slab == 0) {
     for (i = 0; i < Ndim; i++) {
      r[Ndim * np + i] = periodic(pos[i], lside[i]);
      v[Ndim * np + i] = vel[i];
     }
     np++;
     if (np >= MaxPart) {
      printf("ERROR: inputfile: MASTER: np=%d exceeded MaxPart=%d\n", np,
             MaxPart);
      return EXIT_FAILURE;
     }
    } else {                      /* slab != 0 */
     /* The position in the buffer to store this particle's info */
     BufPos = Ndim * (slab * MAXBUF + Nh[slab]);
    #if VERBOSE >= 3
     printf(" BufPos=%i\n", BufPos);
     fflush(stdout);
    #endif
     /* Store this particle's information in the buffer arrays */
     for (i = 0; i < Ndim; i++) {
      rh[BufPos + i] = periodic(pos[i], lside[i]);
      vh[BufPos + i] = vel[i];
     }
     Nh[slab]++; /* increment the counter for this slab */
     if (Nh[slab] == MAXBUF) {
     #if VERBOSE >= 1
      printf("(%2i) inputfile: Sending buffer data to node (slab) %i\n",me,slab);
      fflush(stdout);
     #endif
      MPI_Ssend(&Nh[slab], 1, MPI_INT, slab, slab, PM_Comm);
      MPI_Ssend(&rh[slab * Ndim * MAXBUF], Ndim * Nh[slab], MPI_FLOAT, slab,
                slab, PM_Comm);
      MPI_Ssend(&vh[slab * Ndim * MAXBUF], Ndim * Nh[slab], MPI_FLOAT, slab,
                slab, PM_Comm);
      Nh[slab] = 0;
     }
    }
   }
   fclose(fpv);
   fclose(fpr);
  }                             /* end loop over all files */
 #if VERBOSE >= 1
  printf("(%2i) inputfile: Sending last of the data...\n",me);
  fflush(stdout);
 #endif
  for (slab = 1; slab < nproc; slab++) {
   if (Nh[slab] > 0) {
    MPI_Ssend(&Nh[slab], 1, MPI_INT, slab, slab, PM_Comm);
    MPI_Ssend(&rh[slab * Ndim * MAXBUF], Ndim * Nh[slab], MPI_FLOAT, slab, slab,
              PM_Comm);
    MPI_Ssend(&vh[slab * Ndim * MAXBUF], Ndim * Nh[slab], MPI_FLOAT, slab, slab,
              PM_Comm);
    Nh[slab] = 0;
   }
   MPI_Ssend(&Nh[slab], 1, MPI_INT, slab, slab, PM_Comm);
  }
  free(Nh);
  free(vh);
  free(rh);
 #if VERBOSE >= 1
  printf("Done\n");
 #endif
  fflush(stdout);
 }

 else {                         /* I'm a SLAVE */

  do {
   MPI_Recv(&n, 1, MPI_INT, 0, me, PM_Comm, &status);
  #if VERBOSE >= 1
   printf("(%i) inputfile: Receiving %i particles\n", me, n);
   fflush(stdout);
  #endif
   if (n > 0) {
    MPI_Recv(&r[Ndim * np], Ndim * n, MPI_FLOAT, 0, me, PM_Comm,
             &status);
    MPI_Recv(&v[Ndim * np], Ndim * n, MPI_FLOAT, 0, me, PM_Comm,
             &status);
    np += n;
    if (np >= MaxPart) {
     printf("(%i) ERROR: inputfile: np=%d exceeded MaxPart=%d\n",
            me, np, MaxPart);
     return EXIT_FAILURE;
    }
   }
  } while (n);
 }
 MPI_Allreduce(&np, &Np, 1, MPI_INT, MPI_SUM, PM_Comm);
 *npart = np;
#if VERBOSE >= 1
 printf("(%i) inputfile: Done.\n", me); fflush(stdout);
#endif

 return EXIT_SUCCESS;
}

