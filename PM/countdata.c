/* Reads the files and counts the particles on each slab.
 *
 * ARGUMENTS
 *  Input, not modified
 *   me
 *   fbase
 *   Nfile
 *
 * RETURNS
 *  EXIT_FAILURE
 *  EXIT_SUCCESS
 *
 * SETS THE GLOBAL VARIABLE: Np (total number of particles)
 *
 * AUTHOR: Martin White
 * 
 * HISTORY (All Eric Tittley)
 *  03-10-20 Removed unused variables nn, status.
 *  04 03 04 Don't append .bin to file names.
 *  04 07 22 Include header files explicitly.
 *	Added comments.
 *	Return on error, don't exit.
 *	Deal with "backup" as a file base name.
 *  04 08 11 Added more informative error messages.
 *  04 09 10 The filename "backup" is never passed to countdata through fbase.
 *	Eliminated conditional.
 *	All the arguments are constant.
 *  05 05 03 Already had the header declared in header.h.  Include it and don't
 *	redefine FileHeader.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 *  06 05 11 Cast sizeof() to (int) when in printf statement.
 *	Argument nproc never used.  Remove.
 */

#include "config.h"

#include <stdio.h>
#include <string.h>

#include <mpi.h>

#include "mpipm.h"
#include "header.h"

int countdata(const int me, const char fbase[], const int Nfile)
{
 FileHeader header;
 int ifile, eflag, hsize, ngot;
 char fname[BUFSIZ];
 FILE *fp;

#if VERBOSE >= 1
 printf("(%i) countdata: Begin...\n", me); fflush(stdout);
#endif

 if (me == 0) {
  Np = 0;
  for (ifile = 0; ifile < Nfile; ifile++) {
   if (Nfile > 1) {
    sprintf(fname, "%s.%02d", fbase, ifile);
   } else {
    sprintf(fname, "%s", fbase);
   }
   if ((fp = fopen(fname, "r")) == NULL) {
    perror("fopen");
    printf("ERROR: countdata: unable to open %s\n", fname);
    return EXIT_FAILURE;
   }
   ngot = fread(&eflag, sizeof(int), 1, fp);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: countdata: unable to read eflag from file %s\n",fname);
    return EXIT_FAILURE;
   }
   if (eflag != 1) {
    printf("ERROR: countdata: Endian flag (%i) is not 1.\n",eflag);
    return EXIT_FAILURE;
   }
   ngot = fread(&hsize, sizeof(int), 1, fp);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: countdata: Unable to read hsize from file %s\n",fname);
    return EXIT_FAILURE;
   }
   if (hsize != sizeof(FileHeader)) {
    printf("ERROR: countdata: Header size is stored as %d, expecting %d\n",
           hsize,(int)sizeof(FileHeader));
    return EXIT_FAILURE;
   }
   ngot = fread(&header, sizeof(FileHeader), 1, fp);
   if (ngot != 1) {
    perror("fread");
    printf("ERROR: countdata: Unable to read header from file %s\n",fname);
    return EXIT_FAILURE;
   }
   Np += header.npart;
   printf("%s: %d particles.\n", fname, header.npart);
   fflush(stdout);
   fclose(fp);
  }
 }
 MPI_Bcast(&Np, 1, MPI_INT, 0, PM_Comm);
 
#if VERBOSE >= 1
 printf("(%i) countdata: Done.\n", me); fflush(stdout);
#endif

 return  EXIT_SUCCESS;
}
