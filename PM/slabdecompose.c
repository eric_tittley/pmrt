/*
 * HISTORY
 *  03 12 16 Eric Tittley
 *	Explicit type conversions implemented.
 *	Explicit pointer type conversions on return from mymalloc.
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 */

#include "mpipm.h"

#define		MAXSEND	65536
void slabdecompose(int nproc, int me, int *npart, int rnge[], float *r,
                   float *v)
/* Puts the particles into slabs  */
{
 int partner, ngrp, up, meshcell, PTask;
 int np, nn, nsend, nrecv, idim;
 float *rh, *vh;
 double ng;
 MPI_Status status;
 ng = 0.9999999 * Ng;
 np = *npart;
 MPI_Barrier(PM_Comm);
 rh = (float *) mymalloc(Ndim * MAXSEND * sizeof(float));
 vh = (float *) mymalloc(Ndim * MAXSEND * sizeof(float));
 for (PTask = 0; nproc > (1 << PTask); PTask++);
 for (ngrp = 1; ngrp < (1 << PTask); ngrp++) {
  partner = me ^ ngrp;
  if (partner < nproc) {
   if (me > partner)
    up = 1;

   else
    up = -1;
   for (nsend = 0, nn = 0; nn < np; nn++) {
    meshcell = (int) floor(r[Ndim * nn + 0] * ng);
    if (meshcell >= rnge[2 * partner] && meshcell < rnge[2 * partner + 1]
        && nsend < MAXSEND) {
     nsend++;
    }
   }

   do {
    if (nsend > 0) {
     nn = nsend = 0;

     do {
      meshcell = (int) floor(r[Ndim * nn + 0] * ng);
      if (meshcell >= rnge[2 * partner] && meshcell < rnge[2 * partner + 1]) {
       for (idim = 0; idim < Ndim; idim++) {
        rh[Ndim * nsend + idim] = r[Ndim * nn + idim];
        vh[Ndim * nsend + idim] = v[Ndim * nn + idim];
       }
       nsend++;
       np--;
       for (idim = 0; idim < Ndim; idim++) {
        r[Ndim * nn + idim] = r[Ndim * np + idim];
        v[Ndim * nn + idim] = v[Ndim * np + idim];
       }
      }

      else
       nn++;
     } while (nn < np && nsend < MAXSEND);
    }

    /* If I am "up", send my particles to my partner then receive. */
    if (up == 1) {
     MPI_Ssend(&nsend, 1, MPI_INT, partner, 0, PM_Comm);
     if (nsend > 0) {
      MPI_Ssend(rh, Ndim * nsend, MPI_FLOAT, partner, 1, PM_Comm);
      MPI_Ssend(vh, Ndim * nsend, MPI_FLOAT, partner, 1, PM_Comm);
     }
     MPI_Recv(&nrecv, 1, MPI_INT, partner, 0, PM_Comm, &status);
     if (nrecv > 0) {
      if (np + nrecv > MaxPart) {
       printf("task=%d, slabdecompose: ", me);
       printf("recv from %d, np=%d, MaxPart=%d\n", partner, np + nrecv,
              MaxPart);
       MPI_Abort(PM_Comm, 1200);
      }
      MPI_Recv(&r[Ndim * np], Ndim * nrecv, MPI_FLOAT, partner, 1,
               PM_Comm, &status);
      MPI_Recv(&v[Ndim * np], Ndim * nrecv, MPI_FLOAT, partner, 1,
               PM_Comm, &status);
      np += nrecv;
     }
    }

    else {
     MPI_Recv(&nrecv, 1, MPI_INT, partner, 0, PM_Comm, &status);
     if (nrecv > 0) {
      if (np + nrecv > MaxPart) {
       printf("task=%d, slabdecompose: ", me);
       printf("recv from %d, np=%d, MaxPart=%d\n", partner, np + nrecv,
              MaxPart);
       MPI_Abort(PM_Comm, 1202);
      }
      MPI_Recv(&r[Ndim * np], Ndim * nrecv, MPI_FLOAT, partner, 1,
               PM_Comm, &status);
      MPI_Recv(&v[Ndim * np], Ndim * nrecv, MPI_FLOAT, partner, 1,
               PM_Comm, &status);
      np += nrecv;
     }
     MPI_Ssend(&nsend, 1, MPI_INT, partner, 0, PM_Comm);
     if (nsend > 0) {
      MPI_Ssend(rh, Ndim * nsend, MPI_FLOAT, partner, 1, PM_Comm);
      MPI_Ssend(vh, Ndim * nsend, MPI_FLOAT, partner, 1, PM_Comm);
     }
    }
   } while (nsend == MAXSEND || nrecv == MAXSEND);
  }
 }
 free(vh);
 free(rh);
 *npart = np;
}
