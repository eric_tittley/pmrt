#include <math.h>

#include "mpipm.h"

float periodic(float x, double L)
/* Wraps "x" periodically into the range [0,L). */
{
 float tmp;
 tmp = x / (float)L;
 if (tmp >= 1.0)
  tmp = tmp - (float)floor(tmp);
 if (tmp < 0.0)
  tmp = (float)1.0 - (-tmp - (float)floor(-tmp));
 return ((float)L * tmp);
}
