#ifndef _CONFIG_H_
#define _CONFIG_H_

/* SUBVERSION tells the code that we are versioned by subversion.  Allows it
 * to report the version. */
#define SUBVERSION

/*** Code Control ***/

/* DEBUG traps and reports conditions that will lead to failure. Slows code
 * by about 1%, so safe to keep on. */
#define DEBUG

/* VERBOSE outputs extra diagnostics to STDOUT.  ** Don't undefine ** Just set
 * to 0, 1, or 2. */
#define VERBOSE 1

/* DEBUG and VERBOSE together produce code with runtime about 30% longer */

/* NO_RT disables performing RT.  Still compiled in, just not performed. */
//#define NO_RT

/* RT_MPI turns on MPI parallelisations in the RT code. */
//#define RT_MPI

/* RT_OPENMP turns on OpenMP parallelisations in the RT code. */
#define RT_OPENMP

/* RT_SERIAL compiles serial version of the RT code.  (dummy argument) */
//#define RT_SERIAL

/* CUDA turns on CUDA code. Requires RT_SERIAL? 
 * See also make.config */
//#define CUDA

/* The CUDA device to use. Normally this is 0, for the first. But if you have
 * more than one, you can hard-wire it here. */
#define CUDA_DEVICE 0
#define CUDA_MAX_DEVICES 1

/* The Support/libSupport.a exists */
#define SUPPORT_EXISTS

/* Uncomment the following to completely disable the restrict keyword throughout
 * the code. */
#define NORESTRICT

/* #define HAVE_BOOL */

/* TIME_CODE_SEGMENTS provides coarse timing of the code sections. */
#define TIME_CODE_SEGMENTS

/* FORCE_REDSHIFT_STOP forces the simulation to stop at a given redshift,
 * independent of any other setting (it sets 'af'). This is not recommended.
 * Useful for running the PM simulation prior to any RT source turns on,
 * like when setting up initial conditions files (set BACKUPS if so). */
#define FORCE_REDSHIFT_STOP 7.8

/* MAX_RT_ITERS limits the number of RT timesteps.  Useful only for testing.
 * DEFAULT should be commented out. */
//#define MAX_RT_ITERS 1

/* NO_EXPANSION: by default, an expanding universe is assumed.
 * For testing purposes, the assumption of expansion can be turned off.
 * NOT FULLY IMPLEMENTED <- What the feck does that mean? */
//#define NO_EXPANSION

/* Normalise the spectrum to produce a fixed number of ionising photons per
 * second (N_H_Ionising_photons_per_sec in constants.c) .  This overrules
 * whatever L_0 is set to in constants.c. Note, the spectrum is normalised
 * at z=100 so RT_Luminosity should give a result even if z_on < 100. */
/* #define SOURCE_NORMALISE_SPECTRUM */

/* ** The Geometry ** */
/* If PLANE_WAVE is defined, then the source intensity is not attenuated by
 * 1/r^2.  All cells along the same line of sight see the same flux, ignoring
 * attenuation from gas.  *Otherwise* the flux is attenuated by 1/r^2. */
//#define PLANE_WAVE

/* More stuff that should be in a parameter file: */
/*  Cooling terms */
/*   Use Spitzers.  Otherwise, Scholz & Waters, '91 is used [DEFAULT and PREFERABLE] */
// #define COOLING_COLLISIONAL_EXCITATION_OF_HYDROGEN__SPITZER

/* ** Gas Density Field ** */
/* ** ONE OF READ_DENSITY_FILE, CONSTANT_DENSITY, or CALC_DENSITY must be set */
/* Define READ_DENSITY_FILE with the filename of a density file to be read
   on each pass.  Usefull for testing purposes, it advances the front through
   an unchanging field.
   Turns off CONSTANT_DENSITY and SPREAD_GAS_PARTICLE */
// #define READ_DENSITY_FILE "del_1_swap.dat"

/* CONSTANT_DENSITY sets the gas to a constant density (as calculated in
 * calc_density) and fixes the grid in size (not co-moving) in RT_core. */
#define CONSTANT_DENSITY     1.540600e-26

/* CONSTANT_DENSITY_COMOVING unsets the fixed grid size set by
 * CONSTANT_DENSITY.  The density is then (aa^-3 * CONSTANT_DENSITY). */
//#define CONSTANT_DENSITY_COMOVING

/* CALC_DENSITY has the code calculate the gas density from the PM data. */
//#define CALC_DENSITY

/* CALC_DENSITY_POLAR has the code calculate the gas density from the PM data
 * onto a polar grid. */
//#define CALC_DENSITY_POLAR
#ifdef CALC_DENSITY_POLAR
 #define NR 256
 #define NTHETA 256
 #define XOFFSET 0.5
 #define YOFFSET 0.5
 #define POINTS 500
 #define PARTICLERADIUS 0.01
 #define SEED -1969
 #include <math.h>
 #define RT_RADIUS M_SQRT1_2
 //#define RT_RADIUS 1.0
 #define SLAB_THICKNESS 0.02
#endif

/* SPREAD_GAS_PARTICLE spreads the contribution of each particle to the
 * surrounding 8 cells (splitting each cell into NFLUX/NG slices.
 * Only relevant if CALC_DENSITY set. */
//#define SPREAD_GAS_PARTICLE

/* CONVOLVE_DENSITY smooths the low-density field of the gas.
 * Not to be used with CONSTANT_DENSITY and SPREAD_GAS_PARTICLE can probably
 * be disabled.
 * Only relevant if CALC_DENSITY set. */
//#define CONVOLVE_DENSITY

/* ENFORCE_MINIMUM_DENSITY prevents any cell from having less than the given
 * density.  NOTE: This option does not conserve mass but will prevent densities
 * of 0 which will halt RT_core.  A zero density is possible if 
 * CONVOLVE_DENSITY is not set. */
//#define ENFORCE_MINIMUM_DENSITY 1e-35
#ifndef ENFORCE_MINIMUM_DENSITY
# ifdef CALC_DENSITY_POLAR
#  define ENFORCE_MINIMUM_DENSITY 1e-35
# endif
#endif

/* CALC_VELOCITIES stores the velocity components along the line of sight and
 * perpendicular to the line of sight in the plane of the slice.
 * The velocities are treated the same as the gas density, so this is only
 * relevant if CALC_DENSITY is set.
 * Nowhere in the code are the velocities needed.  They are stored only for
 * post-processing spectra.  But they consume about 2/3 of the calc_density()
 * time and are invariant from one run to the next, given the same PM ICs. */
//#define CALC_VELOCITIES

/* ** The Grid ** */
/* LOS_ZERO finds the gas details along the 0 line of sight, only.  Good for
   testing purposes. */
//#define LOS_ZERO

/* LOS_SLICE finds the gas details along a slice. Incompatible with LOS_ZERO.
 * Set to the slice to take (counting from 0). */
//#define LOS_SLICE 128
#define LOS_SLICE 0
/* Is a non-zero value compatible with CALC_DENSITY_POLAR? SegFault if
 * non-zero. */

/* LOS_ALL find the gas details along every LOS in the volume */
//#define LOS_ALL

/* Define REFINEMENTS if you wish to split a cell with a large optical depth
 * into many slices. See RT_constants for terms that control the conditions
 * of the refinement. */
//#define REFINEMENTS
#define REF_MAX_NSLICES 64

/* Define USE_MCTfR if you wish to limit the cells in which refinements are
 * done to cells to which there is a low (or limited) optical depth set by
 * Max_cumulative_tau_for_refinement in constants.c .
 * The solution is more "exact" if USE_MCTfR is *not* defined.  Defining
 * USE_MCTfR and setting Max_cumulative_tau_for_refinement to something large
 * like 2048 can halve the execution time.  Setting it even smaller makes the
 * code much faster, but tragically the result has a greater error because it
 * limits preheating of the gas in front of the I-front. Harder spectra suffer
 * more. */
#define USE_MCTfR

/* Thin medium approximation:  tau is assumed to be zero everywhere (the input
 * spectrum is not modified by absorption along the ray. You won't normally
 * want this. */
//#define THIN_APPROX

/* Define TRACK_TIMESCALES if you wish to store the cooling, recombination, and
 * ionisation timescales to a file.
 * The fields output are:
 *  dt_RT	Timestep in the RT section.
 *  t_T		cooling timescale.
 *  t_I_H1	Ionisation timescale for HI
 *  t_I_He1	Ionisation timescale for HeI
 *  t_I_He2	Ionisation timescale for HeII
 *  t_R_H1	Recombination timescale to HI
 *  t_R_He1	Recombination timescale to HeI
 *  t_R_He2	Recombination timescale to HeII
 * A value of -1 means not applicable.
 * The file is appended to, so if it needs to be deleted if it already exists.
 * Writing this information to disc is very timeconsuming.
 */
//#define TRACK_TIMESCALES "timescales.output"

/* Define ADAPTIVE_TIMESCALE to allow the RT timescale to adaptively resolve
 * all important timescales.  If not defined, dt_RT = MIN_DT (set in
 * constants.c)  ADAPTIVE_TIMESCALE should ALWAYS be defined. Undefine for
 * testing purposes only (you can speed up the code dramatically by undefining
 * ADAPTIVE_TIMESCALE and setting MIN_DT to something large, like 1e12.) */
#define ADAPTIVE_TIMESCALE

/* Define USE_B_RECOMBINATION_CASE to use the alpha_B and beta_B coefficients
 * when the optical depth is large (or in a refinement) */
#define USE_B_RECOMBINATION_CASE

/* USE_B_RECOMBINATION_CASE_ONLY is usually wrong. Here for testing purposes */
//#define USE_B_RECOMBINATION_CASE_ONLY

/* Enforce a minimum temperature.  Useful if cooling excedes heating. */
//#define ENFORCE_MINIMUM_CMB_TEMPERATURE T_CMB

/* Define BACKUPS to have backups stored after every iteration. The run can be
   restarted from a backup state. */
//#define BACKUPS

/*** End of Code Control ***/

/**** The ODE integration method ****/
/* One of the following must be defined. */

/* Use Euler's method: y => y + tau * dy/dt */
#define INTEGRATE_ODE_EULER

/* Use a fourth-order Runge-Kutta method */
//#define INTEGRATE_ODE_RUNGE_KUTTA

/**** End of the ODE integration method ****/

/**** The quadrature integration method ****/
/* Simpson's rule. */
/* #define SIMPSONS */
/* Romberg integration */
// #define ROMBERG
/* Gaussian Quadrature: Gauss-Legendre for intervals 1 & 2,
 * Gauss-Laguerre for interal 3 */
#define GAUSSIAN_QUADRATURE

#ifdef SIMPSONS
#define N_Slices 100	/* Number of Simpson's Rule's slices */
#endif

#if (defined ROMBERG) || (defined GAUSSIAN_QUADRATURE)
/* Number of levels in the Romberg integration or the number of samples in the
 * Gauss-Legendre quadrature.  There are three spans:
 *  nu_0 to nu_1
 *  nu_1 to nu_2
 *  nu_2 to nu_end 
 * If Romberg: There will be 2^(N_levels-1)+1 function evaluations for each span.
 * If Gaussian-quadtrautre: There will be N_Level function evaluations for each
 *                    span but limited to 2, 4, 8, 16, or 32
 * Suggested:
 *  Power Law	 4 4  8 (verified 050815 for constant density)
 *  Mini-Quasar	16 8 32
 *  Star Burst	 4 4 (ignored)
 *  Hybrid 	16 8 32*/
# if    (defined SOURCE_SB_QSO_OFF_HYBRID ) \
     || (defined SOURCE_SB_QSO_HYBRID ) \
     || (defined SOURCE_MINI_QUASAR ) \
     || (defined CUDA_ALEX_MODIFIED_THIS)
#  define N_Levels_1 16
#  define N_Levels_2 8
#  define N_Levels_3 32
# else
#  define N_Levels_1 4
#  define N_Levels_2 4
#  define N_Levels_3 8
# endif
#endif

/*** End of quadrature integration method ***/

/* Constants that need to be defined since they are used in other constants. 
 * C does not allow global const variables to be declared using other global
 * const variables.
 */

/* WARNING!!! AT THE MOMENT, IF CALC_DENSITY_POLAR IS SET, THEN THE FOLLOWING
 * MUST BE SET.  THE CODE SHOULD BE CHANGED TO BE MORE GENERAL. */
#ifdef CALC_DENSITY_POLAR
 #define NFLUX NR
 #define NG    NTHETA
#else
 /* The resolution along the optical path. */
 #define NFLUX	256

 /* Number of grid zones on a side.
  * This may be most efficient if NG = N*(nproc-1) (unless LOS_SLICE)
  * NFLUX must be a multiple of NG
  * NG must be even */
 #define NG	256
#endif

/* N_OUTPUT_REDSHIFTS must equal the number of elements in OutputRedshift
 * defined in RT_constants.c */
#define N_OUTPUT_REDSHIFTS 2

/* Print the redshift to 6 digits, not 2 */
//#define LONG_FILE_LABEL

/* Check whether we are doing any MPI at all */
#if (!defined RT_MPI) && (defined CONSTANT_DENSITY)
# define NO_MPI
#endif

#endif /* _CONFIG_H_ */
