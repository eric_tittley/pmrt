DEFS += -DHAVE_CONFIG

include make.config

LOCAL_LIBS = libPMRT/libPMRT.a PM/libPM.a RT/libRT.a Support/libSupport.a \
	     calc_density_polar/libcalc_density_polar.a \
	     calc_density/libcalc_density.a

ifeq ($(CUDA),1)
 LOCAL_LIBS += RT/RT_CUDA/libRT_CUDA.a
endif

LIBS = $(LOCAL_LIBS) $(LIB_ARCH) $(LIB_MPI) $(LIB_FFTW) $(NVCC_LIBS) -lm

SUBDIRS = libPMRT PM RT Support calc_density calc_density_polar
ifeq ($(CUDA),1)
 SUBDIRS += RT/RT_CUDA
endif

PMRT_SRC = libPMRT/PMRT_BackupState.c \
           libPMRT/PMRT_ErrorCheck.c \
           libPMRT/PMRT_ErrorCheck_File.c \
           libPMRT/PMRT_ErrorCheck_MPI.c \
           libPMRT/PMRT_GetNewTimeStep.c \
           libPMRT/PMRT_ReadStatus.c \
           libPMRT/PMRT_WriteStatus.c

PM_SRC = PM/slabdecompose.c PM/countdata.c PM/inputfile.c \
         PM/outputfile.c PM/pmforce.c PM/globals.c PM/periodic.c

RT_SRC = RT/cosmological_time.c \
         RT/CrossSections.c \
         RT/expansion_factor_from_time.c \
         RT/Gauss_Laguerre.c \
	 RT/Gauss_Legendre.c \
	 RT/GL_weights.c \
	 RT/PhotonFlux.c \
	 RT/RecombCoef.c \
         RT/RT_AdaptiveTimescale.c \
         RT/RT_AllocateMemory.c \
         RT/RT_Cell_Copy.c \
	 RT/RT_Cell_Pack.c \
	 RT/RT_Cell_Unpack.c \
         RT/RT_constants_configuration.c \
	 RT/RT_constants_cosmology.c \
         RT/RT_constants_physical.c \
	 RT/RT_constants_source.c \
         RT/RT_Data_Allocate.c \
	 RT/RT_Data_Assign.c \
	 RT/RT_Data_Free.c \
         RT/RT_Data_MPI_Recv.c \
	 RT/RT_Data_MPI_Send.c \
         RT/RT_Data_Read.c \
	 RT/RT_Data_Write.c \
         RT/RT_InitialiseSpectrum.c \
	 RT/RT_InitializeCrossSectionArray.c \
         RT/RT_InterpolateFromGrid.c \
	 RT/RT_InterpolateFromTable.c \
         RT/RT_LastIterationUpdate.c \
         RT/RT_LightFrontPosition.c \
         RT/RT_LoadData.c \
         RT/RT_LoadSourceTable.c \
         RT/RT_Luminosity.c \
         RT/RT_MergeCell.c \
         RT/RT_NextTimeStep.c \
	 RT/RT_PhotoionizationRates.c \
	 RT/RT_PhotoionizationRatesFunctions.c \
	 RT/RT_PhotoionizationRatesIntegrate.c \
         RT/RT_ProcessCell.c \
	 RT/RT_ProcessCellRK.c \
         RT/RT_ProcessLOS.c \
         RT/RT_ProcessUnrefinedCells.c \
         RT/RT_Rates.c RT/RT_Rates_RK.c \
         RT/RT_RecombinationCoefficients.c \
         RT/RT_RefineCell.c \
         RT/RT_SaveData.c \
         RT/RT_SetRefinementDensity.c \
         RT/RT_SplitCell.c \
         RT/RT_UpdateCell.c

ifeq ($(MPI),1)
 RT_SRC += RT/RT_DistributeLOSs_mpi.c
else
 RT_SRC += RT/RT_DistributeLOSs.c
endif

SUPPORT_SRC = Support/ReportFileError.c Support/mymalloc.c Support/ShowConfig.c \
              Support/Create_PM_Communicator.c Support/ShowCosmology.c

CALC_DENSITY_SRC = calc_density/calc_density.c calc_density/dumpdensity.c \
                   calc_density/fields_allocate_memory.c \
		   calc_density/InitKernel.c calc_density/reduce.c

CALC_DENSITY_POLAR_SRC = calc_density_polar/calc_polardensity.c \
			 calc_density_polar/ran1.c

DEPS = dep.mak PM/dep.mak RT/dep.mak Support/dep.mak calc_density/dep.mak \
       constants.dep

INCS = $(INCS_BASE) $(INC_MPI) $(INC_FFTW) -I./libPMRT -I./PM -I./RT \
       -I./Support -I./calc_density -I./calc_density_polar

LD_FLAGS += $(OMPFLAGS)

all: $(EXEC)

$(EXEC): $(LOCAL_LIBS) PMRT.o dep.mak
	$(LD) -o $(EXEC) $(LD_FLAGS) PMRT.o $(LIBS)

install:
	mv $(EXEC) ../

libPMRT/libPMRT.a: $(PMRT_SRC) $(DEPS) RT/RT.h PM/mpipm.h
	$(MAKE) -C libPMRT

PM/libPM.a: $(PM_SRC) $(DEPS) RT/RT.h PM/mpipm.h
	$(MAKE) -C PM

RT/libRT.a: $(RT_SRC) $(DEPS) RT/RT.h PM/mpipm.h
	$(MAKE) -C RT

RT/RT_CUDA/libRT_CUDA.a:
	$(MAKE) -C RT/RT_CUDA

Support/libSupport.a: $(SUPPORT_SRC) $(DEPS) RT/libRT.a PMRT_SVN_REVISION.h
	$(MAKE) -C Support

calc_density/libcalc_density.a: $(CALC_DENSITY_SRC) calc_density/calc_density.h \
 $(DEPS) RT/RT.h PM/mpipm.h Support/Support.h
	$(MAKE) -C calc_density

calc_density_polar/libcalc_density_polar.a: $(CALC_DENSITY_POLAR_SRC) \
 calc_density_polar/calc_polardensity.h $(DEPS) RT/RT.h PM/mpipm.h \
 calc_density/calc_density.h
	$(MAKE) -C calc_density_polar

clean:
	-rm *.o
	-rm *~
	-rm Make.log
	-rm PMRT_SVN_REVISION.h
	-rm -f splint.log
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir clean ) \
	done

distclean: clean
	-rm dep.mak
	-rm $(EXEC)
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir distclean ) \
	done

PMRT_SVN_REVISION.h:
	echo "#define PMRT_SVN_REVISION" \"`svnversion .`\"  > PMRT_SVN_REVISION.h

RT/RT_SVN_REVISION.h:
	$(MAKE) -C RT RT_SVN_REVISION.h

$(DEPS): PMRT_SVN_REVISION.h RT/RT_SVN_REVISION.h
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir dep ) \
	done
	$(DEPEND) $(DEPEND_FLAGS) $(INCS) $(DEFS) PMRT.c > dep.mak

check:
	for dir in $(SUBDIRS); do \
	 ( $(MAKE) -C $$dir check ) \
	done
	splint +posixlib -nullderef -nullpass $(INCS) $(DEFS) PMRT.c >> splint.log

include dep.mak

