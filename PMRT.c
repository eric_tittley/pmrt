/* PMRRT PM merged with a RT code.
 * 
 * Author:	   PM: Martin White    (UCB)
 *                 RT: Eric Tittley (U of Edinburgh) & Jamie Bolton
 *               PMRT: Eric Tittley (U of Edinburgh)
 *
 * TODO
 *  - The calculation of the initial dump is seemingly in error (doesn't take
 *    into account when the source turns on) and, in any case, seemingly
 *    ignored.
 *  - Turn all config parameters set in RT_constants_* into things readable
 *    from a parameter file.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <math.h>

#include <mpi.h>

#include <sys/types.h>
#include <dirent.h>

#ifdef _OPENMP
#include <omp.h>
#endif

#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

#include "config.h"

#include "PMRT.h"
#include "mpipm.h"
#include "RT.h"
#include "RT_Data.h"
#include "RT_Luminosity.h"
#include "RT_SourceT.h"
#include "Support.h"
#include "calc_density.h"
#ifdef CALC_DENSITY_POLAR
#include "calc_polardensity.h"
#endif
#include "RT_constants_configuration.h"
#include "RT_constants_cosmology.h"
#include "RT_constants_physical.h"
#include "RT_constants_source.h"

#ifdef CUDA
# include "RT_CUDA/RT_CUDA_Binding.h"
# include "RT_CUDA/RT_CUDA_Util.h"
#endif

/* Macro for freeing memory.  Good for cleaning up after an error.
 * Don't worry if master or slave;
 * if a pointer is NULL, free does nothing. */
#define FREE_MEMORY_BASE \
  free(density);     \
  free(velocity_z);    \
  free(velocity_x);    \
  if(me==MASTER) { for(i=0;i<Nlos;i++) { RT_Data_Free(Data[i]); } free(Data); } \
  free(np_list);     \
  free(pmf);         \
  free(v);           \
  free(r);           \
  free(rnge);        \
  rfftwnd_mpi_destroy_plan(fftfplan); \
  rfftwnd_mpi_destroy_plan(fftiplan); \
  free(kernel); \
  if( (me<num_PM_procs) && (num_PM_procs<nproc) ) { \
   ierr = MPI_Comm_free(&PM_Comm); \
   if( ierr != MPI_SUCCESS) { \
    printf("ERROR: PMRT: %i: MPI_Comm_free failed\n",__LINE__); \
    exit(EXIT_FAILURE); \
   } \
  } \

#ifdef SOURCE_TABLE
 #define FREE_MEMORY_SOURCE_TABLE \
  free(source_nu);  \
  free(source_f);
#else
 #define FREE_MEMORY_SOURCE_TABLE
#endif

#define FREE_MEMORY FREE_MEMORY_BASE \
                    FREE_MEMORY_SOURCE_TABLE


/* ----------------- The main program. ------------------------- */
int main(int argc, char **argv)
{
 int i, j, iout=0;
 int me=-1, nproc=0, num_PM_procs=0;
 int * rnge=NULL;
 int * np_list=NULL;
 double aout[N_OUTPUT_REDSHIFTS];
 float * r=NULL;
 float * v=NULL;
 float * pmf=NULL;
 double aa=0., af, dloga, wtime[2], z, z_f, a_s, a_f;
 rfftwnd_mpi_plan fftfplan=NULL;
 rfftwnd_mpi_plan  fftiplan=NULL;

 /* For Radiative Transfer */
 int ierr;
 int FirstRTPassFlag = TRUE;
 int DoneFlag = FALSE;
 double t, t_f, dt;
 double dR=0., r0, length;
#ifndef CONSTANT_DENSITY
 const double stepmax=0.02;
 const double feps=0.05;
 int nn, fftsize=0;
 float Fmax=0., AFmax=0.;
 double fnow, dlogf;
 double ahlf;
 int xlim[2], ylim[2];
#endif
 int np=0;

#if (defined CALC_DENSITY) || (defined CALC_DENSITY_POLAR)
 double vunit;
#endif
 /* Gas density field */
 float *density = NULL;
 float *velocity_z = NULL;
 float *velocity_x = NULL;
 /* Data structure for the ionisation state for each Line of Sight */
 RT_Data ** Data = NULL;          /* All the data. Only the MASTER needs this. */
 FILE * FID_timescales = NULL;

 char hostname[BUFSIZ];
 const size_t LABEL_STR_SIZE=32;
 char label_string[LABEL_STR_SIZE];

#ifdef SOURCE_NORMALISE_SPECTRUM
 double sum=0.0,nu,dnu,L_0_new;
#endif

 /* For CONVOLVE_DENSITY */
 fftw_complex * kernel = NULL;

#if VERBOSE >= 1
 float mean_np, min_np, max_np;
#endif

#ifdef BACKUPS
 char filename[FILENAME_MAX];
 int NumPMFiles;
#endif

#ifdef TRACK_TIMESCALES
 char file_timescales[FILENAME_MAX];
#endif

 double t_Elapsed, t_PM=0, t_Density=0, t_RT=0;
#ifdef BACKUPS
 double t_Backups=0;
#endif

 const double t_on = cosmological_time(1.0 / (RED_0 + 1.0));

#ifdef CALC_DENSITY_POLAR
 const int nR=NR;
 const int nTheta=NTHETA;
 const float xoffset=XOFFSET;
 const float yoffset=YOFFSET;
 const int points=POINTS;
 const float particleRadius=PARTICLERADIUS;
 long seed=SEED;
 const double R_RT = RT_RADIUS;
 const double slab_thickness = SLAB_THICKNESS;
#endif

#ifdef LOS_ZERO
const int Nlos = 1;
#endif

#ifdef LOS_SLICE
# ifdef CALC_DENSITY_POLAR
const int Nlos = NTHETA;
# else
const int Nlos = NG;
# endif /* if CALC_DENSITY_POLAR else endif */
#endif /* if LOS_SLICE */

#ifdef LOS_ALL
const int Nlos = NG*NG;
#endif

/* The dimensions of the density grid
 *  NGx	Cells along the LOS
 *  NGy	Number of LOSs in a slice
 *  NGz	Number of Slices
 * Note, NGx doesn't have to equal NFLUX
 * and NGz * NGy doesn't have to equal NLOS
 */
#if ((defined CALC_DENSITY) || (defined READ_DENSITY_FILE))
 const int NGx = NG;
 const int NGy = NG;
 const int NGz = NG;
#endif
#ifdef CALC_DENSITY_POLAR
 const int NGx = NR;
 const int NGy = NTHETA;
 const int NGz = 1;
#endif
#ifndef CONSTANT_DENSITY
 const int Ncells = NGx * NGy * NGz;
#endif

#ifndef CONSTANT_DENSITY
 /* The offset to the point in density from which we should be taking
  * densities */
 int Offset;
#endif

 /* The tolerance factor */
 const double DTol = 1.00000001;

 /* Check the arguments */
#ifdef CONSTANT_DENSITY
 if (argc < 2) {
  for(i=0;i<argc;i++) {
   printf("%s ",argv[i]);
  }
  printf("\n");
  printf("Usage: PMRT start\n");
  printf("   or: PMRT backup\n");
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
 }
#else
 if (argc < 4) {
  for(i=0;i<argc;i++) {
   printf("%s ",argv[i]);
  }
  printf("\n");
  printf("Usage: PMRT <ic-file-root> <Nin-files> <Nout-files>\n");
  printf("   or: PMRT backup <Nin-files> <Nout-files>\n");
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
 }
#endif

#ifndef NO_MPI
 /* Initialize MPI stuff */
 ierr=MPI_Init(&argc, &argv);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Init failed",me,__LINE__);
 ierr=MPI_Comm_size(MPI_COMM_WORLD, &nproc);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Comm_size failed",me,__LINE__);
 ierr=MPI_Comm_rank(MPI_COMM_WORLD, &me);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Comm_rank failed",me,__LINE__);
#else
 me=MASTER;
#endif

#ifdef CUDA
 ierr=RT_CUDA_InitialiseDevices();
 PMRT_ErrorCheck(ierr,"Unable to initialise GPU",me,__LINE__);
#endif

#ifdef TIME_CODE_SEGMENTS
 t_Elapsed = Wtime();
#endif


 /****** RUN STATUS INFO ******/ 

 /* Print the host name of this process. */
 memset(hostname, 0, BUFSIZ);
 ierr=gethostname(hostname, BUFSIZ);
 PMRT_ErrorCheck(ierr,"gethostname failed",me,__LINE__);
 printf("(%2i) %s (PID=%i)\n", me, hostname, getpid());
 ierr=fflush(stdout);
#ifndef NO_MPI
 ierr=MPI_Barrier(MPI_COMM_WORLD);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Barrier failed",me,__LINE__);
#endif

 if(me==MASTER) {
  /* Repeat the command line */
 #ifdef CONSTANT_DENSITY
  printf("%s %s\n",argv[0],argv[1]);
 #else
  printf("%s %s %s %s\n",argv[0],argv[1],argv[2],argv[3]);
 #endif
 #ifndef NO_MPI
  printf("PMRT on %d processors.\n", nproc);
 #endif
 #ifdef _OPENMP
 #pragma omp parallel
  { printf(" OMP_NUM_THREADS = %i\n", omp_get_num_threads()); }
 #endif
  printf("\n");
  /* Display the configuration at compile time */
  ierr = ShowConfig();
  PMRT_ErrorCheck(ierr,"Check the compile-time configuration",me,__LINE__);
  ShowCosmology();
 }

 /****** END OF RUN STATUS INFO ******/ 

 /* Globals set only once, here. */
 omega_k = 1.0 - omega_m - omega_v;
 lside[0] = Ls;
 lside[1] = lside[2] = 1.0;
 wtime[0] = Wtime();
#ifndef CONSTANT_DENSITY
 /* Create the Communicator for all processes that deal with the particle data. */
 ierr=Create_PM_Communicator( &PM_Comm, &num_PM_procs );
 PMRT_ErrorCheck(ierr,"Unable to Create_PM_Communicator",me,__LINE__);
#endif

/************** THE FOLLOWING SHOULD BE IN THE PARAMETER FILE ***************/
 /* Set up Source */
 RT_SourceT Source;
 Source.LuminosityFunction = &RT_Luminosity_POWER_LAW_MINUS_ONE_HALF;
 Source.z_on  = 8.0;
 Source.z_off = -0.1;
 Source.L_0 = 1.65e+19; /*  */
 Source.T_BB  = 1e5; /* K */
 Source.nu_0 = 3.282e15; /* Hz */
/***************************************************************************/

 /* Initialise output expansion factors for output redshifts listed in RT.h */
 const double OutputRedshift[N_OUTPUT_REDSHIFTS] = {7.9, 7.8};
 for (i = 0; i < N_OUTPUT_REDSHIFTS; i++) {
  aout[i] = 1.0/(OutputRedshift[i]+1.0);
 }

 /* Initialise the timestep (dloga) */
 dloga = -1;
#ifdef BACKUPS
 if( strcmp(argv[1],"backup")==0 ) {
  if(me==MASTER) {
   ierr=snprintf(filename,FILENAME_MAX-1,"%s/Status.bak",Dir);
   if(ierr>(int)FILENAME_MAX) {
    PMRT_ErrorCheck_File(EXIT_FAILURE,"Path too long",Dir,me,__LINE__);
   }
   ierr = PMRT_ReadStatus(filename,&dloga);
   PMRT_ErrorCheck_File(ierr,"Unable to read",filename,me,__LINE__);
  }
 #ifndef NO_MPI
  ierr=MPI_Bcast(&dloga,1,MPI_DOUBLE,MASTER,MPI_COMM_WORLD);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Bcast failed",me,__LINE__);
 #endif
 }
#endif


#ifdef CONSTANT_DENSITY
 aa = 1./(RED_0+1.);
#else
#if VERBOSE >= 1
 if (me == MASTER) {
  printf("Opening input file...\n"); ierr=fflush(stdout);
 }
#endif
 if(me < num_PM_procs) { /* If I do PM work */
  /* Count the total number of particles (global variable Np) */
  if(strcmp(argv[1],"backup")==0) {
   ierr=countdata(me, PM_BAK, atoi(argv[2]));
   PMRT_ErrorCheck_File(ierr,"Unable to access input file",PM_BAK,me,__LINE__);
  } else {
   ierr=countdata(me, argv[1], atoi(argv[2]));
   PMRT_ErrorCheck_File(ierr,"Unable to access input file",argv[1],me,__LINE__);
  }
 }

 /* Set the mesh size (Ng is a global) */
 Ng = (int) (2 * pow((double) Np / Ls, 1.0 / 3.0) + 0.1);
 if ( (me == MASTER) && (((Ls * Ng) % num_PM_procs ) != 0)) {
  printf("ERROR: PMRT: Ls*Ng must be a multiple of the number of processors.\n");
  printf(" You tried Np=%i; Ls=%i; Ng=%i; num_PM_procs=%i;\n",
         Np, Ls, Ng, num_PM_procs);
  PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
 }
 if(nproc != num_PM_procs) {
  /* Non-particle processes won't have Np, so their Ng will be wrong. */
  ierr=MPI_Bcast(&Ng,1,MPI_INT,MASTER,MPI_COMM_WORLD);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Bcast failed",me,__LINE__);
 }
 
 /* Report info */
 if (me == MASTER) {
  printf("\nNp=%d, Ng=%d, feps=%f\n", Np, Ng, feps);
  printf("omega_m=%f, omega_v=%f\n\n", omega_m, omega_v);
  ierr=fflush(stdout);
 }

 if(me < num_PM_procs) { /* If I do PM work */
  /* Set up the FFTW plan files.  Both forward and inverse expect the
     dimensions to be Nx, Ny, Nz. */
  fftfplan =
   rfftw3d_mpi_create_plan(PM_Comm, Ls * Ng, Ng, Ng, FFTW_REAL_TO_COMPLEX,
                           FFTW_ESTIMATE | FFTW_IN_PLACE);
  fftiplan =
   rfftw3d_mpi_create_plan(PM_Comm, Ls * Ng, Ng, Ng, FFTW_COMPLEX_TO_REAL,
                           FFTW_ESTIMATE | FFTW_IN_PLACE);

  /* Work out the ranges on each processor. */
  memset(xlim,0,2);
  memset(ylim,0,2);
  rfftwnd_mpi_local_sizes(fftfplan, &xlim[1], &xlim[0], &ylim[1], &ylim[0],
                          &fftsize);
  xlim[1] += xlim[0];
  rnge = (int *) mymalloc(2 * num_PM_procs * sizeof(int));
  if(rnge==NULL) {
   PMRT_ErrorCheck(EXIT_FAILURE,"malloc for rnge failed",me,__LINE__);
  }
  ierr=MPI_Allgather(xlim, 2, MPI_INT, rnge, 2, MPI_INT, PM_Comm);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Allgather failed",me,__LINE__);
 }

 if(me < num_PM_procs) { /* If I do PM work */
  /* Allocate particle memory */
  MaxPart = (int) ceil(2.0 * (double) Np / (double) num_PM_procs);
  if (me == MASTER) {
   printf("Each processor is allocating %7.2f M for particle data.\n",
          (double)(  3 * Ndim * MaxPart * sizeof(float) 
	           + num_PM_procs * sizeof(int) ) / pow(2.,20));
   ierr=fflush(stdout);
  }
  r = (float *) mymalloc(Ndim * MaxPart * sizeof(float));
  v = (float *) mymalloc(Ndim * MaxPart * sizeof(float));
  pmf = (float *) mymalloc(Ndim * MaxPart * sizeof(float));
  np_list = (int *) mymalloc(num_PM_procs * sizeof(int));
 }

 if(me < num_PM_procs) { /* If I do PM work */
  /* Read ICs, this also sets Np. */
  if(strcmp(argv[1],"backup")!=0) {
   ierr=inputfile(me, num_PM_procs, &np, &aa, r, v, argv[1], atoi(argv[2]));
   if( (ierr==EXIT_FAILURE) || (r==NULL) || (v==NULL) ) {
    PMRT_ErrorCheck_File(EXIT_FAILURE,"Unable to read input PM file",
                         argv[1],me,__LINE__);
   }
  } else {
   ierr=inputfile(me, num_PM_procs, &np, &aa, r, v, PM_BAK, atoi(argv[2]));
   if( (ierr==EXIT_FAILURE) || (r==NULL) || (v==NULL) ) {
    PMRT_ErrorCheck_File(EXIT_FAILURE,"Unable to read input PM file",
                         PM_BAK,me,__LINE__);
   }
  }
 }
 ierr=MPI_Bcast(&aa, 1, MPI_DOUBLE, MASTER, MPI_COMM_WORLD);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Bcast failed",me,__LINE__);
#endif /* Ifdef CONSTANT_DENSITY ... else ... endif */

#ifndef FORCE_REDSHIFT_STOP
 /* Set the final output redshift */
 af = aout[N_OUTPUT_REDSHIFTS - 1];
#else
 af = 1./(FORCE_REDSHIFT_STOP+1.0);
#endif
 if(me == MASTER) printf("af = %f\n",af); ierr=fflush(stdout);

 /* Find the first output redshift which we have yet to reach */
 while ( (iout < N_OUTPUT_REDSHIFTS - 1) && (aa > aout[iout]) ) {
  /* printf("aa=%f; aout[%i]=%f\n",aa,iout,aout[iout]);  */
  iout++;
 }
 z = 1.0 / aa - 1.0;
 if (me == MASTER) {
  printf("z=%f; a=%f; Setting iout=%d; aout=%f\n", z , aa, iout, aout[iout]);
  ierr=fflush(stdout);
 }

 if(me == MASTER) {
  /* Radiative Transfer: Allocate memory for large arrays */
 #ifdef CALC_DENSITY_POLAR
  Data = RT_AllocateMemory(nTheta, nR);
 #else
  Data = RT_AllocateMemory(Nlos, NFLUX);
 #endif
  if (Data==NULL) {
   PMRT_ErrorCheck(EXIT_FAILURE,"Unable to allocate memory for RT data",
                   me,__LINE__);
  }
 }

#ifndef CONSTANT_DENSITY
 if(me < num_PM_procs) {
  /* Allocate memory for density & velocity fields. */
  ierr = fields_allocate_memory(me, Ncells, &density, &velocity_z, &velocity_x);
  PMRT_ErrorCheck(ierr,"Unable to allocate memory for calc_density data",
                  me,__LINE__);
 }
#endif

#ifdef BACKUPS 
 if( (me==MASTER) && (strcmp(argv[1],"backup")==0) && ( (1.0/aa-1.0) < RED_0 ) ) {
  /* Read the gas data if restarting a run from a backup state */
  FirstRTPassFlag=FALSE;
  ierr=snprintf(filename,FILENAME_MAX-1,"%s/RT.bak",Dir);
  if(ierr>(int)FILENAME_MAX) {
   PMRT_ErrorCheck_File(EXIT_FAILURE,"Path too long",Dir,me,__LINE__);
  }
  ierr=RT_LoadData(filename,Data,Nlos);
  if( (ierr==EXIT_FAILURE) || (Data==NULL) ) {
   PMRT_ErrorCheck_File(EXIT_FAILURE,"Unable to load backup RT data from file",
                        filename,me,__LINE__);
  }
 }
#endif

 /* Initialise the source spectrum */
 if( RT_InitialiseSpectrum(me, &Source) == EXIT_FAILURE ) {
  PMRT_ErrorCheck(EXIT_FAILURE,"Problem with the source spectrum",me,__LINE__);
 }

#ifdef CONVOLVE_DENSITY
 if(me < num_PM_procs) {
   /* radius is defined in constants.c */
  kernel = InitKernel(NG, radius, &PM_Comm);
  if(kernel == NULL) {
   PMRT_ErrorCheck(EXIT_FAILURE,"Unable to initialize kernel",me,__LINE__);
  }
 }
#endif
 
#ifdef TRACK_TIMESCALES
 if(me != MASTER) {
  /* Open the file to which we are going to send the timescale information.
   * Every node will have it's own FID, but they will be appending to the same
   * file. I don't yet know if this will work! */
  ierr=snprintf(file_timescales,FILENAME_MAX-1,"%s.%02i",TRACK_TIMESCALES,me);
  if(ierr>(int)FILENAME_MAX) {
   PMRT_ErrorCheck_File(EXIT_FAILURE,"String too long",TRACK_TIMESCALES,
                        me,__LINE__);
  }
  printf("(%2i) PMRT: Opening %s\n",me,file_timescales);
  ierr=fflush(stdout);
  FID_timescales = fopen(file_timescales,"w");
  if(FID_timescales == NULL) {
   PMRT_ErrorCheck_File(EXIT_FAILURE,"Unable to open file",file_timescales,
                        me,__LINE__);
  }
 }
#endif

#ifdef TIME_CODE_SEGMENTS
 t_Elapsed = Wtime();
#endif

 if(dloga < 0) {
#ifdef CONSTANT_DENSITY
  if(aout[iout]<af) {
   dloga = log(DTol * aout[iout] / aa);
   printf("%s: %d: dloga=%e; aout[%i]=%f; aa=%f\n",__FILE__,__LINE__,
 	  dloga,iout,aout[iout],aa);
  } else {
   dloga = log(DTol * af / aa);
   printf("%s: %d: dloga=%e; af=%f; aa=%f\n",__FILE__,__LINE__,
 	  dloga,af,aa);
  }
#else
  dloga = 0;
#endif
 }

 printf("%s: %d: dloga=%e; aout[%i]=%f; aa=%f\n",__FILE__,__LINE__,
        dloga,iout,aout[iout],aa);

#ifdef CONSTANT_DENSITY
 if( me==MASTER ) {
  const double HeliumMassFraction = 0.235; /* Should be an input parameter */
  const double density_to_n_H = (1.0 - HeliumMassFraction) / (mass_p + mass_e);
  double n_H_to_n_He=0.0;
  if(HeliumMassFraction < 1.0) {
   n_H_to_n_He = 0.25 * HeliumMassFraction / (1.0 - HeliumMassFraction);
  }
  for (j = 0; j < Nlos; j++) {
   for (i = 0; i < Data[j]->NumCells; i++) {
    Data[j]->D[i]    = CONSTANT_DENSITY;
    Data[j]->Dold[i] = CONSTANT_DENSITY;
    Data[j]->n_H[i] = density_to_n_H * CONSTANT_DENSITY;
    if(HeliumMassFraction > 0.) {
     if(HeliumMassFraction  < 1. ) {
      Data[j]->n_He[i] = n_H_to_n_He * Data[j]->n_H[i];
     } else {
      /* HeliumMassFraction==1 */
      Data[j]->n_He[i] = CONSTANT_DENSITY / ( 4.0 * (mass_p + mass_e) );
     }
    } else {
     Data[j]->n_He[i]=0.;
    }
   }
  }
 }
#endif

 /* ********************* START OF MAIN LOOP **************************** */
 do { /* while ( (aa <= af) && (wtime[1] - wtime[0] < MaxTime) ); */
#if VERBOSE >= 1
  printf("(%2i) PMRT: Beginning of while loop\n",me); ierr=fflush(stdout);
#endif

  if(dloga < 0.0) {
   printf("(%2i) WARNING: dloga < 0: dloga = %f\n",me,dloga);
   printf("     Setting to 0.001\n");
   dloga = 0.001;
   ierr=fflush(stdout);
  }

#ifndef CONSTANT_DENSITY
 #ifdef TIME_CODE_SEGMENTS
  t_Elapsed = Wtime();
 #endif

  if(me < num_PM_procs) {
   /************************* START OF PM SECTION **************************/
 #if VERBOSE >= 1
   printf("(%2i) PMRT: Beginning of PM section.\n",me); ierr=fflush(stdout);
 #endif
   for (nn = 0; nn < np; nn++) {
    for (i = 0; i < Ndim; i++) {
     r[Ndim * nn + i] =
      periodic((float)(r[Ndim * nn + i] + 0.5 * v[Ndim * nn + i] * dloga), lside[i]);
    }
   }
 #if VERBOSE >= 2
   printf("(%2i) PMRT: Calling slabdecompose.\n",me); ierr=fflush(stdout);
 #endif
   slabdecompose(num_PM_procs, me, &np, rnge, r, v);
 #if VERBOSE >= 2
   printf("(%2i) PMRT: Done slabdecompose.\n",me); ierr=fflush(stdout);
 #endif

   /* Calculate the PM force (pmf) on all particles */
 #if VERBOSE >= 2
   printf("(%2i) PMRT: Calling pmforce.\n",me); ierr=fflush(stdout);
 #endif
   pmforce(r, aa, pmf, me, num_PM_procs, np, fftfplan, fftiplan);
 #if VERBOSE >= 2
   printf("(%2i) PMRT: Done pmforce.\n",me); ierr=fflush(stdout);
 #endif

   /* Find the maximum force Fmax */
   Fmax = -1;
   for (nn = 0; nn < np; nn++) {
    AFmax = 0;
    for (i = 0; i < Ndim; i++) {
     AFmax += pmf[Ndim * nn + i] * pmf[Ndim * nn + i];
    }
    if (AFmax > Fmax) Fmax = AFmax;
   }

   ahlf = aa * exp(dloga / 2.0);
   fnow = sqrt(omega_m * ahlf + omega_v * ahlf * ahlf * ahlf * ahlf
               + omega_k * ahlf * ahlf);
   dlogf =
    0.5 / fnow * (omega_m + 4. * omega_v * ahlf * ahlf * ahlf + 2 * omega_k * ahlf);
   dlogf = (ahlf / fnow) * dlogf;
   for (nn = 0; nn < np; nn++) {
    for (i = 0; i < Ndim; i++) {
     v[Ndim * nn + i] *= (float)(1.0 - 0.5 * dlogf * dloga);
     v[Ndim * nn + i] += (float)(pmf[Ndim * nn + i] * dloga);
     v[Ndim * nn + i] /= (float)(1.0 + 0.5 * dlogf * dloga);
    }
   }
   for (nn = 0; nn < np; nn++) {
    for (i = 0; i < Ndim; i++) {
     r[Ndim * nn + i] = periodic(r[Ndim * nn + i] + (float)(0.5 * v[Ndim * nn + i] * dloga), lside[i]);
    }
   }
  } /* end if me < num_PM_proc */
  /************************* END OF PM SECTION ***************************/
 #ifdef TIME_CODE_SEGMENTS
  t_PM += Wtime() - t_Elapsed;
  t_Elapsed = Wtime();
 #endif
#endif /* ifndef CONSTANT_DENSITY */

#ifndef NO_RT
  /* *********************************************************************
   * ****************** The Radiative Transfer code **********************
   * *********************************************************************
   *
   * density	 Density grid
   * t  	 Time at this point in the simulation
   * dt 	 Timestep
   * Data	 Structure containing the status of the gas.
   */
 #if VERBOSE >= 1
 #ifndef NO_MPI
  ierr=MPI_Barrier(MPI_COMM_WORLD);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Barrier failed",me,__LINE__);
 #endif
  printf("(%2i) PMRT: Beginning of RT section.\n",me); ierr=fflush(stdout);
 #endif
  /* This is a mess.  All I want is the dt that corresponds to dloga.
   * Why is that so difficult? */
  dt = cosmological_time( aa * exp(dloga) ) - cosmological_time( aa ); /* s */
  z = (1.0 / aa) - 1.0;
  z_f = (1.0 / (aa * exp(dloga))) - 1.0;
  if (z_f < RED_0) { /* The source turns on at redshift RED_0 */
   /* Don't mess with aa, af, or dloga */
   /* Calculate the present time and the time step */
   a_f = aa * exp(dloga);
   if(z > RED_0) {
    /* Source turns on during this iteration, but not at the start. */
    a_s = 1/(RED_0+1);
    t = cosmological_time( a_s ); /* s */
    t_f = cosmological_time( a_f ); /* s */
    dt = t_f - t; /* s */
   } else {
    /* Source is already on, or turns on at the start of this iteration. */
    t = cosmological_time(aa ); /* s */
    if(dloga > 1e-9) {
     t_f = cosmological_time(a_f); /* s */
     a_s = aa;
     dt = t_f - t;
    } else {
     t_f = t;
     dt = 0.;
     a_s = a_f;
    }
   }
   if (me == MASTER) {
    printf("a=%8.6f; z=%10.6f; t=%8.6e; dt=%6.4e;\n", a_s, (1/a_s-1), t, dt);
    ierr=fflush(stdout);
   }
  #ifdef DEBUG
   if(dt < 0.) {
    printf("(%2i) ERROR: PMRT: dt < 0: dt=%f\n",me,dt);
    printf("     t=%e; t_f=%e; a_s=%f;\n",t,t_f,a_s);
    PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
   }
  #endif

  /* Set the dimensions of a RT LOS.
   *  r0 is the distance from the source to the source-end of the LOS
   *  length is the length of the LOS. */
  #if defined(CONSTANT_DENSITY) && !defined(CONSTANT_DENSITY_COMOVING)
   r0    = R_0   * Mpc; /* m */
   length = Length * Mpc; /* m */
  #else
   /* aa is the expansion factor at the end of the iteration.
    * Strictly, R[*] should grow during a single integration. */
   r0    = R_0   * a_f / h * Mpc; /* m */
   length = Length * a_f / h * Mpc; /* m */
  #endif
  #ifdef CALC_DENSITY_POLAR
   length = length*R_RT;
  #endif
  
   if (me == MASTER) {
    dR = length / Data[0]->NumCells;   /* Length of LOS segment (m) */
    for (i = 0; i < Nlos; i++) {
     /* Find the proper distance of each cell from the source, using the
      * corrected length of a 1D slice given the redshift. (h^-1 comoving Mpc)
      * R[i] is the position of the lower edge of the cell. 
      * R[i]+dR is the position of the upper edge. */
     for (j = 0; j < Data[i]->NumCells; j++) {
      Data[i]->R[j] = r0 + ((double)j)*dR;
     }
    }
   }

   /* ** The gas density ** */
  #ifdef TIME_CODE_SEGMENTS
   t_Elapsed = Wtime();
  #endif
  #ifdef READ_DENSITY_FILE
   if (me == MASTER) {
    ierr = read_density_file(me,READ_DENSITY_FILE,a_f,Ncells,density,
                             velocity_z,velocity_x);
    PMRT_ErrorCheck_File(ierr,"read_density_file failed",
                         READ_DENSITY_FILE,me,__LINE__);
   }
  #endif
   if(me < num_PM_procs) {
   #if (defined CALC_DENSITY) || (defined CALC_DENSITY_POLAR)
    /* Calculate the velocity unit */
    vunit=Vunit(aa,Length,omega_m,omega_v,0.0);
    if(me == MASTER ) {
     printf("vunit=%8.6e;\n",vunit);
    }
   #endif
  #ifdef CALC_DENSITY
   #if VERBOSE >= 1
    printf("(%2i) Calling calc_density.\n",me); ierr=fflush(stdout);
   #endif
    ierr = calc_density(PM_Comm, np, NG, a_f, vunit, r, v, kernel,
                        density, velocity_x, velocity_z);
    PMRT_ErrorCheck(ierr,"calc_density failed",me,__LINE__);
   #if VERBOSE >= 1
    printf("(%2i) Returned from calc_density.\n",me);
    ierr=fflush(stdout);
   #endif
  #endif /* CALC_DENSITY */
   #ifdef CALC_DENSITY_POLAR
    /* Note: velocity_z is actually velocity_y */
    ierr = calc_polardensity(me, r, v, np, nR, nTheta, xoffset, yoffset,
    		             points, particleRadius, R_RT, slab_thickness,
			     a_f, vunit, &seed, density, velocity_x, velocity_z);
    PMRT_ErrorCheck(ierr,"calc_polardensity failed",me,__LINE__);
   #endif /* CALC_DENSITY_POLAR */
   } /* end if me < num_PM_procs */
  #ifdef TIME_CODE_SEGMENTS
   t_Density += Wtime() - t_Elapsed;
   t_Elapsed = Wtime();
  #endif

   if( (me==MASTER) ) {
   #ifdef CONSTANT_DENSITY_COMOVING
    const double HeliumMassFraction = 0.235; /* Should be an input parameter */
    const double density_to_n_H = (1.0 - HeliumMassFraction) / (mass_p + mass_e);
    double n_H_to_n_He=0.0;
    if(HeliumMassFraction < 1.0) {
     n_H_to_n_He = 0.25 * HeliumMassFraction / (1.0 - HeliumMassFraction);
    }
    for (j = 0; j < Nlos; j++) {
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->D[i]    *= CONSTANT_DENSITY*pow(1+z_f,3);
      Data[j]->Dold[i] *= CONSTANT_DENSITY*pow(1+z  ,3);
      Data[j]->n_H[i] = density_to_n_H * Data[j]->D[i];
      if(HeliumMassFraction > 0.) {
       if(HeliumMassFraction  < 1. ) {
   	Data[j]->n_He[i] = n_H_to_n_He * Data[j]->n_H[i];
       } else {
   	/* HeliumMassFraction==1 */
   	Data[j]->n_He[i] = Data[j]->D[i] / ( 4.0 * (mass_p + mass_e) );
       }
      } else {
       Data[j]->n_He[i]=0.;
      }
     }
    }
   #endif
   #ifndef CONSTANT_DENSITY /* Not constant density */
    /* Set the densities at the start and end of the iteration. */
    /* First, calculate the offset to get us to the slice we want */
   #ifdef LOS_SLICE
    Offset = LOS_SLICE*NGy;
   #else
    Offset = 0;
   #endif
    for (j = 0; j < Nlos; j++) {
     memcpy(Data[j]->Dold, Data[j]->D, sizeof(double)*Data[j]->NumCells);
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->D[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
                       density+Offset+NGx*j, NGx, r0, length );
     #ifdef ENFORCE_MINIMUM_DENSITY
      /*Enforce a minimum density */
      if (Data[j]->D[i] <= 0.) {
       Data[j]->D[i] = ENFORCE_MINIMUM_DENSITY;
      }
     #endif
     #ifdef DEBUG
      if(Data[j]->D[i] <= 0) {
       printf("ERROR: PMRT: %i: RT_InterpolateFromGrid returned Data[%i].D[%i]=%5.3e\n",
              __LINE__,j,i,Data[j]->D[i]);
       printf("       Data.R+dR/2=%5.3e; NGx=%i; r0=%5.3e; length=%5.3e\n",
              Data[j]->R[i]+dR/2.0, NGx, r0, length);
       PMRT_ErrorCheck(EXIT_FAILURE,"Exiting...",me,__LINE__);
      }
     #endif
     }
    }
   #endif /* If CONSTANT_DENSITY then ... else ... endif */
   }

#if 0
/* FORCE CONSTANT DENSITY. TESTING ONLY!!! */
   if( (me==MASTER) ) {
    double ConstantDensity=4e-25;
    for (j = 0; j < Nlos; j++) {
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->D[i]    = ConstantDensity;
      Data[j]->Dold[i] = ConstantDensity;
     }
    }
   }
#endif

   /* On the first pass, set Data.D to density at the *start*, and the
    * velocities to 0. */
   if( (FirstRTPassFlag==TRUE) && (me==MASTER) ) {
    FirstRTPassFlag=FALSE;
    for (j = 0; j < Nlos; j++) {
     memcpy(Data[j]->Dold, Data[j]->D, sizeof(double)*Data[j]->NumCells);
     memset(Data[j]->v_z,           0, sizeof(double)*Data[j]->NumCells);
     memset(Data[j]->v_x,           0, sizeof(double)*Data[j]->NumCells);
    } 
   }

  #ifdef CALC_VELOCITIES
   /* If me==Master, then velocity_* hold the velocity grids.
    * Copy the LOS velocities to the Data structure.
    * Not needed for the code to run, but useful for post-processing.
    * velocity_* are [NG x NG x NFLUX].  We only want velocity_*[0,*,*] 
    * or velocity_*[0,0,*] */
   if( me == MASTER ) {
    for (j = 0; j < Nlos; j++) {
     for (i = 0; i < Data[j]->NumCells; i++) {
      Data[j]->v_z[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
                                        velocity_z+NGx*j, NGx, r0, length );
      Data[j]->v_x[i] = RT_InterpolateFromGrid(Data[j]->R[i]+dR/2.0,
                                        velocity_x+NGx*j, NGx, r0, length );
     }
    }
   }
  #endif

   /* Do the radiative transfer.
    * RT expects the expansion factor at the *end* of the iteration, since it
    * uses it for calculations at the end of the dt increment. */
  #ifdef TIME_CODE_SEGMENTS
   t_Elapsed = Wtime();
  #endif
  #if VERBOSE >= 1
   printf("(%2i) Calling RT...\n",me); ierr=fflush(stdout);
  #endif
  #ifdef RT_MPI
   ierr = RT_DistributeLOSs(me, MASTER, Nlos, nproc, t, dt, a_f, Data,
                            &Source, FID_timescales, t_on);
  #else
   if(me==MASTER) {
  #ifdef CUDA
    ierr = RT_CUDA_DistributeLOSs(Nlos, t, dt, a_f, Data,
                                  &Source, FID_timescales, t_on);
  #else
    ierr = RT_DistributeLOSs(Nlos, t, dt, a_f, Data,
                             &Source, FID_timescales, t_on);
  #endif
   } else {
    ierr = EXIT_SUCCESS;
   }
  #endif 
   PMRT_ErrorCheck(ierr,"RT failed",me,__LINE__);

   if (me == MASTER) {
    z_f = 1.0 / a_f - 1.0;
    printf("PMRT: z=%8.6f a=%8.6f next dump @ z_out=%8.6f\n", z_f, a_f,
           OutputRedshift[iout]);
    ierr=fflush(stdout);

    /* If we are at a redshift for which a dump was requested, then dump */
    if (z_f <= OutputRedshift[iout]) {
     /* Dump all the data */
    #ifdef LONG_FILE_LABEL
     ierr=snprintf(label_string, LABEL_STR_SIZE-1, "%s/Data_z=%8.6f", Dir,
              OutputRedshift[iout]);
    #else
     ierr=snprintf(label_string, LABEL_STR_SIZE-1, "%s/Data_z=%4.2f.%03i", Dir,
              OutputRedshift[iout], me);
    #endif
     if(ierr>(int)LABEL_STR_SIZE) {
      PMRT_ErrorCheck_File(EXIT_FAILURE,"Path too long",Dir,me,__LINE__);
     }
     printf("PMRT: dumping RT to file %s\n",label_string);
     ierr = RT_SaveData(label_string, Data, Nlos);
     PMRT_ErrorCheck_File(ierr,"Unable to write dump file",
                          label_string,me,__LINE__);
    } /* End of if time to dump */
   } /* If me==MASTER */

  #ifdef TIME_CODE_SEGMENTS
   t_RT += Wtime() - t_Elapsed;
   t_Elapsed = Wtime();
  #endif

  } /* end if z <= RED_0 */
  /* *************************************************************************
   * *** End of Radiative Transfer section ***********************************
   * *************************************************************************/
#endif /* ifndef NO_RT */

  /* Update the expansion factor. */
  aa *= exp(dloga);

 #ifndef CONSTANT_DENSITY
  if(me < num_PM_procs) {
   ierr=MPI_Gather(&np, 1, MPI_INT, np_list, 1, MPI_INT, 0, PM_Comm);
   PMRT_ErrorCheck_MPI(ierr, "MPI_Gather failed",me,__LINE__);
  }
 #endif

  /* Output diagnostics */
  wtime[1] = Wtime(); /* Needed for master & slaves */
  if (me == MASTER) {
  #if VERBOSE >= 1 && !defined(CONSTANT_DENSITY)
   for (mean_np = 0, max_np = -1, min_np = Np + 1, nn = 0; nn < nproc; nn++) {
    if (min_np > np_list[nn])
     min_np = np_list[nn];
    if (max_np < np_list[nn])
     max_np = np_list[nn];
    mean_np += np_list[nn];
   }
   mean_np /= nproc;
   printf("PMRT: End of iter: a=%8.6f dloga=%6.4e delta_min=%4.2f delta_max=%4.2f elapsed=%8.1f\n",
          aa, dloga, min_np / mean_np, max_np / mean_np, wtime[1] - wtime[0]);
  #else
   printf("PMRT: End of iter: a=%8.6f dloga=%6.4e elapsed=%8.1f\n",
          aa, dloga, wtime[1]-wtime[0]);
  #endif
   ierr=fflush(stdout);
  } /* end if MASTER */

  /* Increment the output counter if we have already reached it */
  if ( aa >= aout[iout] ) {
   iout++;
   /* If we have already reached the last output, then flag as done */
   if(iout == N_OUTPUT_REDSHIFTS) {
    if(me == MASTER) {
     printf("PMRT: No more output redshifts.\n"); ierr=fflush(stdout);
    }
    DoneFlag = TRUE; /* All nodes should come to the same conclusion */
   } else {
    if(me == MASTER) {
     printf("PMRT: Next dump @ a[%2i]=%8.6f\n",iout,aout[iout]);
     ierr=fflush(stdout);
    }
   }
  }

  if( DoneFlag==FALSE ){
   /* ** Get a new time step. ** */
   dloga = PMRT_GetNewTimeStep(
                              #ifndef CONSTANT_DENSITY
                               PM_Comm, Ng,
			       (double)Fmax, feps, stepmax,
                              #endif
                               aa, DTol, af, iout, aout);
  }
  
 #ifdef BACKUPS
  if( DoneFlag==FALSE ){
  /* Back up the current state */
  #ifdef TIME_CODE_SEGMENTS
   t_Elapsed = Wtime();
  #endif
   NumPMFiles=atoi(argv[3]);
   ierr=PMRT_BackupState(num_PM_procs,np,NumPMFiles,FirstRTPassFlag,Nlos,
                         Dir,PM_BAK,r,v,Data,aa,dloga);
   PMRT_ErrorCheck(ierr,"ERROR: PMRT_BackupState failed",me,__LINE__);
  #ifdef TIME_CODE_SEGMENTS
   t_Backups += Wtime() - t_Elapsed;
   t_Elapsed = Wtime();
  #endif
  }
 #endif /* If BACKUPS */

  ierr=fflush(stdout);

 #if VERBOSE >= 1
  printf("(%2i) PMRT: End of while loop\n",me); ierr=fflush(stdout);
 #endif
 #ifndef NO_MPI
  ierr=MPI_Barrier(MPI_COMM_WORLD);
  PMRT_ErrorCheck_MPI(ierr,"MPI_Barrier failed",me,__LINE__);
 #endif

 } while ( (DoneFlag==FALSE)&& (aa < af) && (wtime[1] - wtime[0] < MaxTime) );
 /* (DoneFlag==FALSE) && (aa <= af) are repetative.  They should become false
  * at the same time. But I keep af around to hardwire a different exit time. */
 /*********************** END OF MAIN LOOP ************************/

 /* Report why we terminated since it wasn't an error. */
 if (me == MASTER) {
  if (aa >= af) {
   printf("Evolution done. a >= a_final\n");
  } else if (wtime[1] - wtime[0] >= MaxTime) {
   printf("Evolution stopped at wallclock limit.\n");
  } else {
   printf("Evolution done for other reasons.\n");
  }
  wtime[1] = Wtime();
  printf("Elapsed wall time: %.1f sec.\n\n", wtime[1] - wtime[0]);
  ierr=fflush(stdout);
 }

#ifdef TIME_CODE_SEGMENTS
 /* Report the time spent in each section */
#ifdef BACKUPS
 printf("(%2i) Time spent: PM %.1f; Density %.1f; RT %.1f; Backups: %.1f\n",
        me, t_PM, t_Density, t_RT, t_Backups);
#else
 printf("(%2i) Time spent: PM %.1f; Density %.1f; RT %.1f\n",
        me, t_PM, t_Density, t_RT);
#endif
 ierr=fflush(stdout);
#ifndef NO_MPI
 ierr=MPI_Barrier(MPI_COMM_WORLD);
 PMRT_ErrorCheck_MPI(ierr,"MPI_Barrier failed",me,__LINE__);
#endif
#endif

 /* Close streams */
 if(FID_timescales!=NULL) {
  ierr=fclose(FID_timescales);
  PMRT_ErrorCheck(ierr,"fclose failed",me,__LINE__);
 }

 /* Free allocated memory */
 FREE_MEMORY;

#ifndef NO_MPI
 ierr=MPI_Finalize();
 PMRT_ErrorCheck_MPI(ierr,"MPI_Finalize",me,__LINE__);
#endif

 printf("(%2i) Done!\n",me);
 ierr=fflush(stdout);

 return EXIT_SUCCESS; /* YAY! */
}
