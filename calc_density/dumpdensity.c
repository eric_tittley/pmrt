#include <stdio.h>

#include "calc_density.h"
#include "RT.h"

int dumpdensity(const char *filename, const float *density, const size_t N)
{
 FILE *fid;
 int ierr;
 /* size_t Nwritten; */

 /* Open the file */
 fid = fopen(filename, "w");
 if (fid == NULL) {
  printf("ERROR: dumpdensity: Unable to open file %s\n", filename);
  return (-1);
 }

 /* Write the data */
 (void)fwrite(density, N, sizeof(float), fid);
 /* fwrite occasionally returns a short count even if all the elements
  * get written to the file.  I don't know why this is.
  * Nwritten = fwrite(density, N, sizeof(float), fid);
  * if( Nwritten != N ) {
  * printf("ERROR: dumpdensity: Only wrote %d of %d elements to %s\n",
  * (int)Nwritten, (int)N, filename);
  * return(-1);
  * }
  */

 /* Close the file */
 ierr = fclose(fid);
 if (ierr != 0) {
  printf("ERROR: dumpdensity: Unable to close file %s\n", filename);
  return (-1);
 }

 return (0);
}
