/* CalcDensity_ConvolveMomentumGrid : Convolve a Momentum Grid
 *
 * Smooth the Momentum Grid derived from N-body data, using the same
 * density rules as used for smoothing the density data:
 *  If there are few particles, smooth
 *  If there are enough particles, smooth only the component
 *  below the threshold.
 *
 * ARGUMENTS
 *  Input not Modified
 *   me                 MPI process number
 *   TAG                MPI tag.
 *   nproc              Number of MPI processors
 *   Ncells             Number of Cells to be processed
 *   Ngrid              The number of cells per grid edge
 *   local_nx
 *   local_size
 *   local_ny_after_transpose
 *   density            Density grid
 *   density_lo         Density grid containing the low-density component
 *   density_hi         Density grid containing the high-density component
 *   kernel             Kernel with which to convolve with
 *   fftiplan           FFTW plan for the FFT
 *  Input then Modified
 *   momentum [MASTER]  Input: Momentum grid to be convolved.
 *                      Output: Smoothed momentum field.
 *                              Still need to divide by the smoothed density.
 *               NOTES: - "Momentum" can be number*velocity instead of mass.
 *                        It's up to the user to normalise appropriately.
 *                      - On input and output, momentum is defined for only
 *                        the MASTER.
 *  AUTHOR: Eric Tittley
 *
 *  HISTORY
 *   110202 First version, extracted from calc_density.c
 *   110203 The momentum field had never been forward FFTed.  How did this
 *          ever work before!?!  Now I get velocities with a similar
 *          distribution to that produced by Enzo.
 *   110224 BUG FIX: The routine was being given momentum and was returning
 *          momentum, but I was thinking it was velocity coming in and
 *          momentum going out.  No need to scale the input but density.
 *          Renamed routine and variables to be explicit.
 */

#include "calc_density.h"

#include <stdlib.h>
#include <assert.h>

#include <mpi.h>
#define MASTER 0
#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

int CalcDensity_ConvolveMomentumGrid(const MPI_Comm Communicator,
                                     const int Ngrid,
                                     const float NumberCutForConvolution,
                                     const float *density,
                                     const fftw_real *density_lo,
                                     const float *density_hi,
		                     const fftw_complex *kernel,
                                     const rfftwnd_mpi_plan fftfplan,
                                     const rfftwnd_mpi_plan fftiplan,
                                     float *momentum) {

 const int Ncells = Ngrid * Ngrid * Ngrid;
 const int NZ = Ngrid + 2; /* FFTW needs padding. */
 const int TAG=10;
 int Ncells_IN_SLAB;
 int proc;
 int nproc=-1;
 int me=-1;
 int i;
 int ix,iy,iz;
 int pos;
 int local_nx=0;
 float FractionInLowDensity;
 float *momentum_local;
 float *momentum_hi=NULL;
 MPI_Status status;

 /* FFTW variables */
 int local_x_start=0;
 int local_ny_after_transpose=0;
 int local_y_start_after_transpose=0;
 int local_size=0;
 fftw_complex rho;
 fftw_complex *momentum_lo_c=NULL;
 fftw_real *momentum_lo=NULL;

 /* Get number of nodes and my node number. */
 (void)MPI_Comm_size(Communicator, &nproc);
 (void)MPI_Comm_rank(Communicator, &me);

 Ncells_IN_SLAB = Ncells/nproc;

 /* Work out the ranges on each processor. */
 rfftwnd_mpi_local_sizes(fftfplan,
                         &local_nx,
                         &local_x_start,
                         &local_ny_after_transpose,
                         &local_y_start_after_transpose,
                         &local_size);

 /* Allocate memory for momenta */
 momentum_lo    = (fftw_real*)malloc(sizeof(fftw_real) * local_size);
 if (momentum_lo == NULL) {
  printf("(%2i) ERROR: %s: %i: Allocation of memory failed: momentum_lo\n",me,__FILE__,__LINE__);
  return EXIT_FAILURE;
 }
 momentum_hi    = (float *)malloc(sizeof(float) * Ncells_IN_SLAB);
 if (momentum_hi == NULL) {
  printf("(%2i) ERROR: %s: %i: Allocation of memory failed: momentum_hi\n",me,__FILE__,__LINE__);
  free(momentum_lo);
  return EXIT_FAILURE;
 }

 /* Pass the momentum field slabs to the MPI processes */
 if( me == MASTER ) {
  for(proc=1; proc<nproc; proc++) {
   (void)MPI_Ssend((float *)(momentum+proc*Ncells_IN_SLAB), Ncells_IN_SLAB,
                             MPI_FLOAT, proc, TAG, Communicator);
  }
  momentum_local = momentum;
 } else { /* SLAVE */
  /* Allocate memory to receive the momentum field */
  momentum_local = (float *)malloc(sizeof(float) * Ncells_IN_SLAB);
  if (momentum_local == NULL) {
   printf("(%2i) ERROR: %s: %i: Allocation of memory failed: momentum_local\n",me,__FILE__,__LINE__);
   free(momentum_lo);
   free(momentum_hi);
   return EXIT_FAILURE;
  }
  (void)MPI_Recv(momentum_local, Ncells_IN_SLAB, MPI_FLOAT, MASTER,
                 TAG, Communicator, &status);
 }

 /* Assign momentum for each cell to the momentum_lo and momentum_hi fields */
 i = 0;
 for(ix=0; ix<local_nx; ix++) {
  for(iy=0; iy<Ngrid; iy++) {
   for(iz=0; iz<Ngrid; iz++) {
    pos = (ix*Ngrid + iy)*NZ + iz;
   #ifdef DEBUG
    assert( i < Ncells_IN_SLAB );
    assert( pos < local_size );
   #endif
    if( density[i] < NumberCutForConvolution ) {
     momentum_lo[pos] = momentum_local[i];
     momentum_hi[i] = 0.0;
    } else { /* cell has more than NumberCutForConvolution particles */
     FractionInLowDensity = NumberCutForConvolution/density[i];
     momentum_lo[pos] = momentum_local[i]*FractionInLowDensity;
     momentum_hi[i]   = momentum_local[i]*(1.0-FractionInLowDensity);
    }
    i++;
   }
  }
 }

 /**********************************/
 /* Convolve the momentum_lo field */
 /**********************************/

 /* Take the forward FFT of the low-density field */
 rfftwnd_mpi(fftfplan, 1, momentum_lo,  NULL, FFTW_TRANSPOSED_ORDER);

 /* Multiply by the FFT of the kernel function */
 momentum_lo_c = (fftw_complex*)momentum_lo;
 for (i = 0; i < Ngrid*local_ny_after_transpose*NZ/2; i++) {
  rho.re = momentum_lo_c[i].re;
  rho.im = momentum_lo_c[i].im;
  momentum_lo_c[i].re = rho.re * kernel[i].re - rho.im * kernel[i].im;
  momentum_lo_c[i].im = rho.im * kernel[i].re + rho.re * kernel[i].im;
 }

 /* Inverse transform */
 momentum_lo = (fftw_real*)momentum_lo_c;
 rfftwnd_mpi(fftiplan, 1, momentum_lo, NULL, FFTW_TRANSPOSED_ORDER);

 /* Combine momentum fields */
 i = 0;
 for(ix=0; ix<local_nx; ix++) {
  for(iy=0; iy<Ngrid; iy++) {
   for(iz=0; iz<Ngrid; iz++) {
    pos = (ix*Ngrid + iy)*NZ + iz;
   #ifdef DEBUG
    assert( i < Ncells_IN_SLAB );
    assert( pos < local_size );
   #endif
    momentum_local[i] = (float)(momentum_lo[pos] + momentum_hi[i]);
    i++;
   }
  }
 }

 /* Bring the momentum slabs back together */
 if( me == MASTER ) {
  for(proc=1; proc<nproc; proc++) {
   (void)MPI_Recv((float *)(momentum+proc*Ncells_IN_SLAB), Ncells_IN_SLAB, MPI_FLOAT,
                  proc, TAG, Communicator, &status);
  }
 } else { /* SLAVE */
  (void)MPI_Ssend(momentum_local, Ncells_IN_SLAB, MPI_FLOAT, MASTER, TAG, Communicator);
  free(momentum_local);
 }

 free(momentum_lo);
 free(momentum_hi);
 return EXIT_SUCCESS;
}
