/* CalcDensity_ConvolveGrids: Convolve the density and velocity fields with a
*                             Gaussian
 *
 * ARGUMENTS
 *  Input, not modified
 *   Communicator               The MPI Communicator to use
 *   Ngrid                      The number of cells per grid side
 *   NumberCutForConvolution    The threshold number density below which to
 *                              convolve
 *   kernel                     Pointer to the kernel to convolve with
 *  Input and modified, MASTER only.  Meaningless for SLAVES
 *   density     Density grid
 *   velocity_x  Momentum grid in the X-direction (along the LOS)
 *   velocity_z  Momentum grid in the Z-direction (perpendicular to the LOS, but
 *               in the plane)
 *   The MASTER process has the full density and velocity grids
 *   For the slaves, only part contributions
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  110221 Split from calc_density.c
 *  110224 Make it much clearer that the velocity field is actually a momentum field
 *      until scaled before returning.
 */


#ifdef HAVE_CONFIG
/* For CALC_VELOCITIES, DEBUG, VERBOSE */
#include "config.h"
#endif

#include "calc_density.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef DEBUG
#include <assert.h>
#endif

#include <mpi.h>
#define MASTER 0

#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

#include "Logic.h"

#define FREE_MEMORY \
rfftwnd_mpi_destroy_plan(fftfplan); \
rfftwnd_mpi_destroy_plan(fftiplan); \
free(density_lo); \
free(density_hi); \
if(me != MASTER ) { free(density_local); }

int CalcDensity_ConvolveGrids(const MPI_Comm Communicator,
                              const int Ngrid,
                              const float NumberCutForConvolution,
 		              const fftw_complex *kernel,
                              float *density,
                              float *velocity_x,
		              float *velocity_z
                             ) {

 const int Ncells = Ngrid * Ngrid * Ngrid;
 const int NZ = Ngrid + 2; /* FFTW needs padding. */
 const int TAG = 10;            /* This can be any integer? */

 unsigned int AllocatedMemory=0;
 int me=0;
 int nproc=0;
 int i;
 int ix, iy, iz;
 int ierr;
 int local_nx=0,local_x_start=0,local_ny_after_transpose=0;
 int local_y_start_after_transpose=0,local_size=0;
 int pos;
 int proc;
 int Ncells_IN_SLAB;

 /* Pointers to allocated memory */
 fftw_real *density_lo=NULL;
 float *density_hi=NULL;
 float *density_local=NULL;
 rfftwnd_mpi_plan fftfplan=NULL;
 rfftwnd_mpi_plan fftiplan=NULL;

 MPI_Status status;
 fftw_complex *density_lo_c;
 fftw_complex rho;

 /* Get my node number. */
 (void)MPI_Comm_rank(Communicator, &me);
 
 /* Get the number of processors in the communicator */
 ierr=MPI_Comm_size(Communicator, &nproc);
 if(ierr != MPI_SUCCESS) {
  printf("ERROR: PMRT: %i: MPI_Comm_size failed\n",__LINE__);
  exit(EXIT_FAILURE);
 }

 /* Allocate Memory */
 /* Set up the FFTW plan files.  Both forward and inverse expect the
    dimensions to be Nx, Ny, Nz. */

 fftfplan =
  rfftw3d_mpi_create_plan(Communicator, Ngrid, Ngrid, Ngrid,
                          FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);

 fftiplan =
  rfftw3d_mpi_create_plan(Communicator, Ngrid, Ngrid, Ngrid,
                          FFTW_COMPLEX_TO_REAL, FFTW_ESTIMATE);

 /* Work out the ranges on each processor. */
 rfftwnd_mpi_local_sizes(fftfplan,
                         &local_nx,
                         &local_x_start,
                         &local_ny_after_transpose,
                         &local_y_start_after_transpose,
                         &local_size);

#if VERBOSE >= 1
 printf("(%2i) %s: local_nx=%i; local_x_start=%i\n",
        me,__FILE__,local_nx,local_x_start);
 printf("(%2i) %s: local_ny_after_transpose=%i\n",
        me,__FILE__,local_ny_after_transpose);
 (void)fflush(stdout);
#endif

 AllocatedMemory += sizeof(fftw_real) * local_size;
 density_lo    = (fftw_real*)malloc(sizeof(fftw_real) * local_size);
 if (density_lo == NULL) {
  printf("ERROR: %s: Allocation of memory failed: density_lo\n",__FILE__);
  FREE_MEMORY;
  return EXIT_FAILURE;
 }

 Ncells_IN_SLAB = Ncells/nproc;
 AllocatedMemory += sizeof(float) * Ncells_IN_SLAB;
 density_hi    = (float *)malloc(sizeof(float) * Ncells_IN_SLAB);
 if (density_hi == NULL) {
  printf("ERROR: %s: Allocation of memory failed: density_hi\n",__FILE__);
  FREE_MEMORY;
  return EXIT_FAILURE;
 }

 if(me!=MASTER) {
  AllocatedMemory += sizeof(float) * Ncells_IN_SLAB;
  density_local = (float *)malloc(sizeof(float) * Ncells_IN_SLAB);
  if (density_local == NULL) {
   printf("ERROR: %s: Allocation of memory failed: density_local\n",__FILE__);
   FREE_MEMORY;
   return EXIT_FAILURE;
  }
 } else {
  density_local = density;
 }

#if VERBOSE >= 1
 if(me<=1) { \
  printf("(%2i) %s: Allocated %i bytes = %9.2f kb = %6.2f Mb\n", \
         me, __FILE__, (int)AllocatedMemory, (float)AllocatedMemory/1024.0, \
 	(float)AllocatedMemory/1048576.0); \
  (void)fflush(stdout); \
 }
#endif

#if VERBOSE >= 1
 if(me == MASTER) {
  printf("%s: Convolving fields.\n",__FILE__);
  (void)fflush(stdout);
 }
#endif

/* NOTE: At the moment, the unit of density is numbers of particles.
 *       It is known only to the MASTER.  Each will get a slab of
 *       length Ng/nproc.  */

 if( me == MASTER ) {

#ifdef DEBUG
  /* Sanity Check */
  if(local_size != Ngrid*Ngrid*NZ/nproc) {
   printf("(%2i) ERROR: %s: local_size != Ngrid*Ngrid*NZ/nproc : %i != %i\n",
          me,__FILE__,local_size,Ngrid*Ngrid*NZ/nproc);
   (void)fflush(stdout);
   FREE_MEMORY ;
   return EXIT_FAILURE;
  }
#endif

  /* Distribute the slabs to the nodes */
  for(proc=1; proc<nproc; proc++) {
   (void)MPI_Ssend((float *)(density+proc*Ncells_IN_SLAB), Ncells_IN_SLAB,
                   MPI_FLOAT, proc, TAG, Communicator);
  }
  /* Don't forget that the master does work, too! on the first slab */
  /* density_local already points to density for the MASTER.
   * (see: calc_density) */
  /* memcpy(density_local,density,(size_t)Ncells_IN_SLAB); */
 } else { /* SLAVE */
  (void)MPI_Recv(density_local, Ncells_IN_SLAB, MPI_FLOAT, MASTER, TAG,
                 Communicator, &status);
 }

 /* Split the densities in the slab into low and high number-count components.
  * Remember that density_lo has been allocated with padding. So while 
  * density_local and density_hi are contiguous in memory, density_lo needs
  * to skip over the padding (NZ = Ng+2) */
 /* Density */
 i = 0;
 for(ix=0; ix<local_nx; ix++) {
  for(iy=0; iy<Ngrid; iy++) {
   for(iz=0; iz<Ngrid; iz++) {
    pos = (ix*Ngrid + iy)*NZ + iz;
    if( density_local[i] < NumberCutForConvolution ) {
     density_lo[pos] = density_local[i];
     density_hi[i] = 0.0;
    } else { /* cell has more that NumberCutForConvolution particles */
     density_lo[pos] = NumberCutForConvolution;
     density_hi[i] = density_local[i] - NumberCutForConvolution;
    }
    i++;
   }
  }
 }

#ifdef CALC_VELOCITIES
 /* To save memory, do the momentum convolutions separately up so they are
  * completed before the density convolution.   */

 /***************************************************************
  ****** Start with velocity_x (perpendicular to the LOS) *******
  ***************************************************************/
 ierr = CalcDensity_ConvolveMomentumGrid(Communicator, Ngrid, NumberCutForConvolution,
                                         density_local, density_lo, density_hi,
		                         kernel, fftfplan, fftiplan, velocity_x);
 if( ierr != EXIT_SUCCESS ) {
  printf("(%2i) ERROR: %s: %i: CalcDensity_ConvolveMomentumGrid failed for velocity_x.\n",
 	 me,__FILE__,__LINE__);
  
 }
 /*********************************************************
  ********* Repeat for velocity_z (along the LOS) *********
  *********************************************************/
 ierr = CalcDensity_ConvolveMomentumGrid(Communicator, Ngrid, NumberCutForConvolution,
                                         density_local, density_lo, density_hi,
		                         kernel, fftfplan, fftiplan, velocity_z);
 if( ierr != EXIT_SUCCESS ) {
  printf("(%2i) ERROR: %s: %i: CalcDensity_ConvolveMomentumGrid failed for velocity_z.\n",
 	 me,__FILE__,__LINE__);
  
 }
 /* NOTE: velocity_x and velocity_z are *momentum fields*.  Need to divide by
  *       the smoothed density field, which we don't have, yet. */
#endif /* CALC_VELOCITIES */

 /****************************************************
  ********* Finish the density ***********************
  ****************************************************/

 /* Take the forward DFT of the low density field (LDF)
  * Note, I only give it my slab.  FFTW knows it is part of a larger
  * array and will combine it with every other nodes slab. */
 rfftwnd_mpi(fftfplan, 1, density_lo,  NULL, FFTW_TRANSPOSED_ORDER);

 /* density_lo now holds fftw_complex data, so recast */
 density_lo_c =  (fftw_complex*)density_lo;

 /* Multiply the DFT of LDF by the DFT of the kernel (previously calculated).
  * kernel should already include the normalisation factor (1/Ncells).
  * Both the kernel and the density are slabs, hopefully oriented correctly! */
 for (i = 0; i < Ngrid*local_ny_after_transpose*NZ/2; i++) {
  rho.re = density_lo_c[i].re;
  rho.im = density_lo_c[i].im;
  density_lo_c[i].re = rho.re * kernel[i].re - rho.im * kernel[i].im;
 #ifdef DEBUG
  if(!(bool)isfinite(density_lo_c[i].re)) {
   printf("(%2i) ERROR: %s: %i: density_lo_c[%i].re=nan; rho.re=%5.3e;\n",
 	  me,__FILE__,__LINE__,i,rho.re);
   printf("      kernel[%i].re=%5.3e; rho.im=%5.3e; kernel[%i].im=%5.3e;\n",
          i,kernel[i].re,rho.im,i,kernel[i].im);
   (void)fflush(stdout);
   FREE_MEMORY;
   return EXIT_FAILURE;
  }
 #endif
  density_lo_c[i].im = rho.im * kernel[i].re + rho.re * kernel[i].im;
 }

 /* density_lo_c contains fftw_real data which density_lo already points to.
  * But let me just re-iterate it for stylistic purposes. */
 density_lo  = (fftw_real*)density_lo_c;

 /* Take the inverse DFT of the multiplied transform */
 rfftwnd_mpi(fftiplan, 1, density_lo,  NULL, FFTW_TRANSPOSED_ORDER);

 /* Re-combine the low and high density fields. */
 i = 0;
 for(ix=0; ix<local_nx; ix++) {
  for(iy=0; iy<Ngrid; iy++) {
   for(iz=0; iz<Ngrid; iz++) {
    pos = (ix*Ngrid + iy)*NZ + iz;
    density_local[i] = (float)density_lo[pos] + density_hi[i];
   #ifdef DEBUG
    if(!(bool)isfinite(density_local[i])) {
     printf("(%2i) ERROR: calc_density: %i: density_local[%i]=nan; density_lo[%i]=%5.3e;\n",
            me,__LINE__,i,pos,density_lo[pos]);
     printf("      density_hi[%i]=%5.3e\n",i,density_hi[i]);
     (void)fflush(stdout);
     FREE_MEMORY;
     return EXIT_FAILURE;
    }
   #endif
    /* Convolution can occasionally produce slightly negative results */
    if(density_local[i] < 0.) { density_local[i] = 0.; }
    i++;
   }
  }
 }
#ifdef DEBUG
 for (i = 0; i < Ncells_IN_SLAB; i++) {
  if(!(bool)isfinite(density_local[i])) {
   printf("(%2i) ERROR: calc_density: %i: density_local[%i]=nan\n",me,__LINE__,i);
   (void)fflush(stdout);
   FREE_MEMORY;
   return EXIT_FAILURE;
  }
 }
#endif

 /* Bring the slabs back together */
 if( me == MASTER ) {
  /* The MASTER's contribution
   * density_local already points to density for the MASTER.
   * memcpy(density,density_local,(size_t)Ncells_IN_SLAB);
   * The rest of the nodes' contributions */
  for(proc=1; proc<nproc; proc++) {
   (void)MPI_Recv((float *)(density+proc*Ncells_IN_SLAB), Ncells_IN_SLAB,
                  MPI_FLOAT, proc, TAG, Communicator, &status);
  }
 #ifdef DEBUG
  for (i = 0; i < Ncells; i++) {
   if(!(bool)isfinite(density[i])) {
    printf("(%2i) ERROR: calc_density: %i: density[%i]=nan\n",me,__LINE__,i);
    (void)fflush(stdout);
    FREE_MEMORY;
    return EXIT_FAILURE;
   }
  }
 #endif
 } else { /* SLAVE */
  (void)MPI_Ssend(density_local, Ncells_IN_SLAB, MPI_FLOAT, MASTER, TAG,
                  Communicator);
 }

 /*********************************************************
  ** DONE CONVOLVING density, velocity_x, and velocity_z **
  *********************************************************/

 FREE_MEMORY;
 return EXIT_SUCCESS;
}
