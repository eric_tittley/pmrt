/* Calculate the gas density, v_x, and v_z, given the dark matter positions in r.
 *
 * int calc_density(const int me, const int NumParticlesInThisProcess,
 *                  const int Ng, const double aa, const double vunit,
 *                  const float *r, const float *v, const fftw_complex *kernel,
 *                  float *density, float *velocity_x, float *velocity_z)
 * ARGUMENTS
 *  Input, not modified
 *   me		My node
 *   NumParticlesInThisProcess		Total number of particles in this node
 *   Ng		The mesh size. (Ng x Ng x Ng)
 *   aa		Expansion factor
 *   r		Positions of the particles (simulation units Lside)
 *   v		Velocities of the particles (simulation units Lside)
 *   kernel	The DFT of the convolution kernel (halfcomplex, which makes it
 *		very complex :p See InitKernel)
 *   vunit	Constant to convert simulation units into km/s
 *  Initialised (but must be pre-allocated)
 *   density	   Grid of density (kg/m^3)
 *   velocity_x    Grid of tangential velocities (km/s, if vunit is correct)
 *   velocity_z    Grid of LOS velocities (km/s, if vunit is correct)
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if:	a particle's cell is out of bounds
 *               EXIT_FAILURE requires DEBUG set in RT.h
 *
 * GLOBAL VARIABLES and MACROS
 *  number_cut_for_convolution	constants.c (double)
 *  h				constants.c
 *  omega_b			constants.c
 *  lside[3]			globals.c (double)
 *  Np                          The total number of particles
 *
 * NOTES
 *  Compiler Options.  I prefer CONVOLVE_DENSITY only.  Some are mutally
 *  		       exclusive.
 *   DEBUG			Include code to check for errors.
 *   VERBOSE			The verbosity level (0, 1, 2, ...)
 *   CALC_VELOCITIES		Also calculate the radial and transverse
 *				velocities.
 *   CONVOLVE_DENSITY		Convolve the *low density* field.
 *   SPREAD_GAS_PARTICLE	Spread the particles amongst neighbouring cells
 *				using a Cloud-in-Cell method.
 *   TIME_CODE_SEGMENTS		Time the separate components of calc_density.
 *   ENFORCE_MINIMUM_DENSITY	Value to which to set as a minimum density
 *				(if set).
 *
 * TODO
 *  Can probably use density_local for both density_local and density_hi.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  03 12 10 First version
 *  04 01 28 Added constant density choice.
 *  04 02 06 In calculation of scale, added (float) in front of Np.
 *	Otherwise it was crashing at this point on occasion.
 *  04 02 06 Only dump density grid if DEBUG.
 *  04 02 10 Memory leak: ix % NFLUX will be negative if ix < 0.
 *  04 03 23 Allocation error: W[2*NSLICE], not W[NSLICE]
 *  04 04 28 dummyMin & dummyMax were initialised to 1e99 & -1e99 which
 *	are outside the range for floats.
 *  04 01 18 Add convolution of low-density component via CONVOLVE_DENSITY.
 *  05 02 14 Ripped out sections for reading the data from a file or setting
 *	the density to a constant value.
 *	Also calculate the velocity along the LOS.  Needs to be done, here,
 *	since it is weighted by the mass, which has methods sensitive to
 *	compiler flags.
 *  05 02 21 Added vunit.
 *  05 02 24 Calculate and save the tangential coplanar velocity.
 *  05 07 05 Grid onto an NG*NG*NG grid, only.
 *  05 08 06 Separate the velocity calculations out with CALC_VELOCITIES
 *  05 10 10 Allocate and free local memory here, not in RT_allocate_memory.
 *  05 10 29 Re-ordered the velocity (_x & _z) so they are always in that order.
 *	Bringing all the velocity_x slabs together was being done twice.
 *  06 02 21 Memory Leak: wasn't clearing the fftw plan properly.
 *  06 03 20 Add ENFORCE_MINIMUM_DENSITY.
 *  06 03 21 Removed the accumulation of grids to reduce().
 *  06 04 17 Added an MPI_Communicator, PM_Comm, for all processes that deal
 *	with the particle data (all the PM code, plus calc_density, etc.). This
 *	communicator is limited to 2^n processes, while the RT code can use all
 *	available processes.
 *  06 08 30 Don't report the time spent scaling anymore.
 *  06 09 21 reduce() has Ncells as an argument.
 *  06 11 01 Removed dependency on NLOS, which was only used in a DEBUG block.
 *  06 11 15 Major Bug!  I've been using | when I meant ||.
 *  06 11 21 Replaced Macros NG & NFLUX with argument Ng.
 *  07 02 01 Cosmetic changes to diagnostic messages.
 *  07 02 04 Tidied up ARGUMENT comments.
 *  07 02 07 Made everything except r, v, and density arrays doubles.
 *  08 08 04 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 *  11 02 02 How did this ever work? I replaced Macro Ng with argument Ng, but Ng
 *      is a global variable.  Why don't compilers check this stuff!!!!
 *      Changed Ng to Ngrid.
 *      Split out CalcDensity_convolveVelocityGrid()
 *  11 02 08 Split out CalcDensity_GridParticles()
 *  11 02 21 Split out CalcDensity_ConvolveGrids()
 *      Note that until this time, the velocities have all been in error.
 *  11 02 24 Eliminated nproc, which isn't needed anymore because the
 *      subtroutines can get that info for themselves.
 * TODO
 */

#include "config.h"

#include "calc_density.h"

#include <stdio.h>
#include <string.h>

#include <math.h>

#include <mpi.h>

#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

#include "mpipm.h"
#include "RT.h"
#include "Support.h"
#include "RT_constants_configuration.h"
#include "RT_constants_cosmology.h"

int calc_density(const MPI_Comm Communicator,
                 const int NumParticlesInThisProcess,
                 const int Ngrid,
                 const double aa,
                 const double vunit,
                 const float *r,
                 const float *v,
		 const fftw_complex *kernel,
                 float *density,
                 float *velocity_x,
		 float *velocity_z)
{
 const int Ncells = Ngrid * Ngrid * Ngrid;
 int i;
 int me=0;
 double rho_c, scale, sum;

 int ierr;

#if VERBOSE >= 1
 char filename[1024];
 float dummyMin, dummyMax;
#endif

#ifdef TIME_CODE_SEGMENTS
 double t_Elapsed, t_Dist, t_Conv;
#endif

 /* Get my node number. */
 (void)MPI_Comm_rank(Communicator, &me);

#ifdef TIME_CODE_SEGMENTS
 t_Elapsed = MPI_Wtime();
#endif

#if VERBOSE >= 1
 printf("(%2i) calc_density: NumParticlesInThisProcess=%d\n",
        me, NumParticlesInThisProcess);
 (void)fflush(stdout);
#endif

 /* Clear density array */
 for (i = 0; i < Ncells; i++) {
  density[i] = 0.0;
 #ifdef CALC_VELOCITIES
  velocity_x[i] = 0.0;
  velocity_z[i] = 0.0;
 #endif
 }

 /* This block should be deleted */
 sum=0;
 for(i=0;i<NumParticlesInThisProcess;i++) {
  sum+=sqrt((double)v[i*3]*(double)v[i*3]+(double)v[i*3+2]*(double)v[i*3+2]);
 }
 sum/=(double)NumParticlesInThisProcess;
 printf("(%2i) Before gridding: mean(|vx+vz|)=%5.3e\n",me,sum);

 /* Calculate the density and velocity (momentum for now) grids from
  * the particle positions */
#ifdef TIME_CODE_SEGMENTS
 t_Dist = MPI_Wtime();
#endif
 ierr = CalcDensity_GridParticles(PM_Comm, NumParticlesInThisProcess, Np, Ndim, Ngrid, lside,
                                  r, v, density, velocity_x, velocity_z);
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) %s: %i: ERROR: CalcDensity_GridParticles failed.\n",
         me,__FILE__,__LINE__);
 }
#ifdef TIME_CODE_SEGMENTS
 t_Dist = MPI_Wtime() - t_Dist;
#endif

#if 0
 /* This block should be deleted */
 if(me==MASTER) {
  sum=0;
  for(i=0;i<Ncells;i++) {
   sum+=sqrt( (double)velocity_x[i]*(double)velocity_x[i]
             +(double)velocity_z[i]*(double)velocity_z[i]);
  }
  sum/=(double)Np;
  printf("(%2i) After gridding: mean(|velocity_x+velocity_z|)=%5.3e\n",me,sum);
 }
#endif

#if 0
 /* This block should be deleted */
 if(me==MASTER) {
  sum=0;
  for(i=0;i<Ncells;i++) {
   sum+=sqrt( (double)velocity_x[i]*(double)velocity_x[i]
             +(double)velocity_z[i]*(double)velocity_z[i]);
  }
  printf("(%2i) Before convolving: mean(|p_x+p_z|)=%5.3e\n",me,sum);
 }
#endif

#ifdef CONVOLVE_DENSITY
/* Convolve the density field and, if necessary, the velocity field */
#ifdef TIME_CODE_SEGMENTS
 t_Conv = MPI_Wtime();
#endif
 ierr = CalcDensity_ConvolveGrids(PM_Comm, Ngrid, number_cut_for_convolution,
                                  kernel, density, velocity_x, velocity_z);
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) %s: %i: ERROR: CalcDensity_ConvolveGrids failed.\n",
         me,__FILE__,__LINE__);
 }
#ifdef TIME_CODE_SEGMENTS
 t_Conv = MPI_Wtime() - t_Conv;
#endif
#else
 t_Conv = 0.0;
#endif /* CONVOLVE_DENSITY */

#ifdef CALC_VELOCITIES
 if(me==MASTER) {
  /* Convert the momentum fields into velocity fields */
  for(i=0; i<Ncells; i++) {
   if(density[i] > 0.0) {
    velocity_x[i] = velocity_x[i]/density[i];
    velocity_z[i] = velocity_z[i]/density[i];
   } else {
    velocity_x[i] = 0.;
    velocity_z[i] = 0.;
   }
  }

  /* SCALE THE VELOCITY TO km/s */
  for(i=0; i<Ncells; i++) {
   velocity_x[i] *= (float)vunit;
   velocity_z[i] *= (float)vunit;
  }
 }
#endif

/* ********** SCALE THE DENSITY FROM NUMBER PER CELL TO kg/m^3 ********** */
 if( me==MASTER ) {
#if VERBOSE >= 1
  sum = 0;
  for (i = 0; i < Ncells; i++) {
   sum += density[i];
  }
  printf("calc_density: Np should equal sum(density)\n");
  printf("              Np=%i; sum(density)=%e;\n", Np, sum);
  (void)fflush(stdout);
#endif
  /* Scale the density to the gas density */
  rho_c = 1.879e-26 * h * h * pow(aa, -3);      /* critical density kg/m^3 */
  /*
   * V_box = pow(Length*Mpc,3.0);
   * V_cell= V_box/Ncells;
   * scale = ( rho_c * omega_b * V_box ) / ( Np * V_cell );
   * * Following is  the same as the previous line
   * * (gas density for overdensity of 1) / ( average # of particles per cell )
   */
  scale = (rho_c * omega_b) * ((double)Ncells / (double)Np); /* kg/m^3 / particle */
  for (i = 0; i < Ncells; i++) {
   density[i] *= (float)scale;
  }

 #ifdef DEBUG
  for (i = 0; i < Ncells; i++) {
   if(!(bool)isfinite(density[i])) {
    printf("(%2i) ERROR: calc_density: %i: density[%i]=nan\n",me,__LINE__,i);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
  }
 #endif

 #ifdef ENFORCE_MINIMUM_DENSITY
  /* Enforce a minimum density.  NOTE: this does not conserve mass. */
  for (i = 0; i < Ncells; i++) {
   if(density[i] <= 0.) density[i] = ENFORCE_MINIMUM_DENSITY;
  }
 #endif

#if VERBOSE >= 1
  printf("calc_density: rho_c=%5.3e; omega_b=%5.3e; scale=%5.3e;\n", rho_c, omega_b,
         scale);
  printf("calc_density: h=%5.3e; aa=%5.3e;\n", h, aa);
  (void)fflush(stdout);
#endif

#if VERBOSE >= 1
  /* Dump the density grid to file */
  strncpy(filename, "density.dat", 1023);
  ierr = dumpdensity(filename, density, (size_t) Ncells);
#ifdef DEBUG
  if (ierr != 0) {
   printf("ERROR: calc_density: Failed to dump density to file: %s\n",
          filename);
   return EXIT_FAILURE;
  }
#endif
#endif
 } /* end if me==MASTER */

#ifdef TIME_CODE_SEGMENTS
 if(me==MASTER) {
  t_Elapsed = MPI_Wtime() - t_Elapsed;
  printf("(%i) calc_density: Time spent:\n",me);
  printf("(%i)  Total: %4.2f; Distribute: %4.2f; Convolve: %4.2f\n",
         me, t_Elapsed, t_Dist, t_Conv);
  (void)fflush(stdout);
 }
#endif
 return EXIT_SUCCESS;
}
