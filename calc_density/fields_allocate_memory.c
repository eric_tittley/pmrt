/* Allocate memory for the density & velocity fields.
 *
 * ARGUMENTS
 *  Input, not modified
 *   me			My MPI process ID.
 *   Ncells		Number of cells to allocate.
 *  Assigned
 *   density		Pointer to density grid.
 *   velocity_z 	Pointer to LOS velocity grid.
 *   velocity_x 	Pointer to velocity grid across LOS.
 *   density_los	Pointer to density along a LOS.
 *
 * RETURNS
 *  EXIT_SUCCESS
 *  EXIT_FAILURE if there is a problem allocating memory.
 *
 * NOTES
 * 1) Since we want to modify the value of the pointer, we need to pass the
 *  addresses of the pointers to fields_allocate_memory. We modify the contents
 *  of those addresses, putting pointer values in them.  All very weird at
 *  first, but it works.
 * 2) If the master calls fields_allocate_memory, density, density_tmp, and Data
 *    are set.
 *    If a slave calls fields_allocate_memory, density, density_los, and
 *    Data_los are set.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  06 04 19 Separated from RT_allocate_memory.
 *  06 11 20 Removed the Macro NCELLS and added the arguement Ncells.
 *  08 09 06 pointers.
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>

#include "calc_density.h"
#include "RT.h"

int fields_allocate_memory(const int me, const int Ncells,
                           float **density,
                           float **velocity_z,
                           float **velocity_x)
{
 size_t AllocatedMemory=0;

#if VERBOSE >= 1
 printf("(%i) fields_allocate_memory: Begin...\n",me); (void)fflush(stdout);
#endif

 /* both master & slave need this */
 AllocatedMemory += Ncells * sizeof(float);
 *density = (float *) malloc(Ncells * sizeof(float));
 if (*density == NULL) {
  printf("ERROR: fields_allocate_memory: Allocation of memory failed: density\n");
  return EXIT_FAILURE;
 }
#ifdef CALC_VELOCITIES
 AllocatedMemory += Ncells * sizeof(float);
 *velocity_z = (float *) malloc(Ncells * sizeof(float));
 if (*velocity_z == NULL) {
  printf("ERROR: fields_allocate_memory: Allocation of memory failed: velocity_z\n");
  return EXIT_FAILURE;
 }
 AllocatedMemory += Ncells * sizeof(float);
 *velocity_x = (float *) malloc(Ncells * sizeof(float));
 if (*velocity_x == NULL) {
  printf("ERROR: fields_allocate_memory: Allocation of memory failed: velocity_x\n");
  return EXIT_FAILURE;
 }
#endif
 
 printf("(%2i) fields_allocate_memory: Allocated %i bytes = %6.2f Mb\n",
        me, (int)AllocatedMemory, (float)AllocatedMemory/1048576.0);
 (void)fflush(stdout);
#if VERBOSE >= 1
 printf("(%i) fields_allocate_memory: Done.\n",me); (void)fflush(stdout);
#endif
 return EXIT_SUCCESS;
}
