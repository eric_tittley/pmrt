/* Initialise the smoothing kernel used to convolve the low-density component
 * of the density field in calc_density().
 *
 * fftw_complex* InitKernel(int Ng, double radius, MPI_Comm *Communicator)
 *
 * ARUGUMENTS
 *  Input, not modified
 *   Ng			Number of elements per grid side.
 *   radius		The radius in cells over which to smooth.
 *   Communicator	The MPI Communicator involved in the convolution.
 *
 * GLOBAL VARIABLES or MACROS (Except those from the MPI and FFTW libraries)
 *
 * RETURNS
 *  kernel	The DFT of the smoothing kernel.
 *		A pointer to an [Ng^3/nproc] float, the data pointed to by
 *		kernel is only the slab used in this node (each node gets a
 *		different slab).
 *              kernel is complex with dimensions Ng*Ng*(Ng/2+1).
 *		It has components kernel[i].re and kernel[i].im
 *  NULL on failure.
 *
 * NOTE
 *  Memory for the kernel is allocated here.  IT MUST BE FREED by the calling
 *  routine.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  040117 First version
 *  040118 Modified gaussian to integrate to 1 over all space.
 *	Fixed bugs.  Seems to be working properly.
 *  050224 Added SRFFTW block to get working on lomond.
 *  050329 Minor modifications to diagnostic output.
 *	Explicitly convert integers into doubles in norm calculation.
 *  061121 Removed dependency on NG & NCELLS.  Now Ng is an argument.
 *  080804 Replace constants.c with RT_constants_configuration.c
 *	RT_constants_cosmology.c, RT_constants_physics.c, RT_constants_source.c
 *  110307 Ng must be even.
 */

#include <stdio.h> 
#include <math.h>
#ifndef M_PI
# define M_PI		3.14159265358979323846	/* pi */
#endif

#include <mpi.h>

#ifdef SRFFTW
#include <srfftw_mpi.h>
#else
#include <rfftw_mpi.h>
#endif

#include "calc_density.h"
#include "RT.h"
#include "RT_constants_configuration.h"

double gaussian(const double dist, const double radius) {
 /* Integrates to unity over all space */
 double P1,P2,P3,E1;
 P1=pow(radius,3);
 P2=pow(M_PI,3./2.);
 P3=pow(dist/radius,2.);
 E1=exp(-1.*P3);
 return 1./(P1*P2) * E1;
}

/*@null@*/ fftw_complex* InitKernel(const int Ng, const double radius,
                                    MPI_Comm const * Communicator) {
 const int NZ = Ng+2; /* FFTW needs padding. */
 const double norm = 1./(double)(Ng*Ng*Ng); /* The normalisation factor */
 fftw_real *kernel;
 double dist;
 int ix,iy,iz;
 int x,y,z;
 int nproc, me;
 int local_nx=0,local_x_start=0,local_ny_after_transpose=0;
 int local_y_start_after_transpose=0,local_size=0;
 rfftwnd_mpi_plan fftfplan=NULL;
#ifdef DEBUG
 int i;
 fftw_complex *kernel_C;
#endif
 
 /* Checks */
 /* Ng must be even */
 if( (Ng % 2) != 0 ) {
  printf("ERROR: %s: %i: Ng must be even. Ng = %i\n",__FILE__,__LINE__,Ng);
  (void)fflush(stdout);
  return NULL;
 }
 
 /* Get number of nodes and my node number. This can either be requested or
  * passed in. */
 (void)MPI_Comm_size(*Communicator, &nproc);
 (void)MPI_Comm_rank(*Communicator, &me);
 
 /* Initialize the DFT plan */
 fftfplan = rfftw3d_mpi_create_plan(*Communicator, Ng, Ng, Ng,
                                    FFTW_REAL_TO_COMPLEX, FFTW_ESTIMATE);
 
 /* Work out the ranges on each processor and the amount of reals we need
  * to allocate for (local_size).  Note: local_size will include the extra
  * padding of 2*Ng*Ng required for the complex data. */
 rfftwnd_mpi_local_sizes(fftfplan, &local_nx, &local_x_start, 
                         &local_ny_after_transpose,
                         &local_y_start_after_transpose, &local_size);

#ifdef DEBUG 
 /* Sanity Check */
 if( me == MASTER ) {
  if(local_size != Ng*Ng*NZ/nproc) {
   printf("(%2i) ERROR: InitKernel: local_size != Ng*Ng*NZ/nproc : %i != %i\n",
          me,local_size,Ng*Ng*NZ/nproc);
   (void)fflush(stdout);
    return NULL;
  }
 }
#endif

 /* Allocate memory for the local slab on which we will work.  FFTW knows
  * of the other processors' slabs and will work on them together. */
#ifdef DEBUG
 if( Ng > (int)sqrt( pow(2,31)-0.5 ) ) {
  printf("(%2i) ERROR: InitKernel: Ng too large to perform integer math.\n", me);
  (void)fflush(stdout);
  return NULL;
 }
#endif
 printf("(%2i) InitKernel: Allocating %6.2f MB.\n", me,
        (float)(local_size * sizeof(fftw_real)) / 1048576.0);
 (void)fflush(stdout);
 kernel = (fftw_real*)malloc(sizeof(fftw_real) * local_size);
 if(kernel == NULL) {
  printf("(%2i) ERROR: InitKernel: Allocation of memory failed: density\n",me);
  (void)fflush(stdout);
  return NULL;
 }

 /* Populate the kernel in real space.
  * Note: Every value repeats itself in 8 positions.  So we could save some cpu
  * time coding it to take advantage of the symmetry, but the code would look
  * much more convoluted. */
#if VERBOSE >= 1
 printf("(%2i) InitKernel: norm=%5.3e; Ncells=%i\n",me,norm,Ng*Ng*Ng);
 (void)fflush(stdout);
#endif
 for(ix=0; ix<local_nx; ix++) {
  x = local_x_start + ix;
  if(x > Ng/2) {
   x = Ng - x;
  }
  for(iy=0; iy<Ng; iy++) {
   if(iy <= Ng/2) {
    y = iy;
   } else {
    y = Ng - iy;
   }
   for(iz=0; iz<Ng; iz++) {
    if(iz <= Ng/2) {
     z = iz;
    } else {
     z = (Ng - iz);
    }
    /* Careful of the following. The addition can overflow if x, y, or z
     * get to > 15000.  That's not likely to happen with current memory, but
     * it would be possible if the machine had on the order of 100Tb. */
    dist = sqrt((double)(x*x + y*y + z*z));
    kernel[(ix*Ng + iy)*NZ + iz] = (fftw_real)(norm * gaussian(dist,radius));
   #ifdef DEBUG
    if(!(bool)isfinite(kernel[(ix*Ng + iy)*NZ + iz])) {
     printf("(%2i) ERROR: InitKernel: %i: kernel[%i]=nan\n",
            me,__LINE__-3,(ix*Ng + iy)*NZ + iz);
     (void)fflush(stdout);
     return NULL;
    }
   #endif
   }
  }
 }

 /* Fourier Transform the kernel */
 rfftwnd_mpi(fftfplan, 1, kernel, NULL, FFTW_TRANSPOSED_ORDER);

#ifdef DEBUG
 kernel_C = (fftw_complex *)kernel; /* Cast as a complex number */
 for(i=0;i<local_size/2;i++) {
  if(!(bool)isfinite(kernel_C[i].re)) {
   printf("(%2i) ERROR: InitKernel: %i: kernel_C[%i].re=nan\n",me,__LINE__,i);
   (void)fflush(stdout);
   return NULL;
  }
  if(!(bool)isfinite(kernel_C[i].im)) {
   printf("(%2i) ERROR: InitKernel: %i: kernel_C[%i].im=nan\n",me,__LINE__,i);
   (void)fflush(stdout);
   return NULL;
  }
 } 
#endif
 
 /* We are done with the plan */
 rfftwnd_mpi_destroy_plan(fftfplan);

 /* After the FFT, kernel points to complex data */
 return (fftw_complex *)kernel;
}
