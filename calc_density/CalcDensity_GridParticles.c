/* Interpolate particle positions onto a mesh
 *
 * ARGUMENTS
 *  Input, not modified
 *   Communicator               The MPI Communicator to use
 *   NumParticlesThisProcess    The number of controlled by this process
 *   NumParticles               The total number of particles in the simulation
 *   NumDimensions              The number of dimensions.  Always 3 (?)
 *                              Ndim is the associated global variable
 *   Ngrid                      The number of cells per grid side
 *   lside                      No idea. Some PM global, which I have passed in.
 *   r                          The particle positions
 *   v                          The particle velocities
 *  Output, initialised
 *   The MASTER process has the full density and velocity grids
 *   For the slaves, the following contain only part contributions
 *   density     Number of particles in each grid cell
 *   velocity_x  Sum of the velocity contributions in the X-direction
 *               (along the LOS)
 *   velocity_z  Sum of the velocity contributions in the Z-direction (perpendicular to the LOS, but
 *               in the plane)
 *
 * NOTES:
 *  Each processor has 1/NumParticlesrocessors of the total number of particles.
 *  No processor has all the particles.
 *
 *  To get mean velocity, the velocity grids need to be divided by the density
 *  grid, which contains the number of particles in each cell.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  110208 Split from calc_density.c
 *      There was an odd bug when SPREAD_GAS_DENSITY. Seems it was being done
 *      incorrectly.  But I rarely use it, so not sure.
 */

#ifdef HAVE_CONFIG
/* For DEBUG, CALC_VELOCITIES, SPREAD_GAS_DENSITY */
#include "config.h"
#endif

#include "calc_density.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#ifdef DEBUG
#include <assert.h>
#endif

#include <mpi.h>
#define MASTER 0

#include "isequal.h"

int CalcDensity_GridParticles(const MPI_Comm Communicator,
                              const int NumParticlesThisProcess,
                              const int NumParticles,
                              const int NumDimensions,
                              const int Ngrid,
                              const double *lside,
                              const float *r,
                              const float *v,
                              float *density,
                              float *velocity_x,
		              float *velocity_z
                              ) {

 const int Ncells = Ngrid * Ngrid * Ngrid;

 int i,nn,ierr;
 int me=0;
 double x[3];

#ifdef DEBUG
 float sum;
#endif

#ifdef SPREAD_GAS_PARTICLE
 int j, ix, iy, iz, i1y, i1z, los00, los01, los10, los11, ix_lo, ix_hi;
 double dx, dy, dz;
 double contribution00, contribution01, contribution10, contribution11;
 double W[2];
#else
 int los = 0;
 int cell;
#endif

#ifdef CALC_VELOCITIES
 /* pointer + 0 => x, + 1 => y, + 2 => z */
 const int Z_DIR = 2;
 const int X_DIR = 0;
 float vel_z, vel_x;
#endif

 /* Get my node number. */
 (void)MPI_Comm_rank(Communicator, &me);

#ifdef SPREAD_GAS_PARTICLE
 /* Initialise the weighting kernel */
 W[0] = 0.5;
 W[1] = 0.5;
#endif /* SPREAD_GAS_PARTICLE */

 /* Loop over all particles */
 for (nn = 0; nn < NumParticlesThisProcess; nn++) {

  /* Get the x,y,z of each particle, scaled to 0 to 1 */
  for (i = 0; i < NumDimensions; i++) {
   x[i] = r[NumDimensions * nn + i] / ((float)lside[i]);  /* x spans 0 to < 1 */
   if (x[i] >= 1.0) { /* How often does this happen? */
    x[i] = 1.0 - 1.0 / (float) (2 * Ngrid);
   }
  }
  
 #ifdef CALC_VELOCITIES
  vel_x = v[NumDimensions * nn + X_DIR];
  vel_z = v[NumDimensions * nn + Z_DIR];
 #endif

/* We have the option of putting the particle in one cell
 * or spreading it out over the nearest four */
#ifndef SPREAD_GAS_PARTICLE
  los  =  (int) floor((float) Ngrid * x[1]) 
        + Ngrid * (int) floor((float) Ngrid * x[2]);
  cell = (int) floor((float) Ngrid * x[0]) + Ngrid * los;
  density[cell] += 1.0;
 #ifdef CALC_VELOCITIES
  velocity_x[cell] += vel_x;
  velocity_z[cell] += vel_z;
 #endif
#else /* SPREAD_GAS_PARTICLE is defined */
  /* Spread the gas particle over nearest 4 cells. */
  /* Note the x dimension is the fast index in the density array. */
  ix = (int) floor(Ngrid * x[0]);
  iy = (int) floor(Ngrid * x[1]);
  iz = (int) floor(Ngrid * x[2]);
  i1y = (iy + 1) % Ngrid;
  i1z = (iz + 1) % Ngrid;
  dx = Ngrid * x[0] - ix;
  dy = Ngrid * x[1] - iy;
  dz = Ngrid * x[2] - iz;
  los00 = iy + Ngrid * iz;
  los01 = iy + Ngrid * i1z;
  los10 = i1y + Ngrid * iz;
  los11 = i1y + Ngrid * i1z;
  if (dx <= 0.5) {
   ix_lo = (ix - 1 );
   ix_hi = (ix + 1 ) - 1;
  } else {
   ix_lo = (ix - 1 ) + 1;
   ix_hi = (ix + 1 );
  }
  contribution00 = ((1.0 - dy) * (1.0 - dz));
  contribution01 = ((1.0 - dy) * dz);
  contribution10 = (dy * (1.0 - dz));
  contribution11 = (dy * dz);
  /* The following _might_ be faster in 4 separate loops due to distribution 
   * in memory. */
  j=0;
  for (ix = ix_lo; ix <= ix_hi; ix++) {
   i = (ix + Ngrid) % Ngrid;
  #ifdef DEBUG
   if (((i + Ngrid * los00) >= Ncells) || ((i + Ngrid * los00) < 0)) {
    printf("(%2i) ERROR: calc_density: index out of bounds %i\n", me,
           (i + Ngrid * los00));
    printf("     ix=%i; i=%i; Ngrid=%i; los00=%i;\n", ix, i, Ngrid, los00);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
   if (((i + Ngrid * los01) >= Ncells) || ((i + Ngrid * los01) < 0)) {
    printf("(%2i) ERROR: calc_density: index out of bounds %i\n", me,
           (i + Ngrid * los01));
    printf("     ix=%i; i=%i; Ngrid=%i; los01=%i;\n", ix, i, Ngrid, los01);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
   if (((i + Ngrid * los10) >= Ncells) || ((i + Ngrid * los10) < 0)) {
    printf("(%2i) ERROR: calc_density: index out of bounds %i\n", me,
           (i + Ngrid * los10));
    printf("     ix=%i; i=%i; Ng=%i; los10=%i;\n", ix, i, Ngrid, los10);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
   if (((i + Ngrid * los11) >= Ncells) || ((i + Ngrid * los11) < 0)) {
    printf("(%2i) ERROR: calc_density: index out of bounds %i\n", me,
           (i + Ngrid * los11));
    printf("     ix=%i; i=%i; Ng=%i; los11=%i;\n", ix, i, Ngrid, los11);
    (void)fflush(stdout);
    return EXIT_FAILURE;
   }
   assert(j<2);
  #endif /* DEBUG */
   density[i + Ngrid * los00] += (float)contribution00*W[j];
   density[i + Ngrid * los01] += (float)contribution01*W[j];
   density[i + Ngrid * los10] += (float)contribution10*W[j];
   density[i + Ngrid * los11] += (float)contribution11*W[j];
  #ifdef CALC_VELOCITIES
   /* These sums are essentially momenta. Convert to velocity later. */
   velocity_x[i + Ngrid * los00] += (float)contribution00*W[j]*vel_x;
   velocity_x[i + Ngrid * los01] += (float)contribution01*W[j]*vel_x;
   velocity_x[i + Ngrid * los10] += (float)contribution10*W[j]*vel_x;
   velocity_x[i + Ngrid * los11] += (float)contribution11*W[j]*vel_x;
   velocity_z[i + Ngrid * los00] += (float)contribution00*W[j]*vel_z;
   velocity_z[i + Ngrid * los01] += (float)contribution01*W[j]*vel_z;
   velocity_z[i + Ngrid * los10] += (float)contribution10*W[j]*vel_z;
   velocity_z[i + Ngrid * los11] += (float)contribution11*W[j]*vel_z;
  #endif
   j++;
  }
#endif /* SPREAD_GAS_DENSITY */
 } /* End loop over particles */

#ifdef DEBUG
 for (i = 0; i < Ncells; i++) {
  if(!(bool)isfinite(density[i])) {
   printf("(%2i) ERROR: %s: %i: density[%i]=nan\n",me,__FILE__,__LINE__,i);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }
#endif

 /* Merge the contributions from each process */
 ierr = reduce(&Communicator, Ncells, density);
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) ERROR: %s: %i: reduce() returned in error\n",
         me,__FILE__,__LINE__-3);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }

#ifdef CALC_VELOCITIES
 ierr = reduce(&Communicator, Ncells, velocity_x);
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) ERROR: %s: %i: reduce() returned in error\n",
         me,__FILE__,__LINE__-3);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
 ierr = reduce(&Communicator, Ncells, velocity_z);
 if(ierr != EXIT_SUCCESS) {
  printf("(%2i) ERROR: %s: %i: reduce() returned in error\n",
         me,__FILE__,__LINE__-3);
  (void)fflush(stdout);
  return EXIT_FAILURE;
 }
#endif

#ifdef DEBUG
 if(me == MASTER) {
  sum = 0;
  for (i = 0; i < Ncells; i++) {
   sum += density[i];
  }
  if( ! f_isequal( sum, (float)NumParticles) ) {
   printf("ERROR: %s: %i NumParticles should equal sum(density)\n",
          __FILE__,__LINE__);
   printf("                       NumParticles=%i; sum(density)=%e;\n",
          NumParticles, sum);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 }
#endif

 return EXIT_SUCCESS;
}
