/* Add the contributions to a grid from all processes.
 *
 * ARGUMENTS
 *  Input, not modified
 *   Communicator  The communicator for processed involved in the reduction.
 *   Ncells	Number of cells to merge. (int)
 *  Input and modified
 *   array	Data to merge. Size Ncells. (float)
 *
 *  RETURNS
 *   EXIT_SUCCESS
 *
 *  NOTES
 *   "array" is accumulated on the Master node.  Other nodes may modify it.
 *   Assumes the data are floats.
 *
 * GLOBAL VARIABLES and MACROS
 *  REDUCE_METHOD	1 (Linear accumulation.  Poor scaling.)
 *			2 (Built-in MPI_Reduce.  Should work best, but doesn't.)
 *	      (default)	3 (Binary Tree.  Best.)
 *
 *  AUTHOR: Eric Tittley
 *
 *  HISTORY
 *   06 03 21 First version.
 *   06 09 21 NCELLS is passed in as Ncells
 *   06 10 05 Removed the need for round(), which produces inconsistent results
 *	on different machines, different compilers on the same machine, and
 *	even different versions of the same compiler.  NEVER USE round(),
 *	rint(), or nearbyint().
 *	Introduce the function powi() to circumvent the need for round().
 *	Introduce the function whatexpi() to circumvent the need for rint().
 */

#include <math.h>
#include <mpi.h>
#include <stdlib.h>
#include <stdio.h>

#include "config.h"

#include "calc_density.h"
#include "RT.h"

#ifndef REDUCE_METHOD
#define REDUCE_METHOD 3
#endif

int powi(int N, int exp) {
 /* Return N^exp, using integer arithmetic */
 int i,result=1;
#ifdef DEBUG
 if( exp < 0 ) {
  printf("ERROR: ipow: exp = %i < 0\n",exp);
  (void)fflush(stdout);
  return -1;
 }
#endif
 for(i=1;i<=exp;i++) {
  result *= N;
 }
 return result;
}

int whatexpi(int N, int value) {
 /* returns n such that N*n = value where N and value are integers. */
 int n=-1;
 do {
  n++;
 } while( powi(N,n)<value );
 return n;
}

int reduce(const MPI_Comm *Communicator, const int Ncells,
           float *array) {
 float *array_tmp=NULL;
 int me=-1;

#if REDUCE_METHOD == 1
 /* Linear accumulation. */
 const int TAG = 10;
 int proc, i, nproc;
 MPI_Status status;

 (void)MPI_Comm_size(*Communicator, &nproc);
 (void)MPI_Comm_rank(*Communicator, &me);

 if (me != MASTER) {
  (void)MPI_Ssend(array, Ncells, MPI_FLOAT, MASTER, TAG, *Communicator);
 } else { /* MASTER */
  array_tmp = malloc(sizeof(float)*Ncells);
  for (proc = 1; proc < nproc; proc++) {
   (void)MPI_Recv(array_tmp, Ncells, MPI_FLOAT, MPI_ANY_SOURCE, TAG,
                  *Communicator, &status);
   for (i = 0; i < Ncells; i++) {
    array[i]  += array_tmp[i];
   }
  }
 } /* endif me!= MASTER, else */
#endif /* METHOD_1 */


#if REDUCE_METHOD == 2
 (void)MPI_Comm_rank(*Communicator, &me);
 /* Built-in Reduce */
 if(me == MASTER) {
  array_tmp = malloc(sizeof(float)*Ncells);
 }
 (void)MPI_Reduce(array, array_tmp, Ncells, MPI_FLOAT, MPI_SUM, MASTER, *Communicator);
 if(me == MASTER) {
  memcpy(array,array_tmp,Ncells*sizeof(float));
 }
#endif


#if REDUCE_METHOD == 3
 /* Binary Tree */
 const int TAG = 10;
 int i,nproc=-1;
 int Nlevels, level, me_tree, source, dest;
 MPI_Status status;
 bool done;

 (void)MPI_Comm_size(*Communicator, &nproc);
 (void)MPI_Comm_rank(*Communicator, &me);

 if( (int)floor(me/2.)*2 == me ) {
  array_tmp = (float *)malloc(sizeof(float)*Ncells);
 #ifdef DEBUG
  if(array_tmp == NULL) {
   printf("(%2i) reduce: %i: Unable to allocate memory\n",me,__LINE__-3);
   (void)fflush(stdout);
   return EXIT_FAILURE;
  }
 #endif
 }
 Nlevels = whatexpi(2,nproc); /* nproc = 2^Nlevels */
 me_tree = me;
 done = FALSE;
 for(level=1;level<=Nlevels;level++) {
  if(!done) {
   if( (int)floor(me_tree/2.)*2 == me_tree ) { /* Even. Accumulate. */
    source = me + powi(2,level-1);
    (void)MPI_Recv(array_tmp, Ncells, MPI_FLOAT, source, TAG, *Communicator, &status);
    for(i=0;i<Ncells;i++) {
     array[i] += array_tmp[i];
    }
   } else { /* Odd.  Pass to left. I'm done.*/
    dest = me - powi(2,level-1);
    (void)MPI_Ssend(array, Ncells, MPI_FLOAT, dest, TAG, *Communicator);
    done = TRUE;
   }
   me_tree = me_tree / 2;
  }
  /* Use a barrier to prevent node from getting so far ahead of others that it
   * sends data from one level prior to other nodes getting there.  Probably not
   * needed since for every level each node receives from a unique source. But
   * it has no effect on the execution time. */
  (void)MPI_Barrier(*Communicator);
 }
#endif

 free(array_tmp);
 return EXIT_SUCCESS;
}
