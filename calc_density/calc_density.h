#ifndef _CALC_DENSITY_H_
#define _CALC_DENSITY_H_

#ifdef HAVE_CONFIG
#include "config.h"
#endif

#include <mpi.h>
#ifdef SRFFTW
#include	<srfftw_mpi.h>
#else
#include	<rfftw_mpi.h>
#endif

/* Calculation of gas density from DM distribution */
int calc_density(const MPI_Comm Communicator,
                 const int np,
                 const int Ng,
                 const double aa,
                 const double vunit,
                 const float *r,
                 const float *v,
		 const fftw_complex *kernel,
                 float *density,
                 float *velocity_x,
		 float *velocity_z);

/* Allocate memory for the density & velocity fields. */
int fields_allocate_memory(const int me, const int Ncells, float **density,
                           float **velocity_z, float **velocity_x);

/* Dumping the density grid to file */
int dumpdensity(const char *filename, const float *density, const size_t N);

double gaussian(double dist, double radius);

/* Initializing the smoothing kernel for calc_density */
/*@null@*/ fftw_complex* InitKernel(const int Ng, const double radius,
                                    MPI_Comm const *Communicator);

/* Convolve the velocity (momentum) grid */
int CalcDensity_ConvolveMomentumGrid(const MPI_Comm Communicator,
                                     const int Ngrid,
                                     const float NumberCutForConvolution,
                                     const float *density,
                                     const fftw_real *density_lo,
                                     const float *density_hi,
		                     const fftw_complex *kernel,
                                     const rfftwnd_mpi_plan fftfplan,
                                     const rfftwnd_mpi_plan fftiplan,
                                     float *velocity);

/* Generate the density and momentum grids from the particle positions and
 * velocities */
int CalcDensity_GridParticles(const MPI_Comm Communicator,
                              const int np,
                              const int NumParticles,
                              const int NumDimensions,
                              const int Ngrid, 
                              const double *lside,
                              const float *r,
                              const float *v,
                              float *density,
                              float *velocity_x,
		              float *velocity_z);

int CalcDensity_ConvolveGrids(const MPI_Comm Communicator,
                              const int Ngrid,
                              const float NumberCutForConvolution,
 		              const fftw_complex *kernel,
                              float *density,
                              float *velocity_x,
		              float *velocity_z);

/* Add the contributions to a grid from all processes. */
int reduce(const MPI_Comm *Communicator, const int Ncells,
           float *array);

int powi(int N, int exp);
int whatexpi(int N, int value);
#endif
