/* Show the configurations with which PMRT was compiled.
 *
 * AUTHOR: Eric Tittley
 */

#include "config.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#ifndef M_SQRT1_2
# define M_SQRT1_2	0.70710678118654752440	/* 1/sqrt(2) */
#endif

#include "Support.h"
#include "RT.h"
#include "RT_SourceT.h"
#include "RT_constants_configuration.h"
#include "RT_constants_cosmology.h"
#include "RT_constants_physical.h"
#include "RT_constants_source.h"

#include "RT_Data.h"

#include "CFLAGS.h"

int ShowConfig(void) {
 int source_method=0;
 int los_method=0;

#ifdef SUBVERSION
 #include "PMRT_SVN_REVISION.h"
 #include "RT_SVN_REVISION.h"
 printf("PMRT revision: %s\n",PMRT_SVN_REVISION);
 printf("  RT revision: %s\n",RT_SVN_REVISION);
#endif

 printf("**** Compile-time options ****\n");

 /* The optimization flags with which the code was compiled */
 printf("CC = %s\n",CC);

 /* The optimization flags with which the code was compiled */
 printf("CFLAGS = %s\n",CFLAGS);

#ifdef DEBUG
 printf("DEBUG\n");
#endif

 printf("VERBOSE = %i\n", VERBOSE);

#ifdef TIME_CODE_SEGMENTS
 printf("TIME_CODE_SEGMENTS\n");
#endif

/* The parallelisation method, if any */
#ifdef RT_SERIAL
 printf("RT_SERIAL: Code compiled for serial execution.\n");
#endif
#ifdef RT_MPI
 printf("RT_MPI: Code compiled for parallel execution using distributed memory.\n");
#endif
#ifdef RT_OPENMP
 printf("RT_OPENMP: Code compiled for parallel execution using shared memory.\n");
#endif
#if   (defined RT_SERIAL) && (defined RT_MPI) \
   || (defined RT_SERIAL) && (defined RT_OPENMP) \
   || (defined RT_MPI) && (defined RT_OPENMP)
 printf("Can enable only one of RT_SERIAL, RT_MPI & RT_OPENMP\n");
 return EXIT_FAILURE; 
#endif
#if   !(defined RT_SERIAL) && !(defined RT_MPI) && !(defined RT_OPENMP)
 printf("One of RT_SERIAL, RT_MPI or RT_OPENMP must be set.\n");
 return EXIT_FAILURE; 
#endif

#ifdef FORCE_REDSHIFT_STOP
 printf("WARNING: Forcibly halting at a redshift of %f\n",FORCE_REDSHIFT_STOP);
#endif

 /* *** CHECK THE DENSITY METHOD *** */
#ifdef CALC_DENSITY
 source_method++;
 printf("CALC_DENSITY\n");
#endif
#ifdef CONSTANT_DENSITY
 source_method++;
 printf("CONSTANT_DENSITY = %e\n", CONSTANT_DENSITY);
#endif
#ifdef READ_DENSITY_FILE
 source_method++;
 printf("READ_DENSITY_FILE = %s\n",READ_DENSITY_FILE);
#endif
#ifdef CALC_DENSITY_POLAR
 source_method++;
 printf("CALC_DENSITY_POLAR\n");
 printf(" NR=%i; NTHETA=%i; XOFFSET=%f; YOFFSET=%f\n",NR,NTHETA,XOFFSET,YOFFSET);
 printf(" POINTS=%i; PARTICLERADIUS=%f; SEED=%i\n",POINTS,PARTICLERADIUS,SEED);
 printf(" RT_RADIUS=%f (%f h^-1 Mpc)\n",RT_RADIUS,RT_RADIUS*Length);
 printf(" SLAB_THICKNESS=%f\n",SLAB_THICKNESS);
#endif

 if(source_method==0) {
  printf("ERROR: You must set either CALC_DENSITY, CONSTANT_DENSITY.\n");
  printf("       READ_DENSITY_FILE, or CALC_DENSITY_POLAR.\n");
  return EXIT_FAILURE;
 }
 if(source_method>1) {
  printf("ERROR: You must set only one of CALC_DENSITY, CONSTANT_DENSITY.\n");
  printf("       READ_DENSITY_FILE, or CALC_DENSITY_POLAR.\n");
  return EXIT_FAILURE;
 }
 source_method=0;

#if (defined CALC_DENSITY_POLAR) & (defined PLANE_WAVE)
 printf("ERROR: Defined CALC_DENSITY_POLAR and PLANE_WAVE");
 return EXIT_FAILURE;
#endif

#ifdef CONSTANT_DENSITY_COMOVING
#ifndef CONSTANT_DENSITY
 printf("ERROR: Defined CONSTANT_DENSITY_COMOVING but not CONSTANT_DENSITY");
 return EXIT_FAILURE;
#endif
 printf("CONSTANT_DENSITY_COMOVING. The density will be scaled by a^-3.\n");
#endif

#ifdef CALC_VELOCITIES
#if !((defined CALC_DENSITY) || (defined CALC_DENSITY_POLAR))
 printf("ERROR: CALC_VELOCITIES set but not CALC_DENSITY nor CALC_DENSITY_POLAR.\n");
 return EXIT_FAILURE;
#endif
 printf("CALC_VELOCITIES\n");
#else
 printf("WARNING: Gas velocities will not be calculated and stored.\n");
#endif

#ifdef SPREAD_GAS_PARTICLE
#ifndef CALC_DENSITY
 printf("WARNING: SPREAD_GAS_PARTICLE set but not CALC_DENSITY.\n");
 printf("         SPREAD_GAS_PARTICLE will be ignored.\n");
#endif
 printf("SPREAD_GAS_PARTICLE\n");
#endif

#ifdef CONVOLVE_DENSITY
#ifndef CALC_DENSITY
 printf("WARNING: CONVOLVE_DENSITY set but not CALC_DENSITY.\n");
 printf("         CONVOLVE_DENSITY will be ignored.\n");
#endif
 printf("CONVOLVE_DENSITY number_cut_for_convolution=%2.0f; radius=%5.2f\n",
        number_cut_for_convolution, radius);
#endif

#ifdef ENFORCE_MINIMUM_DENSITY
 printf("ENFORCE_MINIMUM_DENSITY = %e\n",ENFORCE_MINIMUM_DENSITY);
#endif

#ifdef LOS_ZERO
 los_method++;
 printf("LOS_ZERO\n");
#endif
#ifdef LOS_SLICE
 los_method++;
 printf("LOS_SLICE = %i\n",LOS_SLICE);
#endif
#ifdef LOS_ALL
 los_method++;
 printf("LOS_ALL\n");
#endif
if( los_method==0 ) {
 printf("ERROR: You haven't defined either LOS_ZERO, LOS_SLICE, or LOS_ALL.\n");
}
if( los_method>1 ) {
 printf("ERROR: You can only define one of LOS_ZERO, LOS_SLICE, or LOS_ALL.\n");
}
#ifdef LOS_SLICE
 if(LOS_SLICE > NG) {
  printf("ERROR: LOS_SLICE > NG (%i > %i).\n",LOS_SLICE,NG);
  return EXIT_FAILURE;
 }
 printf("LOS_SLICE = %i\n",LOS_SLICE);
#endif

#ifdef REFINEMENTS
 printf("REFINEMENTS\n");
 printf(" Max_tau_in_cell=%5.2f\n",Max_tau_in_cell);
#ifdef USE_MCTfR
 printf(" USE_MCTfR is set. Max_cumulative_tau_for_refinement=%5.2f\n",
        Max_cumulative_tau_for_refinement);
#endif
#endif

#ifdef THIN_APPROX
 printf("THIN_APPROX: Thin approximation. Spectrum is not modified by absorption in the column.\n");
#endif

#if !( (defined INTEGRATE_ODE_EULER) || (defined INTEGRATE_ODE_RUNGE_KUTTA) )
 printf("ERROR: Either INTEGRATE_ODE_EULER or INTEGRATE_ODE_RUNGE_KUTTA must be defined.\n");
 return EXIT_FAILURE;
#endif
#if (defined INTEGRATE_ODE_EULER) && (defined INTEGRATE_ODE_RUNGE_KUTTA)
 printf("ERROR: Only one of INTEGRATE_ODE_EULER or INTEGRATE_ODE_RUNGE_KUTTA can be defined.\n");
 return EXIT_FAILURE;
#endif
#ifdef INTEGRATE_ODE_EULER
 printf("INTEGRATE_ODE_EULER: using Euler's method for the ODE integration\n");
#endif
#ifdef INTEGRATE_ODE_RUNGE_KUTTA
 printf("INTEGRATE_ODE_RUNGE_KUTTA: using a 4th-order Runge-Kutta method for the ODE integration\n");
#endif

#ifdef ADAPTIVE_TIMESCALE
 printf("ADAPTIVE_TIMESCALE\n");
 printf(" Timestep_Factor=%6.3f\n",Timestep_Factor);
#endif
#ifdef TRACK_TIMESCALES
 printf("TRACK_TIMESCALES = %s\n",TRACK_TIMESCALES);
#endif
 printf("MIN_DT=%5.2e\n",MIN_DT);

#ifdef USE_B_RECOMBINATION_CASE
 printf("USE_B_RECOMBINATION_CASE\n");
#endif

#ifdef ENFORCE_MINIMUM_CMB_TEMPERATURE
 printf("ENFORCE_MINIMUM_CMB_TEMPERATURE\n");
#endif

#ifdef BACKUPS
 printf("BACKUPS\n");
#endif

#if !( (defined SIMPSONS) || (defined ROMBERG) || (defined GAUSSIAN_QUADRATURE) )
 printf("ERROR: You haven't defined any integration method in config.h .\n");
 return EXIT_FAILURE;
#endif

#if (defined SIMPSONS) && (defined ROMBERG)
 printf("ERROR: You've defined both SIMPSONS and ROMBERG in config.h .\n");
 return EXIT_FAILURE;
#endif

#if (defined SIMPSONS) && (defined GAUSSIAN_QUADRATURE)
 printf("ERROR: You've defined both SIMPSONS and GAUSSIAN_QUADRATURE in config.h .\n");
 return EXIT_FAILURE;
#endif

#if (defined ROMBERG) && (defined GAUSSIAN_QUADRATURE)
 printf("ERROR: You've defined both ROMBERG and GAUSSIAN_QUADRATURE in config.h .\n");
 return EXIT_FAILURE;
#endif

#if ( (defined SIMPSONS) || (defined ROMBERG) || (defined GAUSSIAN_QUADRATURE) )
 printf("N_Levels_[123] = %i; %i; %i\n", N_Levels_1,N_Levels_2,N_Levels_3);
#endif

 printf("NFLUX=%i\n",NFLUX);
 printf("NG=%i\n",NG);
 printf("N_OUTPUT_REDSHIFTS=%i\n",N_OUTPUT_REDSHIFTS);
 printf("Volume: R_0=%5.2f; Length =%5.2f h^-1 Mpc\n",R_0,Length);

 /* *** Check the SOURCE METHOD *** */
 /* With RT_SourceT *Source providing the Source type, this section reviewing
  * the types of sources needs to be re-written */

#ifdef SOURCE_NORMALISE_SPECTRUM
 printf("SOURCE_NORMALISE_SPECTRUM: Ignore L_0 above.\n");
 printf(" Spectrum will be normalised to produce %6.2e H ionising photons per second.\n",
        N_H_Ionising_photons_per_sec);
#endif

#ifdef PLANE_WAVE
 printf("PLANE_WAVE: The source is not attenuated by r^-2.\n");
#else
 printf("The source is attenuated by r^-2.\n");
#endif

 printf("RT_Data is version %i.%i\n",VERSION_MAJOR,VERSION_MINOR);
#ifdef RT_DATA_SHORT
 printf(" RT_DATA_SHORT: Will not store the rates for heating, cooling, ionisation, and energy input.\n");
#endif

 printf("\n");

 (void)fflush(stdout);

 return EXIT_SUCCESS;
}
