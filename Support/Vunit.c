/* Vunit: Convert PM code units of velocity into km/s
 *
 * vunit=Vunit(a,boxsize,Omega_o,Omega_v,Omega_k);
 *
 * ARGUMENTS
 *  a           Current expansion factor
 *  boxsize     Co-moving length of the PM volume edge, in Mpc/h
 *  Omega_o     The contribution to Omega from all the mass (DM + Baryons)
 *  Omega_v     The vacuum contribution to Omega
 *  Omega_k     ?  Set to 0
 *
 * RETURNS
 *  The coefficient to convert Martin White's PM code's internal velocity
 *  unit into km/s.
 * 
 * NOTES:
 * From an email from Avery (110218)
 *
 * The velocity is computed as follows:
 *  * set anow = 1/ (1 + z)
 *  * set Hz = 100.*Sqrt(omega0/anow**3+omegav+omegak/anow**2)
 *  * set vside = boxside*anow*Hz
 *  where boxside is the comoving length of the box in Mpc/h
 *
 * Then vside is the Hubble velocity in km/s along the length
 * of the box at epoch anow. This is the scale factor for the
 * peculiar velocities. The total velocity of a gas element in
 * a cell is then the Hubble velocity to the cell plus vg*vside,
 * where vg is the peculiar velocity in code units.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  110218 First version
 */

#include <math.h>

double Vunit(const double a,
             const double boxsize,
             const double Omega_o, 
             const double Omega_v,
             const double Omega_k) {
 
 double Hz;
 Hz = 100.0*sqrt(Omega_o*pow(a,-3.0) + Omega_v + Omega_k*pow(a,-2.0));
 return boxsize * a * Hz;
}
