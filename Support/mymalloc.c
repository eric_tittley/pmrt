#ifdef HAVE_CONFIG
#include "config.h"
#endif

#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "Support.h"

/*@null@*//*@out@*/ void *mymalloc(const size_t size)
/* Allocate memory and check if it worked. */
{
 void *tmp=NULL;
 if (size > 0) {
  tmp = malloc(size);
 #ifdef DEBUG
  if (tmp == NULL) {
   printf("ERROR: mymalloc: Unable to allocate %li bytes.\n", (long)size);
   fflush(stdout);
   MPI_Abort(MPI_COMM_WORLD, 1);
   exit(1);
  }
 #endif
 }
 return (tmp);
}
