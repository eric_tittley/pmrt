/* Show the cosmological parameters with which PMRT was compiled.
 *
 * AUTHOR: Eric Tittley
 *
 * HISTORY
 *  090703 First version
 */

#include <stdio.h>

#include "Support.h"
#include "RT_constants_cosmology.h"

void ShowCosmology(void) {
 printf("**** Cosmology ****\n");
 printf("h=%f\n",h);
 printf("Y (He mass fraction) = %f\n",Y);
 printf("Omega_b=%f\n",omega_b);
 printf("Omega_m=%f (all matter)\n",omega_m);
 printf("Omega_v=%f\n",omega_v);
 printf("\n");
 (void)fflush(stdout);
}
