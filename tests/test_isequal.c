#include "isequal.h"

int main() {
 
 const float x[2][5] = {
  {1., 1.0e36, 3.1415926535897932,  0.000004, 0.123456},
  {1., 1.0e-4, 3.1415926535897932, -0.000004, 0.123457},
 };

 int i;
 for(i=0;i<5;i++) {
  if( f_isequal(x[0][i],x[1][i]) ) {
   printf("%7.5e == %7.5e\n",x[0][i],x[1][i]);
  } else {
   printf("%7.5e != %7.5e\n",x[0][i],x[1][i]);
  }
 }

 return 0;
}
