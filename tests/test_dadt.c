#include <stdlib.h>
#include <stdio.h>

#include "RT_constants_cosmology.h"

double RT_LFP_dadt(const double t);

int main() {
 static double t[8] = {1e1, 1e2, 1e4, 1e8, 1e16, 0, 0, 0};
 int i;
 
 t[6]=1./(3./2.*H_0*sqrt(1.-omega_m));
 t[5]=t[6]*0.9;
 t[7]=t[6]*1.1;
 
 for(i=0;i<8;i++) {
  printf("%8.3e %8.3e\n",t[i],RT_LFP_dadt(t[i]));
 }
 
 return EXIT_SUCCESS;
}
