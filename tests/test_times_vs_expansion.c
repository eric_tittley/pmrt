/* Verify that cosmological_time and expansion_factor_from_time are
 * self-consistent.  Note, this doesn't verify that they are actually
 * correct. Just consistent. */

#include <stdlib.h>
#include <stdio.h>

#include "config.h"
#include "RT.h"
#include "RT_constants_configuration.h"

double RT_LFP_dadt(const double t);

int main() {
 double a, t, aa, dadt;
 
 for(a=0.000;a<=1.000*OnePlusEps;a+=0.001) {
  t=cosmological_time(a);
  aa=expansion_factor_from_time(t);
  dadt=RT_LFP_dadt(t);
  printf("%8.5e %8.5e %8.5e %8.5e\n",a,aa,t,dadt);
  if( (a>aa*OnePlusEps) || (a<aa*OneMinusEps) ) {
   printf("ERROR: test_times_vs_expansion FAILED\n");
   return EXIT_FAILURE;
  }
 }
 return EXIT_SUCCESS;
}
